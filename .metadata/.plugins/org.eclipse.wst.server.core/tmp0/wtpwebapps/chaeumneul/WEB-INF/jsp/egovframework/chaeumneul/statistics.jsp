<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c"         uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"      uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<%@ taglib prefix="spring"    uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link rel = "icon" href ="<c:url value='/img/egovframework/chaeumneul/books.ico'/>">
	
  	<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>채움늘 교육과정 관리 패키지</title>
	
	<link rel="stylesheet" href="<c:url value='/js/lib/bootstrap/css/bootstrap.min.css'/>">
	
	<style>
	        html,
	        body {
	            height: 100% !important;
	        }
			
			.nav-tabs.nav-justified > li > a {
			    border: none !important;
			    border-radius:0;
			    padding: 0;
			    text-transform: uppercase;
			}
			
			.nav-tabs.nav-justified > li > a:hover {
			    border: none;
			    border-radius:0;
			    background-color: inherit !important;
			}
			
			.nav-tabs.nav-justified > li > a > .mapText {
			    border-bottom:3px solid #FFF!important;
			    display: block;	   
			    color:#000;
			    padding: 20px 0;
			    font-size: 20px;
			}
			
			.nav-tabs.nav-justified > li.active > a > .mapText {
			  	border: none;
			    border-bottom:3px solid #093!important;
			    color: #093;
			    font-size: 20px;  
			}
			
			.nav-tabs.nav-justified > li.active > a {
			    border-right:1px solid #fff;
			    background-color:#fff;
			}
			
			.sidebar-nav {
		    	width: 250px;
		   		margin: 0;
		    	padding: 0;
		    	list-style: none;
		  	}
		  
		  	.sidebar-nav li {
		  		font-size: 20px;  
		    	text-indent: 1.5em;
		    	line-height: 2.8em;
		  	}
		  
		  	.sidebar-nav li a {
		    	display: block;
		    	text-decoration: none;
		    	color: #000;
		  	}
		  
		  	.sidebar-nav li a:hover {
		    	color: #093;
		    	background: rgba(80, 80, 80, 0.1);
		  	}
		  	
		  	.sidebar-nav li.active a {
		    	color: #093;
		    	background: rgba(80, 80, 80, 0.1);
		  	}
		  
		 	.sidebar-nav > .sidebar-brand {
		    	font-size: 1.3em;
		   		line-height: 3em;
		  	}
	</style>
	
    <!-- 외부 라이브러리 -->
    <script type="text/javascript" src="<c:url value='/js/lib/jquery/jquery.min.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/lib/bootstrap/js/bootstrap.min.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/lib/echarts.min.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/lib/inapp.js'/>"></script>

    <!-- INORG 제품 -->
    <script type="text/javascript" src="<c:url value='/js/lib/softin/softin.js'/>"></script>           <!-- 라이센스 정보 -->
    <script type="text/javascript" src="<c:url value='/js/lib/softin/inorg/inorginfo.js'/>"></script>  <!-- 생성시 필요한 함수 및 내부설정용 파일 -->
    <script type="text/javascript" src="<c:url value='/js/lib/softin/inorg/inorg.js'/>"></script>      <!-- core file -->

    <!-- IBSheet 제품 -->

    <script type="text/javascript" src="<c:url value='/js/lib/softin/ibsheet/ibsheet.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/lib/softin/ibsheet/ibsheetinfo.js'/>"></script>
   
   	<!-- 화면 소스 -->
   <script type="text/javascript" src="<c:url value='/js/chaeumneul/statistics.js'/>"></script>
   <script type="text/javascript" src="<c:url value='/js/chaeumneul/app.js'/>"></script>
</head>

<body>
	
	<div id="containerMain" class="container-fluid" style="height:100%; margin: 0 !important;">
		<div id="menuArea" class="row" style="margin: 10px !important; border-bottom:1px solid #808080!important;">
			<div class="col-md-3" style="text-align: center;">
				<img src="/img/logoGreen.png" style="height:70px">
			</div>
			<div class="col-md-9">
				  <!-- Nav tabs -->
				  <ul class="nav nav-tabs nav-justified" role="tablist">
				      <li role="presentation">
				          <a href="chaeumneul.do" >
				              <span class="mapText">조회</span>
				          </a>
				      </li>
				      <li role="presentation" >
				          <a href="management.do" >
				              <span class="mapText">관리</span>
				          </a>
				      </li>
				      <li role="presentation" class="active">
				          <a href="statistics.do">
				              <span class="mapText">통계</span>
				          </a>
				      </li>
				  </ul>
			</div>
			
		</div>
	  <!-- Org -->
	    <div id="contentsArea" class="row" style="height:100%; margin: 0 !important;">
	     	<div class="col-md-3" style="height:100%;">
	       		  <!-- 사이드바 -->
				  <div id="sidebar-wrapper">
				  	<h2>&nbsp;&nbsp;통계</h2>
				    <ul class="sidebar-nav" style="width: 100%;">
				      	<li class="active" ><a href="statistics.do"><span style="font-weight: bold;">전체 통계</span></a></li>
				    </ul>
				  </div>
				  <!-- /사이드바 -->
	        </div>
	        <div class="col-md-9" style="height:100%;">
	        	<div class="row" >
	        		<div class="col-md-5" style="border:1px solid #808080; margin: 10px">
	        			<div class="row well" style="text-align:center; "><span style="font-weight: bold;">강사당 담당 수강인원 수</span></div>
	        			<div class="center-block" id="peopleManagerStudentChart" style="height:300px;">
                  		</div>
	        		</div>
					<div class="col-md-5" style="border:1px solid #808080; margin: 10px">
						<div class="row well" style="text-align:center; "><span style="font-weight: bold;">분류 별 수강인원</span></div>
						<div class="center-block" id="peopleCurChart" style="height:300px;">
                  		</div>
					</div>
	        	</div>
	        	<div class="row" >
	        		<div class="col-md-5" style="border:1px solid #808080; margin: 10px">
	        			<div class="row well" style="text-align:center; "><span style="font-weight: bold;">전체 남녀 인원</span></div>
	        			<div class="center-block" id="averageAgeChart" style="height:300px;">
                  		</div>
	        		</div>
					<div class="col-md-5" style="border:1px solid #808080; margin: 10px">
						<div class="row well" style="text-align:center; "><span style="font-weight: bold;"> 분류 별 단가 평균</span></div>
						<div class="center-block" id="priceCurChart" style="height:300px; ">
                  		</div>
					</div>
	        	</div>
	        </div>
	    </div>
    </div>
    
    <script>
		//resize
		var contentsArea_h, window_h, menuArea_h;
	
	    window_h = $( window ).height(); 
	    menuArea_h = $( "#menuArea" ).height(); 
	    contentsArea_h=$( "#contentsArea" ).height( $( "#contentsArea" ).height() - menuArea_h - 25); 
	    
	    
	    $( window ).resize( function () {
	        window_h = $( window ).height(); 
	        
		    menuArea_h = $( "#menuArea" ).height(); 
		
		    contentsArea_h=$( "#contentsArea" ).height( window_h - menuArea_h -25); 
	
	    } );	
	</script>
</body>
</html>