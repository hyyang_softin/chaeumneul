//var org_view = {
inapp.add( "org_view", {
    init: function () {
        this.create();
        this.button();
        this.chartCreate();
        this.resize();
    },
    actionMap: {
        "tree_view": {
            "click_cell": function ( tree_data ) {
                this.selectNode( tree_data.data.pkey, true );
            },
            "click_tree_expand": function ( tree_data ) {
                this.expandNode( tree_data.data.key, tree_data.data.expand );
            },
            "dbl_click": function ( tree_data ) {
            	var node = viewOrg.nodes( tree_data.data.pkey );
            	
            	this.addChild(node, "cur_name");
            	this.toPopup(node, "cur_name");
            }
        }
    },
    resize: function () {
        var orgView, contentsArea_h, button_view_h, menuArea_h;

        h = $(window).height();
        
        menuArea_h = $( "#menuArea" ).height(); 
        button_view_h = $( "#button_view" ).height(); // 버튼의 높이를 구한다.
        
        $( "#viewOrg" ).height( h -  menuArea_h - button_view_h - 25); //둘을 뺀다.
        
        viewOrg.requestUpdate();
        
        $( window ).resize( function () {
        	h = $(window).height();
            
            menuArea_h = $( "#menuArea" ).height(); 
            contentsArea_h = $( "#contentsArea" ).height(); //지금 윈도우의 높이를 구한다.
            button_view_h = $( "#button_view" ).height(); // 버튼의 높이를 구한다.
            
            $( "#viewOrg" ).height( h -  menuArea_h - button_view_h - 25); //둘을 뺀다.
            
            viewOrg.requestUpdate();
        });
        
        return;
    },
    create: function () {
        var option, self;

        self = this;

        option = {
            layout: {
                level: false, // 레벨 정렬 사용여부
                supporter: true, // 보조자 사용여부
                layerSpacing : 20,
                fromPortOffset: 1
            },
            partsLoader: {
                scrollingNodeDrawType: "ALL"
            },
            expandButtonVisible: false,
            defaultTemplate: {
                link: { // 연결선 스타일
                    style: {
                        borderWidth: 2,
                        borderColor: "#CCC",
                        borderStyle: "solid"
                    }
                }
            },
            event: {
            	onClick: function ( evt ) {
                    var node, jsonData, nodeKey;
                     //노드가 아닐경우 실행안함
                    if ( !evt.key ) {
                        return
                    }

                    if ( evt.isMember ) {
                        node = evt.member;
                    } else {
                        node = evt.node;
                    }
                    //라벨일 경우 실행안함
                    if ( node.isLabel() ) {
                        return;
                    }
                    
                    self.toPopup( node , evt.binding );

                    self.addChild( node , evt.binding );
           
                },
                onExpandButtonClick: function ( evt ) {
                     //확장 버튼 클릭시 발생되는 이벤트.
                     //키값과 열/닫힘 상태 전달
                    inapp.raise( self.name, {
                        action: "click_node_expand",
                        key: evt.key,
                        isExpand: evt.isExpand
                    } );
                 },
                onDiagramLoadEnd: function ( evt ) {
                    //로드가 끝나면 발생되는 이벤트.
                    var jsonData, jsonArray;

                    jsonData = {};
                    jsonArray = [];

                    // 추가 조회면
                    if ( evt.append ) {
                    	return;
                    }

                    // 조직도 데이터를 트리 데이터 형식으로 가공
                    evt.org.nodes( "" ).foreach( function () {
                        var sData = {};

                        if ( !this.isLabel() && !this.isMember() ) {
                        	if(!this.supporter()){
                        		 this.fields().foreach( function () {

                                	 sData[ this.name() ] = this.value();
                                } );
                                sData.level = this.level();
                                jsonArray.push( sData );
                        	}
                        }
                    } );

                    jsonData.data = jsonArray;
                     //데이터 전달
                    inapp.raise( self.name, {
                        action: "org_data",
                        data: jsonData
                    } );
                }
           }
       }

       createINOrg( "viewOrg", option );

       this.load();
    },
    load: function () {
    	var org;
    	
    	org = viewOrg;
    	
        $.ajax({
        	type: "POST",
        	url : "/loadData.do",
        	dataType: "json",
        	contentType : "application/json; charset=UTF-8",
        	success: function(resData) {
        		org.loadJson({
                	data: resData,
                	success: function() {
                		org.scale("fitw");
                	}
                });
        	}
        });
    },
    button: function () {
        var orgView = this;
      
        $( "#openListData" ).on( "click", function () {
        	orgView.openAll();
        } );
        
        $( "#closeListData" ).on( "click", function () {       	
        	orgView.load();	
        } );
        
        $( "#largeView" ).on( "click", function () {
            orgView.zoom( "in" );
        } );

        $( "#smallView" ).on( "click", function () {
            orgView.zoom( "out" );
        } );

        $( "#autoView" ).on( "click", function () {
            orgView.scale( "fullfit" );
        } );

        $( "#savePop" ).on( "click", function () {

            inapp.raise( "org_view", {
                action: "select_save_type"
            } );
        } );

    },
    //전체 펼치기
    openAll : function ( ){
    	var appendData = [];
    	
    	org = viewOrg;
    	
    	//스트링이아닌 배열로 객체 넘기기로 나중엔 바꾸어야함.
    	//현재 존재하는 노드중에 자식이없는 노드만 찾아서 리턴한 후, foreach를 통해서 데이터를 불러온 이후에,
    	//appendData 배열에 추가하고, 데이터를 다받앗다면 orgview에 데이터를 append한다.
    	org.nodes().filter( function () {
    	    return !this.hasChildren();
    	} ).foreach( function () {
    	    var nodeKey;
    	    nodeKey = this.pkey();
    	    $.ajax( {
    	        type: "POST",
    	        data: nodeKey,
    	        async: false,
    	        url: "/loadMemberData.do",
    	        dataType: "json",
    	        contentType: "application/json; charset=UTF-8",
    	        success: function ( resData ) {
    	            $.merge( appendData, resData.orgData );
    	        }
    	    } );
    	} );
    	
    	org.loadJson( {
    	    data: {
    	        orgData: appendData
    	    },
    	    append: true,
    	    success: function() {
    	        org.scale("fitw");
    	    }
    	} );
    },
    zoom: function ( params ) {
        if ( params == "in" ) {
            viewOrg.zoomIn();
        } else {
            viewOrg.zoomOut();
        }
    },
    scale: function ( params ) {
        viewOrg.scale( params );
    },
    expandNode: function ( pKey, value ) {
        //노드 펼치기 기능.
        // 파라미터는 pKey는 노드 pKey값, value 는 포커싱여부 true false
        viewOrg.nodes( pKey ).expand( value );
    },
    selectNode: function ( pKey, value ) {
        //노드 포커싱 기능. 클릭 효과.
        // 파라미터는 pKey는 노드 pKey값, value 는 포커싱여부 true false
        var node = viewOrg.nodes( pKey );

        node.select( value );

    },
    toPopup: function( nodeData , bindingData){
    	var node, imgId, imgsrc, newStartTime, newEndTime;
    	
    	node= nodeData;
    	
    	jsonData = node.fields().toJSON();   
    	
    	//데이터가 level이 3이상이거나 supporter값이 있으면 사진 추가
    	if(node.level() == 3){
    		if( jsonData.start_time.value != null && jsonData.end_time.value != null ){
    			newStartTime = jsonData.start_time.value.split(' ')[0]
            	newEndTime = jsonData.end_time.value.split(' ')[0]
        		jsonData.start_time.value = newStartTime;
            	jsonData.end_time.value = newEndTime;
    		}else{
    			jsonData.start_time.value = "--- ";
            	jsonData.end_time.value = " ---";
    		}
        	imgId = jsonData.pkey.value;
        	imgsrc = jsonData.domain.value+jsonData.picture_name.value;
        	
        	if(jsonData.manager_name.value == null){
        		jsonData.manager_name.value = "없음";
        	}
    	}else{
    		imgId = jsonData.pkey.value;
    		if(jsonData.picture_name == null){
    			imgsrc = jsonData.domain.value+"img/photo/default.png";
    		}else{
    			imgsrc = jsonData.domain.value+jsonData.picture_name.value;
    		}
        	
    	}
    	
    	jsonData.img = {
    			value : "<img id= '"+imgId+"' class='img-responsive' src='"+imgsrc+"' style='width: 100px;'>"
    	}
    	
        //데이터를 전부 전달.
        inapp.raise( this.name, {
            action: "click_node",
            data: {
                binding: bindingData,
                fields: jsonData,
                level: node.level()
            }
        } );
    },
    //데이터 추가
    addChild: function( node , bindingData){
    	var self= this;
    	var nodeKey = node.fields("pkey").value();
    	if ( node.template() == "OrgBox2" && !node.hasChildren()){
    		$.ajax({
             	type: "POST",
             	data : nodeKey,
             	url : "/loadMemberData.do",
             	dataType: "json",
             	contentType : "application/json; charset=UTF-8",
             	success: function(resData) {
             		
             		node.org.loadJson( {
                        data: resData,
                        append: true
                    } );

             	}
             });
    	}
    },
    chartCreate: function () {
    	echarts.init( $( '#peopleNumChart' )[ 0 ] ).setOption(
    			{
                    grid: {
                        top: 'center',
                        left: '3%',
                        right: '5%',
                        bottom: '100%',
                        containLabel: true
                    },
                    xAxis: [ {
                            type: 'value',
                            splitLine: {
                                show: false
                            },
                            boundaryGap: [ 0, 0.1 ],
                            max: 100,
                            min: 0
                        },
                        {
                            type: 'value'
                        }
                    ],
                    yAxis: {
                        type: 'category',
                        splitArea: {
                            show: true
                        },
                        data: [ '' ]
                    },
                    series: [ {
                        name: 'incentive',
                        type: 'bar',
                        color: '#003366',
                        markLine: {
                            label: {
                                show: false
                            },
                            symbol: [ 'none', 'none' ],
                            lineStyle: {
                                color: '#d48265',
                                type: 'solid'
                            },
                            data: [ {
                                xAxis: 80
                            } ]
                        },
                        data: []
                    } ]
                }		
    	);
    	
    	echarts.init( $( '#genderChart' )[ 0 ] ).setOption(
    			{
    				color: ['#5793f3', '#d14a61'],
                    legend: {
                    	orient: 'vertical',
                        right: -5,
                        top: 20,
                        data: [ '남', '여' ]
                    },
                    series: [ {
                        name: 'evaluation',
                        type: 'pie',
                        radius: '65%',
                        center: [ '35%', '50%' ],
                        data: [],
                        label: {
                            show: true,
                            position: 'inside',
                            formatter: '{d}% \n ({c}명)'
                        }
                    } ]
                }	  		
    	);
    }
} );