/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package egovframework.com.chaeumneul.web;

import egovframework.com.chaeumneul.service.PNService;
import egovframework.com.chaeumneul.vo.PNCurriVO;
import egovframework.rte.fdl.property.EgovPropertyService;

import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.SessionStatus;
import org.springmodules.validation.commons.DefaultBeanValidator;

import com.google.gson.Gson;

/**
 * @Class Name : EgovSampleController.java
 * @Description : EgovSample Controller Class
 * @Modification Information
 * @
 * @  수정일      수정자              수정내용
 * @ ---------   ---------   -------------------------------
 * @ 2009.03.16           최초생성
 *
 * @author 개발프레임웍크 실행환경 개발팀
 * @since 2009. 03.16
 * @version 1.0
 * @see
 *
 *  Copyright (C) by MOPAS All right reserved.
 */

@Controller
public class PNController {

	/** EgovSampleService */
	@Resource(name = "PNService")
	private PNService PNService;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/** Validator */
	@Resource(name = "beanValidator")
	protected DefaultBeanValidator beanValidator;

	/**
	 * 글 목록을 조회한다. (pageing)
	 * @param searchVO - 조회할 정보가 담긴 SampleDefaultVO
	 * @param model
	 * @return "egovSampleList"
	 * @exception Exception
	 */
	
	//조회 화면
	@RequestMapping(value = "/chaeumneul.do", method = RequestMethod.GET)
	public String mainView(ModelMap model,HttpServletRequest request) throws Exception {
		//담을 객체를 vo에서 지정하고, 해당 vo에 데이터를 받아와 리스트에 넣고, model로 뿌려주고.
		
		return "chaeumneul/main";
	}
	
	//관리 화면
	@RequestMapping(value = "/management.do", method = RequestMethod.GET)
	public String managementView(ModelMap model,HttpServletRequest request) throws Exception {
		
		return "chaeumneul/management";
	}
	
	//통계 화면
	@RequestMapping(value = "/statistics.do", method = RequestMethod.GET)
	public String statisticsView(ModelMap model,HttpServletRequest request) throws Exception {
		
		return "chaeumneul/statistics";
	}
	
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(value = "/loadData.do", method = RequestMethod.POST, produces = "application/text; charset=UTF-8")
	public Object loadData( ModelMap model,HttpServletRequest request ) throws Exception {
		//경로를 가져옴
		String path = request.getSession().getServletContext().getRealPath("/");
		//템플릿 설정파일을 가져옴
	    JSONObject data = (JSONObject)loadTemplete(path+"data/inorg.json");
	    //데이터를 가져옴
		data.put("orgData",PNService.selectData());
		
		return data.toString();
	}

	
	public Object loadTemplete(String path) throws Exception{
		
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(new FileReader(path));
		
		return obj;
	}
}
