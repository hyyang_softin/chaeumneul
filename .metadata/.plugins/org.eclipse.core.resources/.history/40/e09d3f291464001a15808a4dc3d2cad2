/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package egovframework.com.chaeumneul.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import egovframework.com.chaeumneul.vo.CNCurriVO;
import egovframework.com.chaeumneul.vo.CNManagerVO;
import egovframework.com.chaeumneul.vo.CNStudentVO;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;

import javax.annotation.Resource;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;



/**
 * @Class Name : EgovSampleServiceImpl.java
 * @Description : Sample Business Implement Class
 * @Modification Information
 * @
 * @  수정일      수정자              수정내용
 * @ ---------   ---------   -------------------------------
 * @ 2009.03.16           최초생성
 *
 * @author 개발프레임웍크 실행환경 개발팀
 * @since 2009. 03.16
 * @version 1.0
 * @see
 *
 *  Copyright (C) by MOPAS All right reserved.
 */

@Service("CNService")
public class CNServiceImpl extends EgovAbstractServiceImpl implements CNService {

	private static final Logger LOGGER = LoggerFactory.getLogger(CNServiceImpl.class);

	/** SampleDAO */
	// TODO ibatis 사용
	//@Resource(name = "sampleDAO")
	//private SampleDAO sampleDAO;
	// TODO mybatis 사용
	@Resource(name="CNMapper")
	private CNMapper PNDAO;

	/** ID Generation */
	@Resource(name = "egovIdGnrService")
	private EgovIdGnrService egovIdGnrService;

//	/**
//	 * 글을 등록한다.
//	 * @param vo - 등록할 정보가 담긴 SampleVO
//	 * @return 등록 결과
//	 * @exception Exception
//	 */
//	@Override
//	public String insertSample(SampleVO vo) throws Exception {
//		LOGGER.debug(vo.toString());
//
//		/** ID Generation Service */
//		String id = egovIdGnrService.getNextStringId();
//		vo.setId(id);
//		LOGGER.debug(vo.toString());
//
//		sampleDAO.insertSample(vo);
//		return id;
//	}

//	/**
//	 * 글을 수정한다.
//	 * @param vo - 수정할 정보가 담긴 SampleVO
//	 * @return void형
//	 * @exception Exception
//	 */
//	@Override
//	public void updateSample(SampleVO vo) throws Exception {
//		sampleDAO.updateSample(vo);
//	}
//
//	/**
//	 * 글을 삭제한다.
//	 * @param vo - 삭제할 정보가 담긴 SampleVO
//	 * @return void형
//	 * @exception Exception
//	 */
//	@Override
//	public void deleteSample(SampleVO vo) throws Exception {
//		sampleDAO.deleteSample(vo);
//	}
//
//	/**
//	 * 글 목록을 조회한다.
//	 * @param searchVO - 조회할 정보가 담긴 VO
//	 * @return 글 목록
//	 * @exception Exception
//	 */
//	@Override
//	public List<?> selectSampleList(PNCurVO searchVO) throws Exception {
//		return sampleDAO.selectSampleList(searchVO);
//	}

	//교육과정 전체 조회 데이터
	@SuppressWarnings("unchecked")
	@Override
	public JSONArray selectOrgData() throws Exception {		
		//데이터 받아옴
		List<CNCurriVO> selectCurr = PNDAO.selectCurr();
		
		//정렬
		Collections.sort( selectCurr, new Comparator<CNCurriVO>() {
		   
				@Override
				public int compare(CNCurriVO o1, CNCurriVO o2) {
					
		            String[] arrayA = o1.getId().split("-");
		            String[] arrayB = o2.getId().split("-");
		            
		            int valA = Integer.parseInt(arrayA[0]+arrayA[1]);
		            int valB = Integer.parseInt(arrayB[0]+arrayB[1]);
		       
		            
		            return Integer.compare(valA,valB);
				}
		});
		
		//데이터 가공 
		JSONArray data_array = new JSONArray();
		
		//교육과정 부분
		for(int i=0; i<selectCurr.size(); i++){
			JSONObject data = new JSONObject();
			JSONObject layoutData = new JSONObject();
			JSONObject fieldsData = new JSONObject();
			
			layoutData.put("level", selectCurr.get(i).getClass_level());
			if(selectCurr.get(i).getTagType() == 1){
				layoutData.put("alignment", "busr");
				layoutData.put("wrapWidth", 300);
			}else if(selectCurr.get(i).getTagType() == 2)
			{
				layoutData.put("alignment", "busr");
				layoutData.put("wrapWidth", 300);
				layoutData.put("fromPortOffset", -100);
			}
		
			fieldsData.put("pkey", selectCurr.get(i).getId());	
			if(selectCurr.get(i).getClass_name() != null || selectCurr.get(i).getClass_name() != ""){
				fieldsData.put("rkey", selectCurr.get(i).getClass_name());
			}
			fieldsData.put("cur_name", selectCurr.get(i).getName());
			fieldsData.put("manager_name", selectCurr.get(i).getManager());
			
			data.put("layout", layoutData);
			if(selectCurr.get(i).getTagType() == 0){
				data.put("template", "OrgBox1");
			}else if(selectCurr.get(i).getTagType() == 1){
				data.put("template", "OrgBox1");
			}else if(selectCurr.get(i).getTagType() == 2){
				data.put("template", "OrgBox2");
			}
			data.put("fields", fieldsData);
			
	
			data_array.add(data);
		}
		
		
		return data_array;
	}

	//특정 강좌의 멤버조회
	@SuppressWarnings("unchecked")
	@Override
	public JSONArray selectOrgMemberData(String pKey) throws Exception {
		
		List<CNManagerVO> selectManager = PNDAO.selectManager(pKey);
		List<CNStudentVO> selectStudent = PNDAO.selectStudent(pKey);
		JSONArray data_array = new JSONArray();
		//강사 코드
		String ManagerCod = "";
		
		//강사 서치
		for(int i=0; i<selectManager.size(); i++){
			JSONObject data = new JSONObject();
			JSONObject layoutData = new JSONObject();
			JSONObject fieldsData = new JSONObject();
								
			layoutData.put("supporter", 2);
			layoutData.put("level", 4);
			
			fieldsData.put("pkey", selectManager.get(i).getMember_cod()+"-"+selectManager.get(i).getID());
			ManagerCod= selectManager.get(i).getMember_cod()+"-"+selectManager.get(i).getID();
			fieldsData.put("rkey", pKey);
			fieldsData.put("name", selectManager.get(i).getName());
					
			data.put("layout", layoutData);	
			data.put("template", "MemberBox");
			data.put("fields", fieldsData);
			
			data_array.add(data);
		}
		
		//학생 서치
		for(int i=0; i<selectStudent.size(); i++){
			JSONObject data = new JSONObject();
			JSONObject layoutData = new JSONObject();
			JSONObject fieldsData = new JSONObject();
								
			layoutData.put("supporter", 2);

			fieldsData.put("pkey", selectStudent.get(i).getMember_cod()+"-"+selectStudent.get(i).getID());	
			fieldsData.put("mkey", ManagerCod);
			fieldsData.put("name", selectStudent.get(i).getName());
					
			data.put("layout", layoutData);	
			data.put("template", "MemberBox");
			data.put("fields", fieldsData);
					
			data_array.add(data);
		}
				
		
		return data_array;
	}
}
