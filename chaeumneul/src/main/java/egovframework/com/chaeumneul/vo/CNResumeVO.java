package egovframework.com.chaeumneul.vo;

import java.io.Serializable;

public class CNResumeVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5L;
	
	private int ID = 0;
	private String skill = "";
	private String self = "";
	
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public String getSkill() {
		return skill;
	}
	public void setSkill(String skill) {
		this.skill = skill;
	}
	public String getSelf() {
		return self;
	}
	public void setSelf(String self) {
		this.self = self;
	}
	
	
}
