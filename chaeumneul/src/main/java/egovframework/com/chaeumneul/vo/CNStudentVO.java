package egovframework.com.chaeumneul.vo;

import java.io.Serializable;

public class CNStudentVO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;
	
	private int ID = 0;
	private String name = "";
	private int gender = 0;
	private int age = 0;
	private String phone_num = "";
	private String regi_cur = "";
	private String email = "";
	private String address = "";
	private int member_cod = 0;
	private String picture_name = "";
	private String sub = "";
	
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getGender() {
		return gender;
	}
	public void setGender(int gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getPhone_num() {
		return phone_num;
	}
	public void setPhone_num(String phone_num) {
		this.phone_num = phone_num;
	}
	public String getRegi_cur() {
		return regi_cur;
	}
	public void setRegi_cur(String regi_cur) {
		this.regi_cur = regi_cur;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getMember_cod() {
		return member_cod;
	}
	public void setMember_cod(int member_cod) {
		this.member_cod = member_cod;
	}
	public String getPicture_name() {
		return picture_name;
	}
	public void setPicture_name(String picture_name) {
		this.picture_name = picture_name;
	}
	public String getSub() {
		return sub;
	}
	public void setSub(String sub) {
		this.sub = sub;
	}

}
