/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package egovframework.com.chaeumneul.service;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import egovframework.com.chaeumneul.vo.CNCurriVO;
import egovframework.com.chaeumneul.vo.CNManagerVO;
import egovframework.com.chaeumneul.vo.CNResumeVO;
import egovframework.com.chaeumneul.vo.CNStudentVO;
import egovframework.com.chaeumneul.vo.sendEmailVO;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import egovframework.rte.fdl.idgnr.EgovIdGnrService;

import javax.annotation.Resource;

import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.SimpleEmail;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;




/**
 * @Class Name : EgovSampleServiceImpl.java
 * @Description : Sample Business Implement Class
 * @Modification Information
 * @
 * @  수정일      수정자              수정내용
 * @ ---------   ---------   -------------------------------
 * @ 2009.03.16           최초생성
 *
 * @author 개발프레임웍크 실행환경 개발팀
 * @since 2009. 03.16
 * @version 1.0
 * @see
 *
 *  Copyright (C) by MOPAS All right reserved.
 */

@Service("CNService")
public class CNServiceImpl extends EgovAbstractServiceImpl implements CNService {

	private static final Logger LOGGER = LoggerFactory.getLogger(CNServiceImpl.class);

	private static final String domin = "/";
	
	/** SampleDAO */
	// TODO ibatis 사용
	//@Resource(name = "sampleDAO")
	//private SampleDAO sampleDAO;
	// TODO mybatis 사용
	@Resource(name="CNMapper")
	private CNMapper PNDAO;

	/** ID Generation */
	@Resource(name = "egovIdGnrService")
	private EgovIdGnrService egovIdGnrService;

	//교육과정 전체 조회 데이터
	@SuppressWarnings("unchecked")
	@Override
	public JSONArray selectOrgData() throws Exception {		
		//데이터 받아옴
		List<CNCurriVO> selectCurr = PNDAO.selectCurr();

		//정렬
		Collections.sort( selectCurr, new Comparator<CNCurriVO>() {
		   
				@Override
				public int compare(CNCurriVO o1, CNCurriVO o2) {
					
		            String[] arrayA = o1.getId().split("-");
		            String[] arrayB = o2.getId().split("-");
		            
		            int valA = Integer.parseInt(arrayA[0]+arrayA[1]);
		            int valB = Integer.parseInt(arrayB[0]+arrayB[1]);
		       
		            
		            return Integer.compare(valA,valB);
				}
		});
		
		//데이터 가공 
		JSONArray data_array = new JSONArray();
		
		//교육과정 부분
		for(int i=0; i<selectCurr.size(); i++){
			JSONObject data = new JSONObject();
			JSONObject layoutData = new JSONObject();
			JSONObject fieldsData = new JSONObject();
			JSONObject endMarkData = new JSONObject();
			JSONObject endMarkStyleData = new JSONObject();
			
			String pkey = selectCurr.get(i).getId();
			List<CNManagerVO> selectManager = PNDAO.selectManager(pkey);
			int studentNum = PNDAO.selectStudentNum(pkey);
			
			layoutData.put("level", selectCurr.get(i).getClass_level());
			if(selectCurr.get(i).getTagType() >= 1){
				layoutData.put("alignment", "busr");
				layoutData.put("wrapWidth", 300);
			}
		
			fieldsData.put("pkey", pkey);	
			if(selectCurr.get(i).getClass_name() != null || selectCurr.get(i).getClass_name() != ""){
				fieldsData.put("rkey", selectCurr.get(i).getClass_name());
			}
			fieldsData.put("cur_name", selectCurr.get(i).getName());
			if(selectManager.size()> 0){
				fieldsData.put("manager_name", selectManager.get(0).getName());
			}else{
				fieldsData.put("manager_name", "");
			}
			fieldsData.put("contents", selectCurr.get(i).getContents());
			fieldsData.put("price", selectCurr.get(i).getPrice());
			fieldsData.put("start_time", selectCurr.get(i).getStart_time().split(" ")[0]);
			fieldsData.put("end_time", selectCurr.get(i).getEnd_time().split(" ")[0]);
			fieldsData.put("person_max_num", selectCurr.get(i).getPerson_num());
			if(selectManager.size() > 0){
				if(selectManager.get(0).getPicture_name() != "" ){
					fieldsData.put("picture_name", "img/photo/"+selectManager.get(0).getPicture_name());
				}else{
					fieldsData.put("picture_name", "img/photo/default.png");
				}
			}else{
				fieldsData.put("picture_name", "img/photo/default.png");
			}
			fieldsData.put("domain", domin);
			fieldsData.put("person_num", studentNum);
			fieldsData.put("manConut", PNDAO.selectStudentMan(pkey));
			fieldsData.put("womanCount", PNDAO.selectStudentWoman(pkey));
			fieldsData.put("personRatio",  studentNum +" / "+selectCurr.get(i).getPerson_num());
			if(studentNum == selectCurr.get(i).getPerson_num()){
				endMarkStyleData.put("backgroundColor", "red");
				endMarkStyleData.put("visible", true);
				endMarkData.put("value", "마감");
				endMarkData.put("style", endMarkStyleData);
				fieldsData.put("end_mark",  endMarkData);
			}else if(studentNum+3 > selectCurr.get(i).getPerson_num()){
				endMarkStyleData.put("backgroundColor", "orange");
				endMarkStyleData.put("visible", true);
				endMarkData.put("value", "마감\n임박");
				endMarkData.put("style", endMarkStyleData);
				fieldsData.put("end_mark",  endMarkData);	
			}
			
			if(fieldsData.get("rkey").equals("0000-0000")){
				fieldsData.put("curr_num", PNDAO.selectCurrInviClass(selectCurr.get(i).getId()));
			}
			
			data.put("layout", layoutData);
			if(selectCurr.get(i).getTagType() == 0){
				data.put("template", "OrgBox1");
			}else if(selectCurr.get(i).getTagType() == 1){
				data.put("template", "OrgBox3");
			}else if(selectCurr.get(i).getTagType() == 2){
				data.put("template", "OrgBox2");
			}
			data.put("fields", fieldsData);
			
	
			data_array.add(data);
		}
		
		
		return data_array;
	}

	//특정 강좌의 멤버조회
	@SuppressWarnings("unchecked")
	@Override
	public JSONArray selectOrgMemberData(String pKey) throws Exception {
		
		List<CNManagerVO> selectManager = PNDAO.selectManager(pKey);
		List<CNStudentVO> selectStudent = PNDAO.selectStudent(pKey);
		JSONArray data_array = new JSONArray();
		//강사 코드
		String ManagerCod = "";
		
		
		if(selectManager.size() > 0){
		
			//강사 서치
			for(int i=0; i<selectManager.size(); i++){
				JSONObject data = new JSONObject();
				JSONObject layoutData = new JSONObject();
				JSONObject fieldsData = new JSONObject();
				
				layoutData.put("level", 4);
				ManagerCod= selectManager.get(i).getMember_cod()+"-"+selectManager.get(i).getID();
				fieldsData.put("pkey", ManagerCod);
				fieldsData.put("rkey", pKey);
				fieldsData.put("emp_nm", selectManager.get(i).getName());
				if(selectManager.get(i).getGender() == 0){
					fieldsData.put("gender", "남");
				}else if(selectManager.get(i).getGender() == 1){
					fieldsData.put("gender", "여");
				}
				if(selectManager.get(i).getPicture_name() != "" ){
					fieldsData.put("picture_name", "img/photo/"+selectManager.get(i).getPicture_name());
				}
				fieldsData.put("age", selectManager.get(i).getAge());
				fieldsData.put("phone_num", selectManager.get(i).getPhone_num());
				fieldsData.put("member_cod", selectManager.get(i).getMember_cod());
				fieldsData.put("sub", selectManager.get(i).getSub());
				fieldsData.put("domain", domin);
						
				data.put("layout", layoutData);	
				data.put("template", "MemberBox");
				data.put("fields", fieldsData);
				
				data_array.add(data);
			}
			
			//학생 서치
			for(int i=0; i<selectStudent.size(); i++){
				JSONObject data = new JSONObject();
				JSONObject layoutData = new JSONObject();
				JSONObject fieldsData = new JSONObject();
									
				layoutData.put("supporter", 2);
				layoutData.put("level", 4);
				
				fieldsData.put("pkey", selectStudent.get(i).getMember_cod()+"-"+selectStudent.get(i).getID());	
				fieldsData.put("mkey", ManagerCod);
				fieldsData.put("emp_nm", selectStudent.get(i).getName());
				if(selectStudent.get(i).getGender() == 0){
					fieldsData.put("gender", "남");
				}else if(selectStudent.get(i).getGender() == 1){
					fieldsData.put("gender", "여");
				}
				fieldsData.put("age", selectStudent.get(i).getAge());
				fieldsData.put("phone_num", selectStudent.get(i).getPhone_num());
				if(selectStudent.get(i).getPicture_name() != "" ){
					
					fieldsData.put("picture_name", "img/photo/"+selectStudent.get(i).getPicture_name());
				}
				fieldsData.put("member_cod", selectStudent.get(i).getMember_cod());
				fieldsData.put("sub", selectStudent.get(i).getSub());
				fieldsData.put("domain", domin);
				
				data.put("layout", layoutData);	
				data.put("template", "MemberBox");
				data.put("fields", fieldsData);
						
				data_array.add(data);
			}
		}
				
		
		return data_array;
	}

	//교육과정 조회(분류 제외)
	@SuppressWarnings("unchecked")
	@Override
	public JSONArray selectCurrData() throws Exception {
		HashMap<String, String> rkeyMap = new HashMap<String, String>();
		List<CNCurriVO> selectCurr = PNDAO.selectCurr();
		//정렬
		
		long timestamp = new java.util.Date().getTime();
		
		Collections.sort( selectCurr, new Comparator<CNCurriVO>() {
				   
			@Override
			public int compare(CNCurriVO o1, CNCurriVO o2) {
							
				String[] arrayA = o1.getId().split("-");
				String[] arrayB = o2.getId().split("-");
				            
				int valA = Integer.parseInt(arrayA[0]+arrayA[1]);
				int valB = Integer.parseInt(arrayB[0]+arrayB[1]);
				       
				            
				return Integer.compare(valA,valB);
			}
		});
		
		//분류 해쉬맵
		for(int i=0; i<selectCurr.size(); i++){
			if(selectCurr.get(i).getClass_name().equals("0000-0000")){
				rkeyMap.put(selectCurr.get(i).getId(), selectCurr.get(i).getName());
			}
		}
		//데이터 가공 
		JSONArray data_array = new JSONArray();
		//교육과정 부분
		for(int i=0; i<selectCurr.size(); i++){
			
			if(selectCurr.get(i).getClass_name()== "" || selectCurr.get(i).getClass_name().equals("0000-0000")){
				continue;
			}else{
				JSONObject data = new JSONObject();
				String curr_cod = selectCurr.get(i).getId();
				List<CNManagerVO> selectManager = PNDAO.selectManager(curr_cod);
				
				data.put("pkey", curr_cod);
				data.put("class_name", rkeyMap.get(selectCurr.get(i).getClass_name()).toString());
				data.put("cur_name", selectCurr.get(i).getName());
				if(selectManager.size() < 1){
					data.put("manager_name", "");
					data.put("manager_cod", "");
					data.put("img", "<img src="+domin+"img/photo/default.png"+"?ts=" + timestamp +"' style='height: 100px; width: 100px;'>");
				}else{
					data.put("manager_name", selectManager.get(0).getName());
					data.put("manager_cod", selectManager.get(0).getID());
					data.put("img", "<img src="+domin+"img/photo/"+selectManager.get(0).getPicture_name()+"?ts=" + timestamp +"' style='height: 100px; width: 100px;'>");
				}
				data.put("price", selectCurr.get(i).getPrice());
				data.put("contents", selectCurr.get(i).getContents());
				data.put("start_time", selectCurr.get(i).getStart_time());
				data.put("end_time", selectCurr.get(i).getEnd_time());
				data.put("person_num", selectCurr.get(i).getPerson_num());
				data.put("persons", PNDAO.selectStudentNum(selectCurr.get(i).getId()));
				data.put("man", PNDAO.selectStudentMan(curr_cod));
				data.put("woman", PNDAO.selectStudentWoman(curr_cod));
				
				data_array.add(data);
			}
			
		}
		
		return data_array;
	}

	//교육과정 데이터 CRUD
	@Override
	public int saveCurrData(Map<Object, JSONObject[]> data) throws Exception {
		
		for(int i=0; i<data.get("data").length; i++){
				
			JSONObject indiData = data.get("data")[i];
	
			if(indiData.get("sStatus").equals("I")){
				
				String class_num = indiData.get("pkey").toString().substring(0,4) + "-0000";
					
				List<CNCurriVO> selectCurrInvi = PNDAO.selectCurrInvi(class_num);
					
				if(selectCurrInvi.size() < 1){
					CNCurriVO Newvo = new CNCurriVO();
						
					Newvo.setId(class_num);
					Newvo.setName(indiData.get("class_name").toString());
					Newvo.setClass_name("0000-0000");
					Newvo.setClass_level(2);
					Newvo.setTagType(1);
						
					PNDAO.insertCurr(Newvo);
						
				}
					
				CNCurriVO vo = new CNCurriVO();
					
				String managerName = indiData.get("manager_name").toString();
				String curr_cod = indiData.get("pkey").toString();
					
				vo.setId(indiData.get("pkey").toString());
				vo.setName(indiData.get("cur_name").toString());
				vo.setClass_name(class_num);
				vo.setClass_level(3);
				vo.setTagType(2);
				vo.setManager(managerName);
				if(managerName != ""){	//이름이 있으면
					List<CNManagerVO> selectManager = PNDAO.selectManager(curr_cod);
					//기존 강사가 있는지 여부 체크
					if( selectManager.size() > 0){
						// 있으면 기존 강사의 담당강의 데이터 없앰
						int oldManger =selectManager.get(0).getID();
						
						CNManagerVO oldMangerVo = new CNManagerVO();
						
						oldMangerVo.setRegi_cur("");
						oldMangerVo.setID(oldManger);
							
						PNDAO.updateManagerRegicurr(oldMangerVo);
					}
					// 강의에 강사 배정
					CNManagerVO newMangerVo = new CNManagerVO();
					
					newMangerVo.setRegi_cur(indiData.get("pkey").toString());
					newMangerVo.setID(Integer.parseInt(indiData.get("manager_cod").toString()));
					
					PNDAO.updateManagerRegicurr(newMangerVo);
				}
				vo.setContents(indiData.get("contents").toString());
				if(indiData.get("price").toString() != ""){
					vo.setPrice(Integer.parseInt(indiData.get("price").toString()));
				}
				if(indiData.get("start_time").toString() != ""){
					vo.setStart_time(transformDate(indiData.get("start_time").toString()));
				}
				if(indiData.get("end_time").toString() != ""){
					vo.setEnd_time(transformDate(indiData.get("end_time").toString()));
				}
				if(indiData.get("person_num").toString() != ""){
					vo.setPerson_num(Integer.parseInt(indiData.get("person_num").toString()));
						
				}
				PNDAO.insertCurr(vo);
				
			}else if(indiData.get("sStatus").equals("U")){
			
				CNCurriVO Newvo = new CNCurriVO();
				CNCurriVO vo = new CNCurriVO();
				
				String class_num = indiData.get("pkey").toString().substring(0,4) + "-0000";
					
				Newvo.setId(class_num);
				Newvo.setName(indiData.get("class_name").toString());
				
				PNDAO.updateClassCurr(Newvo);
				
				String managerName = indiData.get("manager_name").toString();
				String curr_cod = indiData.get("pkey").toString();
				
				vo.setId(curr_cod);
				vo.setName(indiData.get("cur_name").toString());
				vo.setClass_name(class_num);
				vo.setManager(managerName);
				if(managerName != "" ){ 
					List<CNManagerVO> selectManager = PNDAO.selectManager(curr_cod);
				
					if( selectManager.size() > 0){
						int oldManger =selectManager.get(0).getID();
							
						CNManagerVO oldMangerVo = new CNManagerVO();
							
						oldMangerVo.setRegi_cur("");
						oldMangerVo.setID(oldManger);
							
						PNDAO.updateManagerRegicurr(oldMangerVo);
					}
					
					CNManagerVO newMangerVo = new CNManagerVO();
						
					newMangerVo.setRegi_cur(indiData.get("pkey").toString());
					newMangerVo.setID(Integer.parseInt(indiData.get("manager_cod").toString()));
						
					PNDAO.updateManagerRegicurr(newMangerVo);
				}
				vo.setContents(indiData.get("contents").toString());
				if(indiData.get("price").toString() != ""){
					vo.setPrice(Integer.parseInt(indiData.get("price").toString()));
				}
				if(indiData.get("start_time").toString() != ""){
					vo.setStart_time(transformDate(indiData.get("start_time").toString()));
				}
				if(indiData.get("end_time").toString() != ""){
					vo.setEnd_time(transformDate(indiData.get("end_time").toString()));
				}
				if(indiData.get("person_num").toString() != ""){
					vo.setPerson_num(Integer.parseInt(indiData.get("person_num").toString()));	
				}
					
				PNDAO.updateCurr(vo);
					
			}else if(indiData.get("sStatus").equals("D")){
				String pkey = indiData.get("pkey").toString();
				PNDAO.deleteCurr(indiData.get("pkey").toString());
				
				List<CNManagerVO> selectManager = PNDAO.selectManager(pkey);
				
				if( selectManager.size() > 0 ){
					int oldManger =selectManager.get(0).getID();
					
					CNManagerVO oldMangerVo = new CNManagerVO();
						
					oldMangerVo.setRegi_cur("");
					oldMangerVo.setID(oldManger);
						
					PNDAO.updateManagerRegicurr(oldMangerVo);
				}
					
				String class_num = indiData.get("pkey").toString().substring(0,4) + "-0000";
					
				if(PNDAO.selectCurrInviClass(class_num) ==0){
					PNDAO.deleteCurr(class_num);
				}
			}
		}
	
		return 1;
	}
	
	//해당 강좌 수강생 조회
	@SuppressWarnings("unchecked")
	@Override
	public JSONArray selectStudentByCurrList(String pkey) throws Exception {
		// TODO Auto-generated method stub
		//데이터 받아옴
		List<CNStudentVO> selectList = PNDAO.selectStudent(pkey);
		//데이터 가공 
		JSONArray data_array = new JSONArray();
		
		for(int i=0; i<selectList.size(); i++){
			JSONObject data = new JSONObject();
			
			String pKey = selectList.get(i).getMember_cod()+"-"+selectList.get(i).getID();
			data.put("pkey", pKey);
			data.put("name", selectList.get(i).getName());
			data.put("phone_num", selectList.get(i).getPhone_num());
			data.put("regi_cur", selectList.get(i).getRegi_cur());
			
			data_array.add(data);
		}
				
		return data_array;
	}
	
	//해당 강좌가 아닌 수강생 조회
	@SuppressWarnings("unchecked")
	@Override
	public JSONArray selectStudentByCurrNotList(String pkey) throws Exception {
		// TODO Auto-generated method stub
		//데이터 받아옴
		List<CNStudentVO> selectNotList = PNDAO.selectCurNotStudent(pkey);
		//데이터 가공 
		JSONArray data_array = new JSONArray();
		
		for(int i=0; i<selectNotList.size(); i++){
			JSONObject data = new JSONObject();
			
			String pKey = selectNotList.get(i).getMember_cod()+"-"+selectNotList.get(i).getID();
			data.put("pkey", pKey);
			data.put("name", selectNotList.get(i).getName());
			data.put("phone_num", selectNotList.get(i).getPhone_num());
			data.put("regi_cur", selectNotList.get(i).getRegi_cur());
			
			data_array.add(data);
		}
				
		return data_array;
	}
	
	//수강생 강좌 변경
	@Override
	public int changeCurrStudents(Map<Object, JSONObject[]> data) throws Exception {
		
		System.out.println("11");
		String curr = data.get("data")[0].get("newcur").toString();

		System.out.println(curr);
		List<CNStudentVO> selectStudent = PNDAO.selectStudent(curr);
		
		//기존 해당강좌 전부 del
		if(selectStudent.size() > 0 ){
			for(int i=0; i< selectStudent.size(); i++){
				CNStudentVO vo = new CNStudentVO();
				
				vo.setID(selectStudent.get(i).getID());
				vo.setRegi_cur("");
				
				PNDAO.updateStudentCurr(vo);
			}
		}
	
		if(data.get("data").length != 0){
			for(int i=0; i<data.get("data").length; i++){
				
				JSONObject indiData = data.get("data")[i];
				System.out.println(indiData);
					
				int id = Integer.parseInt(indiData.get("pkey").toString().split("-")[1]);
					
				CNStudentVO vo = new CNStudentVO();
				
				vo.setID(id);
				vo.setRegi_cur(indiData.get("newcur").toString());
					
				PNDAO.updateStudentCurr(vo);
					
			}
		}
		System.out.println("된다2");
	
		return 1;
	}
	
	
	//강사 전체 검색
	@SuppressWarnings("unchecked")
	@Override
	public JSONArray selectManagerList() throws Exception {
		// TODO Auto-generated method stub
		//데이터 받아옴
		List<CNManagerVO> selectList = PNDAO.selectManagerList();
		//데이터 가공 
		JSONArray data_array = new JSONArray();
		
		long timestamp = new java.util.Date().getTime();
		
		//정렬
		Collections.sort( selectList, new Comparator<CNManagerVO>() {
		   
				@Override
				public int compare(CNManagerVO o1, CNManagerVO o2) {
					
		            int valA = o1.getID();
		            int valB = o2.getID();
		 
		            
		            return Integer.compare(valA, valB);
				}
		});
		
		for(int i=0; i<selectList.size(); i++){
			JSONObject data = new JSONObject();
			
			String pkey = selectList.get(i).getMember_cod()+"-"+selectList.get(i).getID();
			data.put("pkey", pkey);
			data.put("id", selectList.get(i).getID());
			data.put("name", selectList.get(i).getName());
			data.put("gender", selectList.get(i).getGender());
			data.put("age", selectList.get(i).getAge());
			data.put("phone_num", selectList.get(i).getPhone_num());
			data.put("regi_cur", selectList.get(i).getRegi_cur());
			data.put("sub", selectList.get(i).getSub());
			if(selectList.get(i).getPicture_name() != "" ){
				data.put("picture", "<img id= '"+pkey+"' src='"+domin+"img/photo/"+selectList.get(i).getPicture_name()+ "?ts=" + timestamp +"' style='height: 60px; width: 60px;'>");
			}else{
				data.put("picture", "<img id= '"+pkey+"' src='"+domin+"img/photo/default.png' style='height: 60px; width: 60px;'>");
			}
			data.put("picture_name", "");
			data.put("lobor_cost", selectList.get(i).getLobor_cost());
			
			data_array.add(data);
		}
				
		return data_array;
	}
	
	//강사 저장
	@Override
	public int saveManagerData(Map<Object, JSONObject[]> data) throws Exception {

		for(int i=0; i<data.get("data").length; i++){
				
			JSONObject indiData = data.get("data")[i];

			if(indiData.get("sStatus").equals("I")){
				int id = Integer.parseInt(indiData.get("pkey").toString().split("-")[1]);
				
				CNManagerVO vo = new CNManagerVO();
			
				vo.setID(id);
				vo.setName(indiData.get("name").toString());
				vo.setGender(Integer.parseInt(indiData.get("gender").toString()));
				
				if(indiData.get("age").toString() != ""){
					vo.setAge(Integer.parseInt(indiData.get("age").toString()));
				}
				vo.setPhone_num(indiData.get("phone_num").toString());
				vo.setRegi_cur(indiData.get("regi_cur").toString());
				
				if(indiData.get("picture_yn").toString().equals("Y")){
					
					vo.setPicture_name(indiData.get("pkey").toString()+".png");
				}
		
				vo.setSub(indiData.get("sub").toString());
				vo.setLobor_cost(indiData.get("lobor_cost").toString());
				
				PNDAO.insertManager(vo);		
				
			}else if(indiData.get("sStatus").equals("U")){
				String pkey = indiData.get("pkey").toString();
				int id = Integer.parseInt(pkey.split("-")[1]);
				
				CNManagerVO vo = new CNManagerVO();
				
				vo.setID(id);
				vo.setName(indiData.get("name").toString());
				vo.setGender(Integer.parseInt(indiData.get("gender").toString()));
				if(indiData.get("age").toString() != ""){
					vo.setAge(Integer.parseInt(indiData.get("age").toString()));
				}
				vo.setPhone_num(indiData.get("phone_num").toString());
				if(indiData.get("picture_yn").toString().equals("Y")){
					CNManagerVO pictureVo = new CNManagerVO();
					
					pictureVo.setID(id);
					pictureVo.setPicture_name(pkey+".png");
					
					PNDAO.updateManagerPicture(pictureVo);
				}
				vo.setSub(indiData.get("sub").toString());
				vo.setLobor_cost(indiData.get("lobor_cost").toString());
				
				PNDAO.updateManager(vo);
				
			}else if(indiData.get("sStatus").equals("D")){
				int id = Integer.parseInt(indiData.get("pkey").toString().split("-")[1]);
				String regi_cur = indiData.get("regi_cur").toString();
				
				List<CNCurriVO> currManger = PNDAO.selectCurrInvi(regi_cur);
				
				if( currManger.size() > 0){
					CNCurriVO delMangerCur = new CNCurriVO();
					
					delMangerCur.setManager("");
					delMangerCur.setId(regi_cur);
					
					PNDAO.updateCurrManager(delMangerCur);
				}
				
				PNDAO.deleteManager(id);
				
				
			}
		}
	
		return 1;
	}	
	
	//수강생 전체 조회
	@SuppressWarnings("unchecked")
	@Override
	public JSONArray selectStudentList() throws Exception {
		//데이터 받아옴
		List<CNStudentVO> selectList = PNDAO.selectStudentList();
		//데이터 가공 
		JSONArray data_array = new JSONArray();
				
		long timestamp = new java.util.Date().getTime();
				
		//정렬
		Collections.sort( selectList, new Comparator<CNStudentVO>() {
				   
			@Override
			public int compare(CNStudentVO o1, CNStudentVO o2) {
							
				int valA = o1.getID();
				int valB = o2.getID();
				 
				            
				return Integer.compare(valA, valB);
				}
		});
				
		for(int i=0; i<selectList.size(); i++){
			JSONObject data = new JSONObject();
					
			String pkey = selectList.get(i).getMember_cod()+"-"+selectList.get(i).getID();
			data.put("pkey", pkey);
			data.put("id", selectList.get(i).getID());
			data.put("name", selectList.get(i).getName());
			data.put("gender", selectList.get(i).getGender());
			data.put("age", selectList.get(i).getAge());
			data.put("phone_num", selectList.get(i).getPhone_num());
			data.put("regi_cur", selectList.get(i).getRegi_cur());
			if(selectList.get(i).getPicture_name() != "" ){
				data.put("picture", "<img id= '"+pkey+"' src='"+domin+"img/photo/"+selectList.get(i).getPicture_name()+ "?ts=" + timestamp +"' style='height: 60px; width: 60px;'>");
			}else{
				data.put("picture", "<img id= '"+pkey+"' src='"+domin+"img/photo/default.png' style='height: 60px; width: 60px;'>");
			}
			data.put("picture_name", "");
			data.put("email", selectList.get(i).getEmail());
			data.put("address", selectList.get(i).getAddress());
			data.put("sub", selectList.get(i).getSub());
					
			data_array.add(data);
		}
						
		return data_array;
	}
	
	//학생 저장
	@Override
	public int saveStudentData(Map<Object, JSONObject[]> data) throws Exception {
		
		for(int i=0; i<data.get("data").length; i++){
			
			JSONObject indiData = data.get("data")[i];
			
			if(indiData.get("sStatus").equals("I")){
				int id = Integer.parseInt(indiData.get("pkey").toString().split("-")[1]);
				
				CNStudentVO vo = new CNStudentVO();
			
				vo.setID(id);
				vo.setName(indiData.get("name").toString());
				vo.setGender(Integer.parseInt(indiData.get("gender").toString()));
				
				if(indiData.get("age").toString() != ""){
					vo.setAge(Integer.parseInt(indiData.get("age").toString()));
				}
				vo.setPhone_num(indiData.get("phone_num").toString());
				vo.setRegi_cur(indiData.get("regi_cur").toString());
				if(indiData.get("picture_yn").toString().equals("Y")){
					vo.setPicture_name(indiData.get("pkey").toString()+".png");
				}
				vo.setEmail(indiData.get("email").toString());
				vo.setAddress(indiData.get("address").toString());
				vo.setSub(indiData.get("sub").toString());
				
				PNDAO.insertStudent(vo);		
				
			}else if(indiData.get("sStatus").equals("U")){
				String pkey = indiData.get("pkey").toString();
				int id = Integer.parseInt(pkey.split("-")[1]);
				
				CNStudentVO vo = new CNStudentVO();
				
				vo.setID(id);
				vo.setName(indiData.get("name").toString());
				vo.setGender(Integer.parseInt(indiData.get("gender").toString()));
				if(indiData.get("age").toString() != ""){
					vo.setAge(Integer.parseInt(indiData.get("age").toString()));
				}
				vo.setPhone_num(indiData.get("phone_num").toString());
				if(indiData.get("picture_yn").toString().equals("Y")){
					CNStudentVO pictureVo = new CNStudentVO();
					
					pictureVo.setID(id);
					pictureVo.setPicture_name(pkey+".png");
					
					PNDAO.updateStudentPicture(pictureVo);
				}
				vo.setEmail(indiData.get("email").toString());
				vo.setAddress(indiData.get("address").toString());
				vo.setSub(indiData.get("sub").toString());
				
				PNDAO.updateStudent(vo);
				
			}else if(indiData.get("sStatus").equals("D")){
				int id = Integer.parseInt(indiData.get("pkey").toString().split("-")[1]);
				
				PNDAO.deleteStudent(id);
				
			}
		}
	
		return 1;
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public JSONObject statisticsDataManager() throws Exception {
	
		JSONArray data_array = new JSONArray();
		//교육과정 관련
		List<String> rkeyMap = new ArrayList();
		List<CNCurriVO> selectCurr = PNDAO.selectCurr(); 
		//정렬	
		Collections.sort( selectCurr, new Comparator<CNCurriVO>() {
				   
			@Override
			public int compare(CNCurriVO o1, CNCurriVO o2) {
							
				String[] arrayA = o1.getId().split("-");
				String[] arrayB = o2.getId().split("-");
				            
				int valA = Integer.parseInt(arrayA[0]+arrayA[1]);
				int valB = Integer.parseInt(arrayB[0]+arrayB[1]);
				       
				            
				return Integer.compare(valA,valB);
			}
		});
		
		//분류 해쉬맵
		for(int i=0; i<selectCurr.size(); i++){
			if(selectCurr.get(i).getClass_name().equals("0000-0000")){
				rkeyMap.add(selectCurr.get(i).getId());
			}
		}
		//데이터 가공 
		JSONObject newData = new JSONObject();
		for(int i=0; i< rkeyMap.size(); i++){
			String key = rkeyMap.get(i);
			
			int countPeople =0;
			int sumCurrPrice = 0; 
			JSONObject data = new JSONObject();
			
			for(int j = 0; j<selectCurr.size(); j++ ){
				if(selectCurr.get(j).getClass_name().equals(key)){
					//분류 별 인원수.
					int numPeopleCurr = PNDAO.selectStudentNum(selectCurr.get(j).getId());
					countPeople = countPeople + numPeopleCurr;  
		         	//분류별 단가 평균
		         	sumCurrPrice = sumCurrPrice+ selectCurr.get(j).getPrice();
				}	
			}
			int classinCur =  PNDAO.selectCurrInviClass(key);
			List<CNCurriVO> keyCurr = PNDAO.selectCurrInvi(key);
			data.put("curname", keyCurr.get(0).getName());
			data.put("countPeople", countPeople);
			data.put("diviCurrPrice", sumCurrPrice / classinCur);
			
			data_array.add(data);
		}
		
		JSONObject dataCount = new JSONObject();
		//학생수 강사수.
		dataCount.put("countStudent", PNDAO.selectCountStudent());
		dataCount.put("countManager", PNDAO.selectCountManager());
		dataCount.put("allCountMan", PNDAO.countManagerMan() + PNDAO.countStudentMan());
		dataCount.put("allCountWoman", PNDAO.countManagerWoman() + PNDAO.countStudentWoman());
		
		newData.put("curData", data_array);
		newData.put("dataCount", dataCount);
		newData.put("domain", domin);
		
		System.out.println(newData);
		
		return newData;
	}
	
	//하나의 수강생 데이터만 검색(이력서용)
	@Override
	public CNStudentVO selectOneStudent(String pkey) throws Exception {
	
		int Id=	Integer.parseInt(pkey.split("-")[1]);
		
		long timestamp = new java.util.Date().getTime();
		
		CNStudentVO data = PNDAO.selectOneStudent(Id);
		
		if(data.getPicture_name().toString() != "" ){
			String img = data.getPicture_name();
			data.setPicture_name(domin+"img/photo/"+img+ "?ts=" + timestamp);
		}else{
			data.setPicture_name(domin+"img/photo/default.png");
		}
		String phonehipn = phone(data.getPhone_num());
		
		data.setPhone_num(phonehipn);
		
		return data;
	}
	
	//이력서 검색
	@Override
	public CNResumeVO selectOneResume(String pkey) throws Exception {
		// TODO Auto-generated method stub
		int Id=	Integer.parseInt(pkey.split("-")[1]);
		
		List<CNResumeVO> selectOneResume =  PNDAO.selectOneResume(Id);
		
		CNResumeVO data = new CNResumeVO();
		if(selectOneResume.size() > 0){
			data.setSkill(selectOneResume.get(0).getSkill().toString());
			data.setSelf(selectOneResume.get(0).getSelf().toString());
		}else{
			data.setSkill("");
			data.setSelf("");
		}
		
		return data;
	}
	
	//이력서 저장
	@Override
	public int saveResumeData(Map<Object, Object> data) throws Exception {
		
		int id = Integer.parseInt(data.get("ID").toString());
		List<CNResumeVO> selectOneResume = PNDAO.selectOneResume(id);
		int result = 0;
		
		if(selectOneResume.size() > 0 ){
			CNResumeVO vo = new CNResumeVO();
			vo.setID(id);
			vo.setSkill(data.get("skill").toString());
			vo.setSelf(data.get("self").toString());
			
			System.out.println(vo.getID() + " : "+ vo.getSkill() + " : ");
			
			result = PNDAO.updateResume(vo);
		}else {
			CNResumeVO vo = new CNResumeVO();
			vo.setID(id);
			vo.setSkill(data.get("skill").toString());
			vo.setSelf(data.get("self").toString());
			
			result = PNDAO.insertResume(vo);
		}
		
		return result;
	}
	
	@Override
	public int sendResumeImpl(Map<Object, Object[]> sendData) {
		
		long beginTime = System.currentTimeMillis(); // 메일 보내는 시작 시간
		
        // HtmlEmail 객체생성
        HtmlEmail email = new HtmlEmail();

        // SMTP 서버 연결 설정
        email.setHostName("smtp.gmail.com");
        email.setSmtpPort(465);
        email.setAuthentication("ozakl2818@gmail.com", "ghduf104");

        // SMTP 보안 SSL, TLS 설정
        email.setSSLOnConnect(true); // SSL 사용 설정
        email.setStartTLSEnabled(true); // TLS 사용 설정
        
        
        String result = "fail";
        int success =0;
        try {
            // 보내는 사람 설정
            email.setFrom("ozakl2818@gmail.com", "채움늘", "utf-8");
        				
            // 받는 사람 설정
            email.addTo(sendData.get("address")[0].toString(), "수신자", "utf-8");
        	
            // 제목 설정
            email.setSubject("[채움늘]요청하신 이력서 보내드립니다~");

            // 본문 설정
            StringBuilder sb = new StringBuilder();
            sb.append("<html><body>");
            sb.append("요청하신 이력서들 입니다.<br>");
            
            for(int i=0; i< sendData.get("data").length; i++){

    			String datastr = sendData.get("data")[i].toString();
    			
    			JSONParser parser = new JSONParser();
    			Object obj = null;
    			try {
    				obj = parser.parse( datastr );
    			} catch (org.json.simple.parser.ParseException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
    			JSONObject jsonObj = (JSONObject) obj;
    			sb.append(jsonObj.get("name") + " : <a href='" + jsonObj.get("address")+"' target='_blank'>이력서 보기 </a><br>");
    	    }
            
            sb.append("</body></html>");
            
           // 문자셋 설정
            email.setCharset("utf-8"); 
            email.setHtmlMsg(sb.toString());
            
            // 메일 전송
            result = email.send();
            
        } catch (EmailException e) {
            e.printStackTrace();
        } finally {
            long execTime = System.currentTimeMillis() - beginTime;
            System.out.println("execTime : " + execTime);
            System.out.println("result : " + result);
        }
		
        if(!result.equals("fail")){
        	success = 1;
        }else{
        	success = 0;
        }
		
		return success;
	}
	
	//날짜 변환
	public String transformDate(String date) throws ParseException{
		SimpleDateFormat beforeFormat = new SimpleDateFormat("yyyymmdd");
        
	    // Date로 변경하기 위해서는 날짜 형식을 yyyy-mm-dd로 변경해야 한다.
	    SimpleDateFormat afterFormat = new SimpleDateFormat("yyyy-mm-dd");
	        
	    java.util.Date tempDate = null;
	        
	    // 현재 yyyymmdd로된 날짜 형식으로 java.util.Date객체를 만든다.
	    tempDate = beforeFormat.parse(date);
  
	    // java.util.Date를 yyyy-mm-dd 형식으로 변경하여 String로 반환한다.
	    String transDate = afterFormat.format(tempDate);
	    
	    return transDate;
	}
	
	//전화번호 형식 변환
	public static String phone(String src) {
	    if (src == null || src == "") {
	      return "";
	    }
	    if (src.length() == 8) {
	      return src.replaceFirst("^([0-9]{4})([0-9]{4})$", "$1-$2");
	    } else if (src.length() == 12) {
	      return src.replaceFirst("(^[0-9]{4})([0-9]{4})([0-9]{4})$", "$1-$2-$3");
	    }
	    return src.replaceFirst("(^02|[0-9]{3})([0-9]{3,4})([0-9]{4})$", "$1-$2-$3");
	}

}
