/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package egovframework.com.chaeumneul.service;

import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import egovframework.com.chaeumneul.vo.CNResumeVO;
import egovframework.com.chaeumneul.vo.CNStudentVO;

/**
 * @Class Name : EgovSampleService.java
 * @Description : EgovSampleService Class
 * @Modification Information
 * @
 * @  수정일      수정자              수정내용
 * @ ---------   ---------   -------------------------------
 * @ 2009.03.16           최초생성
 *
 * @author 개발프레임웍크 실행환경 개발팀
 * @since 2009. 03.16
 * @version 1.0
 * @see
 *
 *  Copyright (C) by MOPAS All right reserved.
 */
public interface CNService {

	JSONArray selectOrgData() throws Exception;
	
	JSONArray selectOrgMemberData(String pKey) throws Exception;
	
	JSONArray selectCurrData() throws Exception;
	
	int saveCurrData(Map<Object, JSONObject[]> data) throws Exception;
	
	JSONArray selectManagerList() throws Exception;

	JSONArray selectStudentByCurrList(String pkey) throws Exception;
	JSONArray selectStudentByCurrNotList(String pkey) throws Exception;
	
	int saveManagerData(Map<Object, JSONObject[]> data) throws Exception;
	
	JSONArray selectStudentList() throws Exception;
	
	int saveStudentData(Map<Object, JSONObject[]> data) throws Exception;
	
	int changeCurrStudents(Map<Object, JSONObject[]> data) throws Exception;
	
	CNStudentVO selectOneStudent(String pkey) throws Exception;
	
	CNResumeVO selectOneResume(String pkey) throws Exception;
	
	JSONObject statisticsDataManager() throws Exception;
	
	int saveResumeData(Map<Object, Object> data) throws Exception;
	
	int sendResumeImpl(Map<Object, Object[]> sendData);
}
