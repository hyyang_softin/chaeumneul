/*
 * Copyright 2011 MOPAS(Ministry of Public Administration and Security).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package egovframework.com.chaeumneul.service;

import java.util.List;

import egovframework.com.chaeumneul.vo.CNCurriVO;
import egovframework.com.chaeumneul.vo.CNManagerVO;
import egovframework.com.chaeumneul.vo.CNResumeVO;
import egovframework.com.chaeumneul.vo.CNStudentVO;
import egovframework.rte.psl.dataaccess.mapper.Mapper;

/**
 * sample에 관한 데이터처리 매퍼 클래스
 *
 * @author  표준프레임워크센터
 * @since 2014.01.24
 * @version 1.0
 * @see <pre>
 *  == 개정이력(Modification Information) ==
 *
 *          수정일          수정자           수정내용
 *  ----------------    ------------    ---------------------------
 *   2014.01.24        표준프레임워크센터          최초 생성
 *
 * </pre>
 */
@Mapper("CNMapper")
public interface CNMapper {

	//교육과정 전체 조회
	List<CNCurriVO> selectCurr() throws Exception;
	//교육과정 ID 조회
	List<CNCurriVO> selectCurrInvi(String pKey) throws Exception;
	// 교육과정 class_name을 가진 강좌가 몇개인지 조회
	int selectCurrInviClass(String class_name) throws Exception;
	// 특정 교육과정 학생수 조회
	int selectStudentNum(String regi_cur) throws Exception;
	// 특정 교육과정의 구성원 조회
	List<CNManagerVO> selectManager(String pKey) throws Exception;
	List<CNStudentVO> selectStudent(String pKey) throws Exception;
	// 특정 교육과정외의 수강생 조회
	List<CNStudentVO> selectCurNotStudent(String pKey) throws Exception;
	// 특정 교육과정의 남자수 조회
	int selectStudentMan(String pKey) throws Exception;
	//특정 교육과정의 여자수 조회
	int selectStudentWoman(String pKey) throws Exception;
	
	//교육과정 Insert
	void insertCurr(CNCurriVO vo) throws Exception;
	//교육과정 delete
	int deleteCurr(String Id) throws Exception;
	//교육과정 update
	int updateClassCurr(CNCurriVO vo) throws Exception;
	int updateCurr(CNCurriVO vo) throws Exception;
	int updateCurrManager(CNCurriVO vo) throws Exception;
	
	//강사 전체 조회
	List<CNManagerVO> selectManagerList() throws Exception;
	//강사 담당강좌'만' 변경
	int updateManagerRegicurr(CNManagerVO vo) throws Exception;
	//강사 Insert
	int insertManager(CNManagerVO vo) throws Exception;
	//강사 delete
	int deleteManager(int id) throws Exception;
	//강사 정보 변경 update
	int updateManager(CNManagerVO vo) throws Exception;
	int updateManagerPicture(CNManagerVO vo) throws Exception;
	
	//수강생 전체 조회
	List<CNStudentVO> selectStudentList() throws Exception;
	//수강생 Insert
	int insertStudent(CNStudentVO vo) throws Exception;
	//수강생 정보 변경 update
	int updateStudent(CNStudentVO vo) throws Exception;
	int updateStudentPicture(CNStudentVO vo) throws Exception;
	int updateStudentCurr(CNStudentVO vo) throws Exception;
	//수강생 delete
	int deleteStudent(int id) throws Exception;
	//한명의 수강생 데이터 가져오기
	CNStudentVO selectOneStudent(int id) throws Exception;
	//해당 이력서 항목 가져오기
	List<CNResumeVO> selectOneResume(int id) throws Exception;
	//수강생 이력서 추가.
	int insertResume(CNResumeVO vo) throws Exception;
	//수강생 이력서 수정.
	int updateResume(CNResumeVO vo) throws Exception;
	
	//통계용
	//총 학생수 (null아닌)
	int selectCountStudent() throws Exception;
	//총 강사수 (null아닌)
	int selectCountManager() throws Exception;
	//전체 남,여 인원
	int countManagerMan() throws Exception;
	int countManagerWoman() throws Exception;
	int countStudentMan() throws Exception;
	int countStudentWoman() throws Exception;
	
}
