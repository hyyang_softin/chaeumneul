/*
 * Copyright 2008-2009 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package egovframework.com.chaeumneul.web;

import egovframework.com.chaeumneul.service.CNService;
import egovframework.com.chaeumneul.vo.CNCurriVO;
import egovframework.com.chaeumneul.vo.CNManagerVO;
import egovframework.com.chaeumneul.vo.CNResumeVO;
import egovframework.com.chaeumneul.vo.CNStudentVO;
import egovframework.rte.fdl.property.EgovPropertyService;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springmodules.validation.commons.DefaultBeanValidator;

/**
 * @Class Name : EgovSampleController.java
 * @Description : EgovSample Controller Class
 * @Modification Information
 * @
 * @  수정일      수정자              수정내용
 * @ ---------   ---------   -------------------------------
 * @ 2009.03.16           최초생성
 *
 * @author 개발프레임웍크 실행환경 개발팀
 * @since 2009. 03.16
 * @version 1.0
 * @see
 *
 *  Copyright (C) by MOPAS All right reserved.
 */

@Controller
public class CNController {

	/** EgovSampleService */
	@Resource(name = "CNService")
	private CNService CNService;

	/** EgovPropertyService */
	@Resource(name = "propertiesService")
	protected EgovPropertyService propertiesService;

	/** Validator */
	@Resource(name = "beanValidator")
	protected DefaultBeanValidator beanValidator;

    private static final long LIMIT_SIZE = 10 * 1024 * 1024;
    
	/**
	 * 글 목록을 조회한다. (pageing)
	 * @param searchVO - 조회할 정보가 담긴 SampleDefaultVO
	 * @param model
	 * @return "egovSampleList"
	 * @exception Exception
	 */
	
	//조회 화면
	@RequestMapping(value = "/chaeumneul.do", method = RequestMethod.GET)
	public String mainView(ModelMap model ) throws Exception {
		//담을 객체를 vo에서 지정하고, 해당 vo에 데이터를 받아와 리스트에 넣고, model로 뿌려주고.
		System.out.println("1");
		return "/chaeumneul/main";
	}
	
	//관리 화면
	@RequestMapping(value = "/management.do", method = RequestMethod.GET)
	public String managementView(ModelMap model, CNCurriVO pkeyVO ) throws Exception {
		
		model.addAttribute("currVo" ,pkeyVO);
		
		return "/chaeumneul/managementcurr";
	}
	
	//강사 관리 화면
	@RequestMapping(value = "/managerview.do", method = RequestMethod.GET)
	public String managerview(ModelMap model, String Id ) throws Exception {
		
		model.addAttribute("managerVo" ,Id);
		return "/chaeumneul/managementmanager";
	}
	
	//수강생 관리 화면
	@RequestMapping(value = "/studentview.do", method = RequestMethod.GET)
	public String studentview(ModelMap model, String Id ) throws Exception {
		
		model.addAttribute("studentVo" , Id);
		return "/chaeumneul/managementstudent";
	}
	
	//통계 화면
	@RequestMapping(value = "/statistics.do", method = RequestMethod.GET)
	public String statisticsView(ModelMap model,HttpServletRequest request) throws Exception {
		
		return "/chaeumneul/statistics";
	}
	
	//이력서 수정 화면
	@RequestMapping(value = "/resumemodi.do", method = RequestMethod.GET)
	public String resumemodi(ModelMap model,String Id) throws Exception {
		
		model.addAttribute("key", "1");
		CNStudentVO vo = CNService.selectOneStudent(Id);
		model.addAttribute( "vo" , vo);
		
		CNResumeVO voResume = CNService.selectOneResume(Id);
		model.addAttribute( "voResume" , voResume);
		
		return "/chaeumneul/resume";
	}
	
	//이력서 조회 화면
	@RequestMapping(value = "/resumesend.do", method = RequestMethod.GET)
	public String resumesend(ModelMap model,String Id) throws Exception {
		model.addAttribute("key", "2");
		CNStudentVO vo = CNService.selectOneStudent(Id);
		model.addAttribute( "vo" , vo);
		
		CNResumeVO voResume = CNService.selectOneResume(Id);
		model.addAttribute( "voResume" , voResume);
		
		return "/chaeumneul/resumesend";
	}
	
	//조회화면 Org 데이터 조회
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(value = "/loadData.do", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	public Object loadData( HttpServletRequest request ) throws Exception {
		//경로를 가져옴
		System.out.println("2");
		String path = request.getSession().getServletContext().getRealPath("/");
		//템플릿 설정파일을 가져옴
	    JSONObject data = (JSONObject)loadTemplete(path+"data/inorgTemplate.json");
	    //데이터를 가져옴
		data.put("orgData",CNService.selectOrgData());
		
		return data;
	}

	//조회화면 org 추가 데이터 조회
	@ResponseBody
	@RequestMapping(value = "/loadMemberData.do", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	public Object loadMemberData(@RequestBody String pKey ) throws Exception {
	    Map<String,Object> data = new HashMap<String,Object>();
	
		data.put("orgData",CNService.selectOrgMemberData(pKey));
		
		return data;
	}

	//교육과정 조회
	@ResponseBody
	@RequestMapping(value = "/selectCurData.do", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	public Object selectCurData() throws Exception {
		Map<String,Object> data = new HashMap<String,Object>();
	
		data.put("data",CNService.selectCurrData());
		
		return data;
	}
	
	//교육과정 저장
	@ResponseBody
	@RequestMapping(value = "/saveCurData.do", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	public Object saveCurData(@RequestBody Map<Object, JSONObject[]> saveData ) throws Exception {
		int success =CNService.saveCurrData(saveData);
		if(success == 1){
			Object newData = selectCurData();
			
			return newData;
		}else{
			Map<String, String> failData =  new HashMap<String,String>();
			
			failData.put("data", "fail");
			return failData;
		}	
	}
		
	//강사 조회
	@ResponseBody
	@RequestMapping(value = "/selectMangerData.do", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	public Object selectMangerData() throws Exception {
		Map<String,Object> data = new HashMap<String,Object>();
	
		data.put("data",CNService.selectManagerList());
		
		return data;
	}
	
	//강사 저장
	@ResponseBody
	@RequestMapping(value = "/saveManagerData.do", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	public Object saveManagerData( @RequestBody Map<Object, JSONObject[]> saveData) throws Exception {
		
		int success =CNService.saveManagerData(saveData);
		
		if(success == 1){
			Object newData = selectMangerData();
			
			return newData;
		}else{
			Map<String, String> failData =  new HashMap<String,String>();
			
			failData.put("data", "fail");
			return failData;
		}
	}
	
	@ResponseBody
	@RequestMapping(value = "/addStudent1Data.do", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	public Object addStudent1Data( @RequestBody String pKey ) throws Exception {
		Map<String,Object> data = new HashMap<String,Object>();
		
		//이미 수강중인 사람 데이터
		data.put("data",CNService.selectStudentByCurrList(pKey));
		
		return data;
	}
	
	@ResponseBody
	@RequestMapping(value = "/addStudent2Data.do", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	public Object addStudent2Data( @RequestBody String pKey ) throws Exception {
		Map<String,Object> data = new HashMap<String,Object>();
		
		//수강 아닌사람 데이터
		data.put("data",CNService.selectStudentByCurrNotList(pKey));
		
		return data;
	}
	
	@ResponseBody
	@RequestMapping(value = "/changeStudents.do", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	public Object changeStudents( @RequestBody Map<Object, JSONObject[]> newData) throws Exception {
		System.out.println(newData);
		int success = CNService.changeCurrStudents(newData);
		
		return success;
	}
	
	//수강생 조회
	@ResponseBody
	@RequestMapping(value = "/selectStudentData.do", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	public Object selectStudentData() throws Exception {
		Map<String,Object> data = new HashMap<String,Object>();
		
		data.put("data",CNService.selectStudentList());
			
		return data;
	}
	
	//수강생 저장
	@ResponseBody
	@RequestMapping(value = "/saveStudentData.do", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	public Object saveStudentData( @RequestBody Map<Object, JSONObject[]> saveData) throws Exception {
		
		int success =CNService.saveStudentData(saveData);
		
		if(success == 1){
			Object newData = selectStudentData();
			
			return newData;
		}else{
			Map<String, String> failData =  new HashMap<String,String>();
			
			failData.put("data", "fail");
			return failData;
		}
	}

	//사진 저장
	@ResponseBody
	@RequestMapping(value = "/multiImageUpload.do", method = RequestMethod.POST)
	public int multiImageUpload(HttpServletRequest request , @RequestParam("name")List<String> names, @RequestParam("files")List<MultipartFile> images) throws IllegalStateException, IOException {
		long sizeSum = 0;
		String filepath = request.getSession().getServletContext().getRealPath("/")+"img/photo/";
		String uploadPath = "";
		
		for(int i =0 ; i<images.size(); i++){
			uploadPath="";
			String originalName = images.get(i).getOriginalFilename();
			//확장자 검사
			if(!isValidExtension(originalName)){
				return -1;
			}
			//용량 검사
			sizeSum += images.get(i).getSize();
			if(sizeSum >= LIMIT_SIZE) {
				return -2;
			}
			 
			 uploadPath = filepath+names.get(i).substring(1)+".png";
			 images.get(i).transferTo(new File(uploadPath));	 
			 
		}
	
		return 1;
	}
	
	//통계
	@ResponseBody
	@RequestMapping(value = "/statisticsData.do", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	public Object statisticsData() throws Exception {
		
		return CNService.statisticsDataManager();
	}
    
	//이력서 저장
	@ResponseBody
	@RequestMapping(value = "/saveResume.do", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	public Object saveResume( @RequestBody Map<Object, Object> saveData) throws Exception {
		
		int success = CNService.saveResumeData(saveData);
		
		return success;
	}
	
	//이력서 이메일 보내기
	@ResponseBody
	@RequestMapping(value = "/sendResume.do", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
	public Object sendResume(@RequestBody Map<Object, Object[]> sendData) throws Exception {
		
		int success = CNService.sendResumeImpl(sendData);
			
		return success;
	}	

	//required above jdk 1.7 - switch(String)
	private boolean isValidExtension(String originalName) {
		String originalNameExtension = originalName.substring(originalName.lastIndexOf(".") + 1);
		switch(originalNameExtension) {
			case "jpg":
			case "png":
			case "gif":
				return true;
		}
		return false;
	}
	
	//템플릿 로드
	public Object loadTemplete(String path) throws Exception{
		
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(new FileReader(path));
		
		return obj;
	}
}
