<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c"         uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"      uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<%@ taglib prefix="spring"    uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link rel = "icon" href ="<c:url value='/img/egovframework/chaeumneul/books.ico'/>">
	
  	<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>채움늘 교육과정 관리 패키지</title>
	
	<link rel="stylesheet" href="<c:url value='/js/lib/bootstrap/css/bootstrap.min.css'/>">
	
	<style>
	        html,
	        body {
	            height: 100% !important;
	        }
			
			.nav-tabs.nav-justified > li > a {
			    border: none !important;
			    border-radius:0;
			    padding: 0;
			    text-transform: uppercase;
			}
			
			.nav-tabs.nav-justified > li > a:hover {
			    border: none;
			    border-radius:0;
			    background-color: inherit !important;
			}
			
			.nav-tabs.nav-justified > li > a > .mapText {
			    border-bottom:3px solid #FFF!important;
			    display: block;	   
			    color:#000;
			    padding: 20px 0;
			    font-size: 20px;
			}
			
			.nav-tabs.nav-justified > li.active > a > .mapText {
			  	border: none;
			    border-bottom:3px solid #093!important;
			    color: #093;
			    font-size: 20px;  
			}
			
			.nav-tabs.nav-justified > li.active > a {
			    border-right:1px solid #fff;
			    background-color:#fff;
			}
			
			
			#memberModalTable td, #memberModalTable th{
				border: none;
			}
			
	</style>
	
	
    <!-- 외부 라이브러리 -->
    <script type="text/javascript" src="<c:url value='/js/lib/jquery/jquery.min.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/lib/bootstrap/js/bootstrap.min.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/lib/echarts.min.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/lib/inapp.js'/>"></script>

    <!-- INORG 제품 -->
    <script type="text/javascript" src="<c:url value='/js/lib/softin/softin.js'/>"></script>           <!-- 라이센스 정보 -->
    <script type="text/javascript" src="<c:url value='/js/lib/softin/inorg/inorginfo.js'/>"></script>  <!-- 생성시 필요한 함수 및 내부설정용 파일 -->
    <script type="text/javascript" src="<c:url value='/js/lib/softin/inorg/inorg.js'/>"></script>      <!-- core file -->

    <!-- IBSheet 제품 -->

    <script type="text/javascript" src="<c:url value='/js/lib/softin/ibsheet/ibsheet.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/lib/softin/ibsheet/ibsheetinfo.js'/>"></script>
   
   	<!-- 화면 소스 -->
   	<script type="text/javascript" src="<c:url value='/js/chaeumneul/popup.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/chaeumneul/org_view.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/chaeumneul/tree_view.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/chaeumneul/app.js'/>"></script>
	
</head>

<body>
	<div id="containerMain" class="container-fluid" style="height:100%; margin: 0 !important;">
		<div id="menuArea" class="row" style="margin: 10px !important; border-bottom:1px solid #808080!important;">
			<div class="col-md-3" style="text-align: center;">
				<img src="/img/logoGreen.png" style="height:70px">
			</div>
			<div class="col-md-9">
				  <!-- Nav tabs -->
				  <ul class="nav nav-tabs nav-justified" role="tablist">
				      <li role="presentation" class="active">
				          <a href="chaeumneul.do" >
				              <span class="mapText">조회</span>
				          </a>
				      </li>
				      <li role="presentation">
				          <a href="management.do" >
				              <span class="mapText">관리</span>
				          </a>
				      </li>
				      <li role="presentation">
				          <a href="statistics.do" >
				              <span class="mapText">통계</span>
				          </a>
				      </li>
				  </ul>
			</div>
			
		</div>
	  <!-- Org -->
	    <div id="contentsArea" class="row" style="margin: 0 !important;">
	        <div class="col-md-3" style="height:100%;">
	            <div id="treeArea"></div>
	        </div>
	        <div class="col-md-9" style="height:100%;">
				
	            <div id="button_view" class="container-fluid form-inline">
	                
					
					<div class="pull-left" style="margin-left: 10px">
	                	<input id="openListData" type="button" class="btn btn-default" value="전체 펼치기"></input>
	                	<input id="closeListData" type="button" class="btn btn-default" value="전체 닫기"></input>
                	</div>
                	<div class="pull-right" style="margin-right: 10px">
                		<button id="largeView" type="button" class="btn btn-default btn-sm">
			                    <span class="glyphicon glyphicon-zoom-in" aria-hidden="true"></span>
			            </button>
			            <button id="smallView" type="button" class="btn btn-default btn-sm">
			                    <span class="glyphicon glyphicon-zoom-out" aria-hidden="true"></span>
			            </button>
			            
			            <button id="autoView" type="button" class="btn btn-default btn-sm">
			                    <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
			            </button>
			            
			            <button id="savePop" type="button" class="btn btn-info btn-sm">
			                    <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
			            </button>
                	
	            
	               	</div>
	            </div>
	            <div id="viewOrg" style="height: 500px;"></div>
	
	        </div>
	    </div>
    </div>
    
     <!-- Currmodal-->
    <div class="modal fade" id="currModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
                    <h4 class="modal-title text-muted" id="currModalName"> 교육과정 정보</h4>
                </div>
                <div class="modal-body" style="margin:0;">
                	<div class="row">
                		<div class="well" id="currModalContents" style="margin: 10px;">
		                  					
		                </div>
                	</div>
                				<table id="currModalTable" class="table table-borderless" style="margin:0;">
		                  		<tr>
		                  			<td style="width:80px;">
		                  				<span style="font-weight: bold;">교육기간 </span>
		                  			</td>
		                  			<td style="width:190px;">
		                  				<span id="currModalTime">
		                  				</span>
		                  			</td>
		                  			<td>
		                  				<span style="font-weight: bold;">인원 </span>
		                  			</td>
		                  			<td>
		                  				<span id="currModalPersonNum">
		                  				</span>
		                  			</td>
		                  		</tr>
		                  		<tr>
		                  			<td>
		                  				<span style="font-weight: bold;">가격 </span>
		                  			</td>
		                  			<td>
		                  				<span id="currModalPrice">
		                  				</span>
		                  			</td>
		                  			<td>
		                  				<span style="font-weight: bold;">강사 </span>
		                  			</td>
		                  			<td>
		                  				<span id="currModalManagerName">
		                  				</span>
		                  			</td>
		                  		</tr>
		                  		<tr>
		                  			<td colspan="2" style="height:100%;">
		                  				<div style="border:1px solid #808080; margin: 0;">
		                  				<div style="text-align:center; background-color: #909090;"><span style="font-weight: bold;"> 정원 및 수강인원 (%)</span></div>
		                  				<div style="height:110px; display:table-cell; vertical-align:middle; ">
		                  					
											<div id="peopleNumChart" style="width:220px; height:80px;">
					                  		</div>
					                 		                  				
		                  				</div>
		                  				</div>
		                  			</td>
		                  			<td colspan="2" style="height:100%;">
		                  				<div style="border:1px solid #808080; margin: 0;">
		                  				<div style="text-align:center; background-color: #909090;"><span style="font-weight: bold;"> 성별 비율 및 수강인원</span></div>
		                  			
		                  				<div class="center-block" id="genderChart" style="width:180px; height:110px;">
		                  				</div>
		                  				</div>
		                  			</td>  
		                  		</tr>               		
		                  	</table>
                	
                </div>
                <div class="modal-footer">
                	<div class="clearfix">
                		<div class="pull-left" style="margin-left: 10px">
	                		 <button type="button" class="btn btn-primary" id="currModalManage" data-dismiss="modal" aria-label="Close" >
	                			관리
	                		 </button>
                		</div>
                		<div class="pull-right" style="margin-right: 10px">
	               			<button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">닫기</button>
	               		</div>
                	</div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Membermodal -->
    <div class="modal fade" id="memberModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
                    <h4 class="modal-title text-muted" id="myModalLabel"> 멤버 정보</h4>
                </div>
                <div class="modal-body" style="margin:0;">
                  	<table id="memberModalTable" class="table table-borderless" style="margin:0;">
                  		<tr>
                  			<td rowspan="5" width="150px">
                  				<div id="memberModalImg" style="padding: 10px; height: 150px; display: table-cell; vertical-align: middle;">
                  				</div>
                  			</td>
                  			<td colspan="2">
                  				<div id="memberModalCod"></div>
                  			</td>
                  		</tr>
                  		<tr>
                  			<td colspan="2">
                  				<div id="memberModalName"></div>
                  			</td>
                  		</tr>
                  		<tr>
                  			<td>
	                  			<div id="memberModalGender"></div>
                  			</td>
                  			<td>
                  				<div id="memberModalAge"></div>
                  			</td>
                  		</tr>
                  		<tr>
                  			<td colspan="2">
                  				<div id="memberModalPhone"></div>
                  			</td>           		
                  		</tr>
                  		<tr>
                  			<td colspan="2">
                  				<div id="memberModalSub"></div>
                  			</td>           		
                  		</tr>
                  	</table>
                </div>
                <div class="modal-footer">
                	<div class="clearfix">
                		<div class="pull-left" style="margin-left: 10px">
	                		 <button type="button" class="btn btn-primary" id="memberModalManage" onclick="location.href='managerview.do?id=1'" data-dismiss="modal" aria-label="Close" >
	                		 		관리
	                		 </button>
                		</div>
                		<div class="pull-right" style="margin-right: 10px">
	               			<button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">닫기</button>
	               		</div>
                	</div>
                </div>
            </div>
        </div>
    </div>

    <!--save modal-->
    <div class="modal fade" id="saveModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
                    <h4 class="modal-title text-muted" id="myModalLabel"> 저장 방식 선택</h4>
                </div>
                <div class="modal-body">
                    <div class="container form-inline">
                        <span class="input-group-text">저장할 이름</span>

                        <input id="saveName" type="text" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <input id="saveAsImage" type="button" class="btn btn-default" value="이미지 저장" data-dismiss="modal"
                        aria-label="Close"></input>
                    <input id="saveAsJson" type="button" class="btn btn-default" value="Json 저장" data-dismiss="modal"
                        aria-label="Close"></input>
                </div>
            </div>
        </div>
    </div>
    
    <script>
  		//resize
// 		var contentsArea_h, window_h, menuArea_h;
	
// 	    window_h = $( window ).height(); 
// 	    menuArea_h = $( "#menuArea" ).height(); 
	    
// 	    $( "#contentsArea" ).height( $( "#contentsArea" ).height() - menuArea_h - 25); 
	    
	    
// 	    $( window ).resize( function () {
// 	        window_h = $( window ).height(); 
	        
// 		    menuArea_h = $( "#menuArea" ).height(); 
		
// 		    contentsArea_h=$( "#contentsArea" ).height( window_h - menuArea_h -25); 

// 	    } );
	</script>
</body>
</html>