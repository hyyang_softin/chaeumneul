<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c"         uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"      uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<%@ taglib prefix="spring"    uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link rel = "icon" href ="<c:url value='/img/egovframework/chaeumneul/books.ico'/>">
	
  	<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>채움늘 교육과정 관리 패키지</title>
	
	<link rel="stylesheet" href="<c:url value='/js/lib/bootstrap/css/bootstrap.min.css'/>">
	
	<style>
	        html,
	        body {
	            height: 100% !important;
	        }
			
			.nav-tabs.nav-justified > li > a {
			    border: none !important;
			    border-radius:0;
			    padding: 0;
			    text-transform: uppercase;
			}
			
			.nav-tabs.nav-justified > li > a:hover {
			    border: none;
			    border-radius:0;
			    background-color: inherit !important;
			}
			
			.nav-tabs.nav-justified > li > a > .mapText {
			    border-bottom:3px solid #FFF!important;
			    display: block;	   
			    color:#000;
			    padding: 20px 0;
			    font-size: 20px;
			}
			
			.nav-tabs.nav-justified > li.active > a > .mapText {
			  	border: none;
			    border-bottom:3px solid #093!important;
			    color: #093;
			    font-size: 20px;  
			}
			
			.nav-tabs.nav-justified > li.active > a {
			    border-right:1px solid #fff;
			    background-color:#fff;
			}
			
			
			.sidebar-nav {
		    	width: 250px;
		   		margin: 0;
		    	padding: 0;
		    	list-style: none;
		  	}
		  
		  	.sidebar-nav li {
		  		font-size: 20px;  
		    	text-indent: 1.5em;
		    	line-height: 2.8em;
		  	}
		  
		  	.sidebar-nav li a {
		    	display: block;
		    	text-decoration: none;
		    	color: #000;
		  	}
		  
		  	.sidebar-nav li a:hover {
		    	color: #093;
		    	background: rgba(80, 80, 80, 0.1);
		  	}
		  	
		  	.sidebar-nav li.active a {
		    	color: #093;
		    	background: rgba(80, 80, 80, 0.1);
		  	}
		  
		 	.sidebar-nav > .sidebar-brand {
		    	font-size: 1.3em;
		   		line-height: 3em;
		  	}
		
	</style>
	
    <!-- 외부 라이브러리 -->
    <script type="text/javascript" src="<c:url value='/js/lib/jquery/jquery.min.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/lib/bootstrap/js/bootstrap.min.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/lib/echarts.min.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/lib/inapp.js'/>"></script>

    <!-- INORG 제품 -->
    <script type="text/javascript" src="<c:url value='/js/lib/softin/softin.js'/>"></script>           <!-- 라이센스 정보 -->
    <script type="text/javascript" src="<c:url value='/js/lib/softin/inorg/inorginfo.js'/>"></script>  <!-- 생성시 필요한 함수 및 내부설정용 파일 -->
    <script type="text/javascript" src="<c:url value='/js/lib/softin/inorg/inorg.js'/>"></script>      <!-- core file -->

    <!-- IBSheet 제품 -->

    <script type="text/javascript" src="<c:url value='/js/lib/softin/ibsheet/ibsheet.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/lib/softin/ibsheet/ibsheetinfo.js'/>"></script>
   
   	<!-- 화면 소스 -->
   	<script type="text/javascript" src="<c:url value='/js/chaeumneul/curr_view.js'/>"></script>
   	<script type="text/javascript" src="<c:url value='/js/chaeumneul/popup_curr.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/chaeumneul/app.js'/>"></script>
  	<script type="text/javascript" src="<c:url value='/js/chaeumneul/popup_curr_student.js'/>"></script>

</head>

<body>
	<input id="searchMainId" type="hidden" value="${currVo.id}">
	<div id="containerMain" class="container-fluid" style="height:100%; margin: 0 !important;">
		<div id="menuArea" class="row" style="margin: 10px !important; border-bottom:1px solid #808080!important;">
			<div class="col-md-3" style="text-align: center;">
				<img src="/img/logoGreen.png" style="height:70px">
			</div>
			<div class="col-md-9">
				  <!-- Nav tabs -->
				  <ul class="nav nav-tabs nav-justified" role="tablist">
				      <li role="presentation">
				          <a href="chaeumneul.do" >
				              <span class="mapText">조회</span>
				          </a>
				      </li>
				      <li role="presentation" class="active">
				          <a href="management.do" >
				              <span class="mapText">관리</span>
				          </a>
				      </li>
				      <li role="presentation">
				          <a href="statistics.do" >
				              <span class="mapText">통계</span>
				          </a>
				      </li>
				  </ul>
			</div>
			
		</div>
	  <!-- Org -->
	    <div id="contentsArea" class="row" style="height:100%; margin: 0 !important;">
	        <div class="col-md-3" style="height:100%;">
	       		  <!-- 사이드바 -->
				  <div id="sidebar-wrapper">
				  	<h2>&nbsp;&nbsp;관리</h2>
				    <ul class="sidebar-nav" style="width: 100%;">
				      	<li class="active"><a href="management.do"><span style="font-weight: bold;">교육과정 관리</span></a></li>
				      	<li><span style="font-weight: bold;">멤버 관리</span></li>
				      	<li><a href="managerview.do"> - 강사 관리</a></li>
				      	<li><a href="studentview.do"> - 수강생 관리</a></li>
				    </ul>
				  </div>
				  <!-- /사이드바 -->
	        </div>
	        <div class="col-md-9" style="height:100%;">
	       		<div id="button_view" class="clearfix" style="margin-bottom: 10px">
                		<div class="pull-left" style="margin-left: 10px">
	                		 <div class="form-inline">
							  	<div class="form-group">
							    	<select id="searchType" class="form-control">
									 	<option value="0">분류</option>
									 	<option value="1">고유번호</option>
									  	<option value="2">과정 명</option>
									  	<option value="3">강사명</option>
									</select>
							    	<input type="text" id="searchValue" class="form-control" >
							  	</div>
							  	<button type="button" id="searchOn" class="btn btn-default">검색</button>
							</div>
                		</div>
                		<div class="pull-right" style="margin-right: 10px">
                			<input id="changeManager" type="button" class="btn btn-default" value="강사 변경" ></input>
	               			<input id="changeStudent" type="button" class="btn btn-default" value="수강 인원 변경" ></input>
	               			
	               			<input id="addData" type="button" class="btn btn-default" value="추가"></input>
	
	                		<input id="saveData" type="button" class="btn btn-info" value="저장"></input>
	               		</div>
               	</div>
				
	           	<div id="currArea"></div>
	
	        </div>
	    </div>
    </div>
    
     <!-- Currmodal-->
    <div class="modal fade" id="currModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
                    <h4 class="modal-title text-muted" id="currModalName"> 교육과정 정보</h4>
                </div>
                <div class="modal-body" style="margin:0;">
                	<div class="row">
                		<div class="well" id="currModalContents" style="margin: 10px;">
		                  					
		                </div>
                		</div>
                				<table id="currModalTable" class="table table-borderless" style="margin:0;">
		                  		<tr>
		                  			<td style="width:80px;">
		                  				<span style="font-weight: bold;">교육기간 </span>
		                  			</td>
		                  			<td style="width:190px;">
		                  				<span id="currModalTime">
		                  				</span>
		                  			</td>
		                  			<td>
		                  				<span style="font-weight: bold;">인원 </span>
		                  			</td>
		                  			<td>
		                  				<span id="currModalPersonNum">
		                  				</span>
		                  			</td>
		                  		</tr>
		                  		<tr>
		                  			<td>
		                  				<span style="font-weight: bold;">가격 </span>
		                  			</td>
		                  			<td>
		                  				<span id="currModalPrice">
		                  				</span>
		                  			</td>
		                  			<td>
		                  				<span style="font-weight: bold;">강사 </span>
		                  			</td>
		                  			<td>
		                  				<span id="currModalManagerName">
		                  				</span>
		                  			</td>
		                  		</tr>
		                  		<tr>
		                  			<td colspan="2" style="height:100%;">
		                  				<div style="border:1px solid #808080; margin: 0;">
		                  				<div style="text-align:center; background-color: #909090;"><span style="font-weight: bold;"> 정원 및 수강인원 (%)</span></div>
		                  				<div style="height:110px; display:table-cell; vertical-align:middle; ">
		                  					
											<div id="peopleNumChart" style="width:220px; height:80px;">
					                  		</div>
					                 		                  				
		                  				</div>
		                  				</div>
		                  			</td>
		                  			<td colspan="2" style="height:100%;">
		                  				<div style="border:1px solid #808080; margin: 0;">
		                  				<div style="text-align:center; background-color: #909090;"><span style="font-weight: bold;"> 성별 비율 및 수강인원</span></div>
		                  			
		                  				<div class="center-block" id="genderChart" style="width:180px; height:110px;">
		                  				</div>
		                  				</div>
		                  			</td>  
		                  		</tr>               		
		                  	</table>
                	
                </div>
                <div class="modal-footer">
                	<div class="clearfix">
   
                		<div class="pull-right" style="margin-right: 10px">
	               			<button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">닫기</button>
	               		</div>
                	</div>
                </div>
            </div>
        </div>
    </div>
    
    
     <!-- ManagerSelectModal-->
    <div class="modal fade" id="managerSelectModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
                    <h4 class="modal-title text-muted" > 강사 선택 </h4>
                </div>
                <div id="managerSelectModalBody" class="modal-body" style="height: 350px;">
                	<input id="managerSelectRegi_cur" type="hidden">
					<input id="managerSelectRow" type="hidden">
					<div id="managerSelectSearchButton" class="form-inline" style="margin-bottom: 8px" >
							<div class="form-group">
							   	<select id="modalsearchType" class="form-control input-sm">
							   		<option value="3">강좌없음</option>
									<option value="0">멤버번호</option>
								 	<option value="1">이름</option>
								</select>
							  	<input type="text" id="modalsearchValue" class="form-control input-sm" >
							</div>
							<button type="button" id="modalsearchOn" class="btn btn-default btn-sm">검색</button>
					</div>
					<div style="height: 280px;">
						<div id="currPopupArea"></div>
					</div>
                </div>
                <div class="modal-footer">
               		<div class="pull-left" style="margin-left: 10px">
	                		
                	</div>
                	<div class="pull-right" style="margin-right: 10px">
                			<button type="button" class="btn btn-primary" id="ManagerSelectSave" data-dismiss="modal" aria-label="Close" >
	                		 		적용
	                		</button>
               
	               			<button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">닫기</button>
	               	</div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- contentsModal-->
    <div class="modal fade" id="contentsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
                    <h4 class="modal-title text-muted" > 강좌 설명 </h4>
                </div>
                <div class="modal-body">
                	<input id="currContetsRow" type="hidden">
                	<input id="currContetsCol" type="hidden">
                  	<textarea id="currModalContents" class="form-control" rows="5" style="min-width: 100%">
                  	</textarea>
                </div>
                <div class="modal-footer">
               		<button type="button" class="btn btn-primary" id="contentsSave" data-dismiss="modal" aria-label="Close" >
	                		 		수정완료
	                </button>
               
	               	<button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">닫기</button>

                </div>
            </div>
        </div>
    </div>
    
    <!-- addStudentsModal-->
    <div class="modal fade" id="addStudentsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
                    <h4 class="modal-title text-muted" > 수강생 추가 </h4>
                </div>
                <div class="modal-body" style="height: 350px;">
                	<div class="row" style="height: 10%;">
                		<div class="col-md-5">
                		<span>수강 멤버</span>
                		</div>
                		<div class="col-md-1">
                		</div>
                		<div class="col-md-6">
                		<div id="addStudentsSelectSearchButton" class="form-inline" >
							<span>비 수강 멤버</span>
							<div class="form-group pull-right">
							   	<select id="addStudentsearchType" class="form-control input-sm">
							   		<option value="3">강좌없음</option>
									<option value="0">멤버번호</option>
								 	<option value="1">이름</option>
								</select>
							  	<input type="text" id="addStudentsearchValue" class="form-control input-sm" style="display:none;">
								<button type="button" id="addStudentsearchOn" class="btn btn-default btn-sm" style="display:none;">검색</button>
							</div>	
						</div>
                		</div>
                	</div>
                	<div class="row" style="height: 90%;">
                		<div class="col-md-5" style="height: 100%;">
                			<div id="currPopupStudent1Area">
                			
                			</div>
                		</div>
                		<div class="col-md-1" style="height: 100%;">
                			<br/>
                			<br/>
                			<br/>
                			<br/>
                			<br/>
                			<button id="leftStudentMove" type="button" class="btn btn-default btn-sm">
			                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			                </button>
			                <br/>
			                <br/>
			                <button id="rightStudentMove" type="button" class="btn btn-default btn-sm">
			                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			                </button>
                		</div>
                		<div class="col-md-6" style="height: 100%;">
                			<div id="currPopupStudent2Area">
                			
                			</div>
                		</div>
                	</div>
                </div>
                <div class="modal-footer">
               		<button type="button" class="btn btn-primary" id="addStudentsModalSave" data-dismiss="modal" aria-label="Close" >
	                		 		적용
	                </button>
               
	               	<button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">닫기</button>

                </div>
            </div>
        </div>
    </div>
    
    <script>
		//resize
		var contentsArea_h, window_h, menuArea_h;
	
	    window_h = $( window ).height(); 
	    menuArea_h = $( "#menuArea" ).height(); 
	    contentsArea_h=$( "#contentsArea" ).height( $( "#contentsArea" ).height() - menuArea_h - 25); 
	    
	    
	    $( window ).resize( function () {
	        window_h = $( window ).height(); 
	        
		    menuArea_h = $( "#menuArea" ).height(); 
		
		    contentsArea_h=$( "#contentsArea" ).height( window_h - menuArea_h -25); 
	
	    } );
	</script>
</body>
</html>