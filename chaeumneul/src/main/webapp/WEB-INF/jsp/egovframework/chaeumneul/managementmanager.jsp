<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c"         uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"      uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<%@ taglib prefix="spring"    uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link rel = "icon" href ="<c:url value='/img/egovframework/chaeumneul/books.ico'/>">
	
  	<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>채움늘 교육과정 관리 패키지</title>
	
	<link rel="stylesheet" href="<c:url value='/js/lib/bootstrap/css/bootstrap.min.css'/>">
	
	<style>
	        html,
	        body {
	            height: 100% !important;
	        }
			
			.nav-tabs.nav-justified > li > a {
			    border: none !important;
			    border-radius:0;
			    padding: 0;
			    text-transform: uppercase;
			}
			
			.nav-tabs.nav-justified > li > a:hover {
			    border: none;
			    border-radius:0;
			    background-color: inherit !important;
			}
			
			.nav-tabs.nav-justified > li > a > .mapText {
			    border-bottom:3px solid #FFF!important;
			    display: block;	   
			    color:#000;
			    padding: 20px 0;
			    font-size: 20px;
			}
			
			.nav-tabs.nav-justified > li.active > a > .mapText {
			  	border: none;
			    border-bottom:3px solid #093!important;
			    color: #093;
			    font-size: 20px;  
			}
			
			.nav-tabs.nav-justified > li.active > a {
			    border-right:1px solid #fff;
			    background-color:#fff;
			}
			
			.sidebar-nav {
		    	width: 250px;
		   		margin: 0;
		    	padding: 0;
		    	list-style: none;
		  	}
		  
		  	.sidebar-nav li {
		  		font-size: 20px;  
		    	text-indent: 1.5em;
		    	line-height: 2.8em;
		  	}
		  
		  	.sidebar-nav li a {
		    	display: block;
		    	text-decoration: none;
		    	color: #000;
		  	}
		  
		  	.sidebar-nav li a:hover {
		    	color: #093;
		    	background: rgba(80, 80, 80, 0.1);
		  	}
		  	
		  	.sidebar-nav li.active a {
		    	color: #093;
		    	background: rgba(80, 80, 80, 0.1);
		  	}
		  
		 	.sidebar-nav > .sidebar-brand {
		    	font-size: 1.3em;
		   		line-height: 3em;
		  	}
		  	
		  	#memberModalTable td, #memberModalTable th{
				border: none;
			}
		  	
			.imgs_wrap {
	            width: 600px;
	            margin-top: 50px;
	        }
	        .imgs_wrap img {
	            max-width: 200px;
	        }

	</style>
	
    <!-- 외부 라이브러리 -->
    <script type="text/javascript" src="<c:url value='/js/lib/jquery/jquery.min.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/lib/bootstrap/js/bootstrap.min.js'/>"></script>
    
    <script type="text/javascript" src="<c:url value='/js/lib/inapp.js'/>"></script>

    <!-- INORG 제품 -->
    <script type="text/javascript" src="<c:url value='/js/lib/softin/softin.js'/>"></script>           <!-- 라이센스 정보 -->
    <script type="text/javascript" src="<c:url value='/js/lib/softin/inorg/inorginfo.js'/>"></script>  <!-- 생성시 필요한 함수 및 내부설정용 파일 -->
    <script type="text/javascript" src="<c:url value='/js/lib/softin/inorg/inorg.js'/>"></script>      <!-- core file -->

    <!-- IBSheet 제품 -->

    <script type="text/javascript" src="<c:url value='/js/lib/softin/ibsheet/ibsheet.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/lib/softin/ibsheet/ibsheetinfo.js'/>"></script>
   
   	<!-- 화면 소스 -->
   	<script type="text/javascript" src="<c:url value='/js/chaeumneul/popup.js'/>"></script>
   	<script type="text/javascript" src="<c:url value='/js/chaeumneul/manager_view.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/chaeumneul/app.js'/>"></script>
   
  
</head>

<body>
	<input id="searchMainManagerId" type="hidden" value="${managerVo}">
	<div class="wrapper">
        <div class="body">
            <!-- 첨부 버튼 -->
            <div id="attach">
                <input id="uploadInputBox" style="display: none" type="file" name="filedata" multiple />
            </div>
            
            <!-- 미리보기 영역 -->
            <div id="preview" class="content"></div>
            
            <!-- multipart 업로드시 영역 -->
            <form id="uploadForm" style="display: none;" />
            
        </div>
    </div>
    
	<div id="containerMain" class="container-fluid" style="height:100%; margin: 0 !important;">
		<div id="menuArea" class="row" style="margin: 10px !important; border-bottom:1px solid #808080!important;">
			<div class="col-md-3" style="text-align: center;">
				<img src="/img/logoGreen.png" style="height:70px">
			</div>
			<div class="col-md-9">
				  <!-- Nav tabs -->
				  <ul class="nav nav-tabs nav-justified" role="tablist">
				      <li role="presentation">
				          <a href="chaeumneul.do" >
				              <span class="mapText">조회</span>
				          </a>
				      </li>
				      <li role="presentation" class="active">
				          <a href="management.do" >
				              <span class="mapText">관리</span>
				          </a>
				      </li>
				      <li role="presentation">
				          <a href="statistics.do" >
				              <span class="mapText">통계</span>
				          </a>
				      </li>
				  </ul>
			</div>
			
		</div>
	  <!-- Org -->
	    <div id="contentsArea" class="row" style="height:100%; margin: 0 !important;">
	        <div class="col-md-3" style="height:100%;">
	       		  <!-- 사이드바 -->
				  <div id="sidebar-wrapper">
				  	<h2>&nbsp;&nbsp;관리</h2>
				    <ul class="sidebar-nav" style="width: 100%;">
				      	<li><a href="management.do"><span style="font-weight: bold;">교육과정 관리</span></a></li>
				      	<li><span style="font-weight: bold;">멤버 관리</span></li>
				      	<li class="active" ><a href="managerview.do"> - 강사 관리</a></li>
				      	<li><a href="studentview.do"> - 수강생 관리</a></li>
				    </ul>
				  </div>
				  <!-- /사이드바 -->
	        </div>
	        <div class="col-md-9" style="height:100%;">
				<div id="button_view" class="clearfix" style="margin-bottom: 10px">
                		<div class="pull-left" style="margin-left: 10px">
	                		 <div class="form-inline">
							  	<div class="form-group">
							    	<select id="searchType" class="form-control">
									 	<option value="0">멤버번호</option>
									 	<option value="1">이름</option>
									</select>
							    	<input type="text" id="searchValue" class="form-control" >
							  	</div>
							  	<button type="button" id="searchOn" class="btn btn-default">검색</button>
							</div>
                		</div>
                		<div class="pull-right" style="margin-right: 10px">
                			<input id="imgUpload" type="button" class="btn btn-default" value="사진올리기"></input>
                			
	               			<input id="addData" type="button" class="btn btn-default" value="추가"></input>
	
	                		<input id="saveData" type="button" class="btn btn-info" value="저장"></input>
	               		</div>
               	</div>
	           	<div id="managerArea"></div>
	
	        </div>
	    </div>
    </div>
    
    <!-- Membermodal -->
    <div class="modal fade" id="memberModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
                    <h4 class="modal-title text-muted" id="myModalLabel"> 멤버 정보</h4>
                </div>
                <div class="modal-body">
                  	<table id="memberModalTable" class="table table-borderless">
                  		<tr>
                  			<td rowspan="5" width="150px">
                  				<div id="memberModalImg" style="padding: 10px; height: 150px; display: table-cell; vertical-align: middle;"></div>
                  			</td>
                  			<td colspan="2">
                  				<div id="memberModalCod"></div>
                  			</td>
                  		</tr>
                  		<tr>
                  			<td colspan="2">
                  				<div id="memberModalName"></div>
                  			</td>
                  		</tr>
                  		<tr>
                  			<td>
	                  			<div id="memberModalGender"></div>
                  			</td>
                  			<td>
                  				<div id="memberModalAge"></div>
                  			</td>
                  		</tr>
                  		<tr>
                  			<td colspan="2">
                  				<div id="memberModalPhone"></div>
                  			</td>           		
                  		</tr>
                  		<tr>
                  			<td colspan="2">
                  				<div id="memberModalSub"></div>
                  			</td>           		
                  		</tr>
                  	</table>
                </div>
                <div class="modal-footer">
                	<div class="clearfix">
                		<div class="pull-left" style="margin-left: 10px">

                		</div>
                		<div class="pull-right" style="margin-right: 10px">
	               			<button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">닫기</button>
	               		</div>
                	</div>
                </div>
            </div>
        </div>
    </div>
    
    <script>
		//resize
		var contentsArea_h, window_h, menuArea_h;
	
	    window_h = $( window ).height(); 
	    menuArea_h = $( "#menuArea" ).height(); 
	    contentsArea_h=$( "#contentsArea" ).height( $( "#contentsArea" ).height() - menuArea_h - 25); 
	    
	    $( window ).resize( function () {
	        window_h = $( window ).height(); 
	        
		    menuArea_h = $( "#menuArea" ).height(); 
		
		    contentsArea_h=$( "#contentsArea" ).height( window_h - menuArea_h -25); 
	
	    } );
	   

	</script>
</body>
</html>