<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c"         uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form"      uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="validator" uri="http://www.springmodules.org/tags/commons-validator" %>
<%@ taglib prefix="spring"    uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
	<link rel = "icon" href ="<c:url value='/img/egovframework/chaeumneul/books.ico'/>">
	<meta charset="UTF-8">
	
<title>이력서</title>
	<style>
		body {
            margin: 0;
            padding: 0;
            font: 12pt "Tahoma";
        }
        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
        }
        .page {
            width: 21cm;
            min-height: 29.7cm;
            padding: 2cm;
            margin: 1cm auto;
            border-radius: 5px;
            background: white;
        }
        
        th {
        	background: #909090;
        }
        
        td input, td textarea {
		    border: 0px;
		}

        table.csstable {
		    border-collapse: separate;
		    border-spacing: 0;
		    border-top: 1px solid #ccc;
		    border-left: 1px solid #ccc;
		}
		table.csstable th {
			text-align: center;
		    font-weight: bold;
		    border-right: 1px solid #ccc;
		    border-bottom: 1px solid #ccc;
		    border-top: 1px solid #fff;
		    border-left: 1px solid #fff;
		    background: #eee;
		}
		table.csstable td {  
		    border-right: 1px solid #ccc;
		    border-bottom: 1px solid #ccc;
		}
	</style>
	
	 <!-- 외부 라이브러리 -->
    <script type="text/javascript" src="<c:url value='/js/lib/jquery/jquery.min.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/js/lib/bootstrap/js/bootstrap.min.js'/>"></script>
    <script type = "text/javascript" src = "https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
	<script type = "text/javascript" src = "https://html2canvas.hertzen.com/dist/html2canvas.min.js"></script>
	 <script type="text/javascript" src="<c:url value='/js/lib/inapp.js'/>"></script>

	<script type="text/javascript" src="<c:url value='/js/chaeumneul/resume.js'/>"></script>
	<script type="text/javascript" src="<c:url value='/js/chaeumneul/app.js'/>"></script>
	
	<script>
		 $(window).on('load' , function() {
	   	  
	   	  var strWidth, strHeight; 
	
	   	  //innerWidth / innerHeight / outerWidth / outerHeight 지원 브라우저 
	
	   	  if ( window.innerWidth && window.innerHeight && window.outerWidth && window.outerHeight ) {
	
	   	    strWidth = $('#container').outerWidth() + (window.outerWidth - window.innerWidth);
	
	   	    strHeight = $('#container').outerHeight() + (window.outerHeight - window.innerHeight);
				console.log(strHeight);
	   	  }
	
	   	  else {
	
	   	    var strDocumentWidth, strDocumentHeight ,strMenuWidth, strMenuHeight; 
	   	    
	   	    strDocumentWidth = $(document).outerWidth();
	
	   	    strDocumentHeight = $(document).outerHeight();
	
	   	    window.resizeTo ( strDocumentWidth, strDocumentHeight );
	
	   	    strMenuWidth = strDocumentWidth - $(window).width();
	
	   	    strMenuHeight = strDocumentHeight - $(window).height();
	
	   	    strWidth = $('#container').outerWidth() + strMenuWidth;
	
	   	    strHeight = $('#container').outerHeight() + strMenuHeight;
	
	   	  }
			  //resize 
	
	   	  window.resizeTo( strWidth, strHeight );
	   	  
	   }); 
		
	</script>

</head>
<body>
	<input type="hidden" id="key" value="${key}" >
	<input type="hidden" id="idnum" value="${vo.ID}" >
	<input type="hidden" id="skilldata" value="${voResume.skill}" >
	<div id="container">
	<div style="text-align:center; margin: 0.5cm ;">
		<button type="button" id="saveresume" style="color: #fff; background:#093; font-size:1em; border-radius:0.3em; padding:5px 20px; margin-right: 6.5cm;">
			수정완료
		</button>
		<button type="button" id="pdfresume" style="color: #fff; background:#d9534f; font-size:1em; border-radius:0.3em; padding:5px 20px;">
			PDF 저장
		</button>
		<button type="button" id="exit" style="color: #fff; background:#808080; font-size:1em; border-radius:0.3em; padding:5px 20px;  margin-left: 6.5cm;">
			닫기
		</button>
	</div>
	<div style="border: 1px solid;">
	<div class="page" id="page">
	
	<table style="width: 100%; align:center;">
		<tr>
			<td colspan="7">
				<div style="height:2cm; text-align:center">
					<font size="5">이  력  서</font>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="1" style="width:20%;">
				<div style="padding:0.3cm; margin-bottom:1.5cm; margin-right:0.3cm; border:1px solid; text-align: center;">
					<img src="${vo.picture_name}" style="height: 3cm;">
				</div>
			</td>
			<td colspan="6">
				<div style="margin-bottom:1.5cm;">
					<table class="csstable" style="width: 100%; height:150px;">
						<tr>
							<th style="width:15%;">
								이름
							</th>
							<td style="width:20%;"><div style="text-align:center; width:100%;">${vo.name}</div></td>
							<th style="width:15%;">나이 </th>
							<td style="width:15%;"><div style="text-align:center; width:100%;">${vo.age}</div></td>
							<th style="width:15%;">성별 </th>
							<td ><div style="text-align:center; width:100%;">
								<c:if test="${vo.gender eq 0}">
								 남
								</c:if>
								<c:if test="${vo.gender eq 1}">
								 여
								</c:if>
							</div></td>
						</tr>
						<tr>
							<th>연락처</th>
							<td colspan= "5"><div style="width:100%;">
							${vo.phone_num}
							</div></td>
						</tr>
						<tr>
							<th> 이메일</th>
							<td colspan= "5"><div style="width:100%;">
							${vo.email}
							</div></td>
						</tr>
						<tr>
							<th>주 소</th>
							<td colspan="5"><div style="width:100%;">
							${vo.address}
							</div></td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="7">
			<div >
				<table class="csstable" style="width: 100%; height:5cm;">
						<tr>
							<th colspan="7" >경 력 사 항</th>
						</tr>
						<tr>
							<th colspan= "2" style="width:20%; height: 0.5cm;">경력 날짜</th>
							<th colspan= "5">경력 내용</th>
						</tr>
						<tr>
							<td colspan= "2"><input id="careerdate1" type="text" style="width:100%; height:100%"> </td>
							<td colspan= "5"><input id="careerval1" type="text" style="width:100%; height:100%"> </td>
						</tr>
						<tr>
							<td colspan= "2"><input id="careerdate2" type="text" style="width:100%; height:100%"></td>
							<td colspan="5"> <input id="careerval2" type="text" style="width:100%; height:100%"></td>
						</tr>
						<tr>
							<td colspan= "2"> <input id="careerdate3" type="text" style="width:100%; height:100%"></td>
							<td colspan="5"> <input id="careerval3" type="text" style="width:100%; height:100%"></td>
						</tr>
						<tr>
							<td colspan= "2"> <input id="careerdate4" type="text" style="width:100%; height:100%"></td>
							<td colspan="5"> <input id="careerval4" type="text" style="width:100%; height:100%"></td>
						</tr>
				</table>
			</div>
			</td>
		</tr>
		<tr>
			<td colspan="7" >
				<div style="margin-top:2cm; margin-bottom:2cm;">
					<table class="csstable" style="width: 100%; height:5cm;">
							<tr>
								<th colspan="7" style="height:30px;">자 기 소 개 서</th>
							</tr>
							<tr>
								<td colspan="7">
								<textarea id="selfintro" rows="10" style="width: 100%;"><c:out value="${voResume.self}" /></textarea>
								</td>
							</tr>
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="7">
				<div >
				<div style = "text-align:center; margin-bottom:1.5cm;">위 기재 사항은 사실과 틀림이 없습니다. </div>
				<div style = "text-align:right" >성명 :&nbsp; <span style="font-style: oblique;">${vo.name}</span> &nbsp; (인)&nbsp;&nbsp;&nbsp;</div>
				</div>
			</td>
		</tr>
	</table>
	</div>
	</div>
	</div>

</body>
</html>