//var tree_view = {
inapp.add( "students_view", {
    cfg: {
    	files : {},
    	clickvalue : "",
    	clickrow : ""
    },
    actionMap: {
  
    },
    init: function () {
        this.create();
        this.resize();
        this.button();
        this.saveFile();
    },
    createSheet: function () {
        /**
         * IBSheet를 생성
         * @param 	{object}				IBSheet가 생성될 대상 컨테이너
         * @param	{object}				IBSheet의 ID
         * @param	{String}	"100%"		너비
         * @param	{String}	"100%"		높이
         */
        createIBSheet2( document.getElementById( "studentArea" ), "studentSheet", "100%", "100%" );
    },
    resize: function () {
    	 var studentView, contentsArea_h, button_view_h;

    	 studentView = this;
    	
         contentsArea_h = $( "#contentsArea" ).height(); 

         button_view_h = $( "#button_view" ).height(); 

         $( "#studentArea" ).height( contentsArea_h - button_view_h - 10 ); //둘을 뺀다.

         $( window ).resize( function () {
         	contentsArea_h = $( "#contentsArea" ).height();

             button_view_h = $( "#button_view" ).height();
             //리사이징 작업
             $( "#studentArea" ).height( contentsArea_h - button_view_h -10 );

             
         } );
    },
    create: function () {
        // 개발자 가이드 - 5.1 항목을 보고 작성
        var option = {};

        this.createSheet();

        option = {
            Cfg: {
                SearchMode: smLazyLoad, // smLazyLoad : 스크롤 조회 , smGeneral : 전체 조회
                Page: 30, //한번에 가져올 데이터 갯수
                AutoFitColWidth: "search|resize|init|colhidden|rowtransaction" //기본값 "" ,조회 및 로드 시점, 시트 Resize시점, 초키화 및 Removeall 호출 시점
            
            },
            HeaderMode: {
                Sort: 0, //헤더 클릭을 통한 컬럼 Sort 허용 여부.0 사용안함, 1 sort기능 사용(디폴트), 2 아이콘만 표시, 헤더셀만 사용
                ColMove: 0, //헤더칼럼 이동여부
                ColResize: 1 //컬럼 너비 ReSize 여부  
            },
            Cols: [
            	{
                    Header: "선택",
                    Type: "CheckBox",
                    SaveName: "CHK",
                    Align: "Center",
                    ShowMobile: 0,
                    Width: 15,
                },
            	{
                    Header: "멤버번호",
                    Type: "Popup",
                    Width: 20,
                    SaveName: "pkey",
                    Align: "Center",
                    ShowMobile: 0
                },
                {
                    Header: "사진",
                    Type: "html",
                    SaveName: "picture",
                    Align: "Center",
                    Width: 25
                },
            	{
                    Header: "이름",
                    Type: "Text",
                    Width: 30,
                    SaveName: "name",
                    Align: "Center",
                    Edit: 1,
                    Hidden: 0
                },
            	{
                    Header: "성별",
                    Type: "Combo",
                    Width: 20,
                    SaveName: "gender",
                    Align: "Center",
                    ComboText: "남|여",
                    ComboCode: "0|1",
                    ShowMobile: 0
                },
                {
                    Header: "나이",
                    Type: "Text",
                    Width: 20,
                    SaveName: "age",
                    Align: "Center",
                    Format: "PhoneNo"
                },
            	{
                    Header: "연락처",
                    Type: "Text",
                    Width: 30,
                    SaveName: "phone_num",
                    Align: "Center",
                    Format: "PhoneNo"
                },
                {
                    Header: "교육받는 강좌",
                    Type: "Text",
                    Width: 50,
                    SaveName: "regi_cur",
                    Align: "Center",
                    Edit: 0,
                    Hidden: 1
                },
                {
                    Header: "사진변경 여부",
                    Type: "Text",
                    SaveName: "picture_yn",
                    Align: "Center",
                    Edit: 0,
                    Hidden: 1
                },
            	{
                    Header: "Email",
                    Type: "Text",
                    Width: 40,
                    SaveName: "email",
                    Align: "Center",
                    Edit: 1,
                    Hidden: 0
                },
            	{
                    Header: "주소",
                    Type: "Text",
                    Width: 50,
                    SaveName: "address",
                    Align: "Center",
                    Edit: 1,
                    Hidden: 0
                },
                { 
                    Header: "이력서", 
                    Type: "Button", 
                    Width: 20, 
                    SaveName: "sub", 
                    Align: "Center", 
                    ShowMobile: 0
                }, 
                {
                    Header: "삭제",
                    Type: "DelCheck",
                    Width: 20,
                    SaveName: "del",
                    Align: "Center",
                    Edit: 1,
                    Hidden: 0
                },
                {
                	Header: "멤버 숫자 번호",
                    Type: "Int",
                    Width: 30,
                    SaveName: "id",
                    Align: "Center",
                    Edit: 0,
                    Hidden: 1
                },
                {
                    Header: "상태",
                    Type: "Status",
                    Width: 50,
                    SaveName: "sStatus",
                    Align: "Center",
                    Edit: 0,                   
                    Hidden: 1
                }
            ]
        }

        IBS_InitSheet( studentSheet, option ); 
//        managerSheet.SetTheme("TM", "TreeMain");
        
        this.bind();
        this.load();

    },
    bind: function () {
        var studentView;

        studentView = this;
        //tree_view를 직접 불러올수 없다.

        function addEvents( studentSheet, callbacks ) {
            //IBSheet 기능을 강제로 연결하여 사용할수 있도록 함.
            for ( var prop in callbacks ) {
                window[ studentSheet + "_" + prop ] = callbacks[ prop ];
            }
        }

        addEvents( studentSheet.id, {
        	OnButtonClick: function (row , col){
        		var rowJson, firstindex, lastindex, URI;
        		
        		rowJson = studentView.getRowJson( row );
        		firstindex = rowJson.picture.indexOf("http");
        		lastindex = rowJson.picture.indexOf("chaeumneul");
        		
        		URI = rowJson.picture.substring(firstindex, lastindex);
        		window.open('/resumemodi.do?Id='+rowJson.pkey, 'newWindow', "width=900,height=100%");
        		
        	},
            OnSelectCell: function ( OldRow, OldCol, NewRow, NewCol, isDelete ) {
            	
            	$("#imgUpload").off().click(function () {
            		studentView.cfg.clickvalue = studentSheet.GetCellValue( NewRow, 1 );
                	studentView.cfg.clickrow = NewRow;
                		
                	$("#uploadInputBox").trigger("click");
            	});
            	
            },
          	OnPopupClick: function ( row, col ){
          		var selectValue, rowJson, newData, idindex, photoindex, pictureURI;
          		if(col == 1){
            		
            		rowJson = studentView.getRowJson( row );    
            		
            		idindex = rowJson.picture.indexOf("http");
            		photoindex =rowJson.picture.indexOf(" style");
            		pictureURI =rowJson.picture.substring(idindex, photoindex);
            		
            		newData={ 
            				fields: {
            					img : {value : pictureURI +" style='width: 100px;'>"
            							},
            					pkey : {value : rowJson.pkey},
            					emp_nm : {value : rowJson.name},
            					age : {value : rowJson.age},
            					phone_num : {value : rowJson.phone_num},
            					sub : {value : rowJson.sub},
            					member_cod : {value : 2}
            				}
            		};
            	
            		if(rowJson.gender == 0){
            			newData.fields.gender = {value : "남"};
            		}else{
            			newData.fields.gender = {value : "여"};
            		}
                    //데이터를 전부 전달.
                    inapp.raise( studentView.name, {
                        action: "show_member",
                        data: newData
                    } );
                    
            	}
          	},
            OnDblClick: function ( row , col ) {
            	//더블클릭시 팝업호출
            	var selectValue, rowJson, newData, idindex, photoindex, pictureURI;
            	if(col == 2){
            		studentView.cfg.clickvalue = studentSheet.GetCellValue( row, 1 );
            		studentView.cfg.clickrow = row;
            		
            		$("#uploadInputBox").trigger("click");
            	}
            }
        } );
    },
    load: function ( ) {
    	studentSheet.ShowFilterRow();
    	studentSheet.SetRowHidden(1, 1);
    	
      	$.ajax({
         	type: "POST",
         	url : "/selectStudentData.do",
         	dataType: "json",
         	contentType : "application/json; charset=UTF-8",
         	success: function(resData) {
        
         		studentSheet.LoadSearchData( resData );
         	}
         });
    },
    button: function () {
    	var that = this;
    	
    	$("#searchOn").click(function(){
    		var searchType ,searchValue;
    		searchType = $( "#searchType" ).val();
    		searchValue = $( "#searchValue" ).val();
    		
    		if( searchValue != "" ){
    			if(searchType==0){
    				studentSheet.SetFilterOption(3 , 0);
    				studentSheet.SetFilterValue(1, searchValue , 9);
    				$( "#searchValue" ).val("");
    			}else if(searchType==1){
    				studentSheet.SetFilterOption(1 , 0);
    				studentSheet.SetFilterValue(3, searchValue , 11);
    				$( "#searchValue" ).val("");
            	}
    		}else{
    			studentSheet.SetFilterOption(1 , 0);
    			studentSheet.SetFilterOption(3 , 0);
    		}
    	});
    	
    	$("#sendEmailshow").click(function() {
    			
    		var i, data, idArray, newDataArray;
    		
    		data = studentSheet.FindCheckedRow(0);
    		
    		idArray = data.split('|');
    		newDataArray = [];
    		
    		for(i=0; i<idArray.length; i++){
    			newDataArray[i] = JSON.stringify({
    					name : studentSheet.GetCellValue( idArray[i], 3 ),
    					address : location.origin + "/resumesend.do?Id=2-"+studentSheet.GetCellValue( idArray[i], 13 )
    				});
    		}
    		
    		
    		inapp.raise( that.name, {
                action: "send_email_data",
                data: newDataArray
            } );
    		
    	});
    	
    	$("#addData").click(function() {
    		var IndexValue, addIndex, newIndexValue,searchURI, domain,idindex, photoindex, i;
    		
    		domain= "";
    		
    		IndexValue = studentSheet.GetCellValue( studentSheet.GetSelectRow(), 13 );
    		
    		//자동 pkey 입력
    		newIndexValue = studentSheet.GetColMaxValue(13)+1;
    		addIndex = studentSheet.DataInsert();	
    		that.setCellValue(addIndex, "pkey", 2+"-"+newIndexValue);
    		that.setCellValue(addIndex, "id", newIndexValue);
    		
    		//자동 사진입력
    		searchURI = studentSheet.GetCellValue(IndexValue , 2);
    		
    		idindex = searchURI.indexOf('http')
    		photoindex =searchURI.indexOf('photo');
    		domain =searchURI.substring(idindex, photoindex);
    		
    		that.setCellValue(addIndex, "picture", "<img id= '2-"+newIndexValue+"' src='img/photo/default.png' style='height: 60px; width: 60px;'>");
    		that.setCellValue(addIndex, "sub", "보기");
    	});
    	
    	
    	$("#saveData").click(function() {
    		var files = that.cfg.files;
    		
    		//가상의 폼만들기.
    		var form = $('#uploadForm')[0];
    		var formData = new FormData(form);
    		
    		var keys = Object.keys(files);
    		
    		for (var index = 0; index < Object.keys(files).length; index++) {
    			//formData 공간에 files라는 이름으로 파일을 추가한다.
    			//동일명으로 계속 추가할 수 있다.
    			formData.append('files',files[keys[index]]);
    			formData.append('name',keys[index]);
    		}
    	
    		if( Object.keys(files).length > 0){
    			
    			//ajax 통신으로 multipart form을 전송한다.
        		$.ajax({
        			type : 'POST',
                    enctype : 'multipart/form-data',
                    processData : false,
                    contentType : false,
                    cache : false,
                    timeout : 600000,
                    url : '/multiImageUpload.do',
                    dataType : 'JSON',
                    data : formData,
                    success : function(result) {
                    	if (result === -1) {
                    		alert('jpg, gif, png, bmp 확장자만 업로드 가능합니다.');
     
                    	} else if (result === -2) {
                    		alert('파일이 10MB를 초과하였습니다.');
       
                    	} else {
        
                    		that.save();
                    	}
                    }  
        		});
    		}else{
    			that.save();
    		}
    	});
    	
    	if( $("#searchMainStudentId").val() != ""){
    		$( "#searchType" ).val(0);
    		$( "#searchValue" ).val($("#searchMainStudentId").val());
    		$("#searchOn").trigger("click");
    	}
    },
    save: function (){
    	var files, saveJson, i;
    	files = this.cfg.files;
    	saveJson = studentSheet.GetSaveJson(); //수정할 데이터를 가져옴
    	console.log(saveJson);
		if (saveJson.data.length <= 0) {
			if (saveJson.Message == "NoTargetRows" ) {
				alert("저장할 내역이 없습니다.");
				return;
			}	
		}
		
		for(i= 0, length=saveJson.data.length ; i<length; i++){
			if(saveJson.data[i].pkey == "" || saveJson.data[i].name == ""){
				alert("필수 입력 사항 미달");
				return;
			}
		}
		
		$.ajax({
         	type: "POST",
         	url : "/saveStudentData.do",
         	data : JSON.stringify(saveJson),
         	dataType: "json",
         	contentType : "application/json; charset=UTF-8",
         	success: function(resData) {
         		if(resData == "fail"){
         			alert("오류! 저장 실패1.");
         		}
         		
         		alert("저장되었습니다.");
         		
         		setTimeout(function() {
         			studentSheet.LoadSearchData( resData );
         		}, 100);
         	},
         	error : function( resData ) {
         		alert("오류! 저장 실패.");
         	}
         });	
		
    },
    getRowJson: function ( params ) {
        //컬럼 데이터 행의 json 값 리턴 함수
        //파라미터는 찾을 row 번호.
        var value;

        value = studentSheet.GetRowJson( params );

        return value;
    },
    setCellValue: function ( row , saveName , data) {

    	studentSheet.SetCellValue( row, saveName , data );
        
    },
    saveFile : function (){
    	
    	var that, previewIndex;
    	
    	that =this;
    	previewIndex = 0;
    	
    	//유효성 검사
    	function addPreview(input,id) {
    		if (input[0].files) {
    	    //파일 선택이 여러개였을 시의 대응
    	    for (var fileIndex = 0; fileIndex < input[0].files.length; fileIndex++) {
    	    	var file = input[0].files[fileIndex];
    	    	
    	    	if(validation(file.name)){ 
    	    		continue;
    	    	}
    	    	
    	    	setPreviewForm(file,id);
    	    	}
    		} else
    			alert('invalid file input'); 
    	}

    	//미리보기
    	function setPreviewForm(file,id, img){
    		var reader = new FileReader();
    	   	var value = "#"+id;
    		
    	    reader.onload = function(img) {
    	    	
    	    
    	    	$( value ).attr("src" , img.target.result);
    	    	
    	    		that.cfg.files[value] = file;  
    	    		
    	    	};
    	 
    	    	reader.readAsDataURL(file);
    	    	
    	}
    	 

    	function validation(fileName) {
    		fileName = fileName + "";
    		var fileNameExtensionIndex = fileName.lastIndexOf('.') + 1;
    		var fileNameExtension = fileName.toLowerCase().substring(
    					fileNameExtensionIndex, fileName.length);
    		if (!((fileNameExtension === 'jpg')
    				|| (fileNameExtension === 'gif') || (fileNameExtension === 'png'))) {
    			alert('jpg, gif, png 확장자만 업로드 가능합니다.');
    			return true;
    		} else {
    			return false;
    		}
    	}

    	$('#attach input[type=file]').change(function() {
    		addPreview($(this), that.cfg.clickvalue); //preview form 추가하기
    		
    		that.setCellValue( that.cfg.clickrow ,"picture_yn", "Y");
    	});
    	
    }
} );