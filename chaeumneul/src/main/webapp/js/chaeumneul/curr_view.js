//var tree_view = {
inapp.add( "curr_view", {
    cfg: {
    	
    },
    actionMap: {
    	"popup_curr": {
    		"new_curr_contents": function ( newContentsData ){
    		
    			var data = newContentsData.data;
    			
    			this.setCellValue(data.row, "contents", data.data)
    	    	
    		},
    		"new_curr_manager": function ( newCurrManagerData ){
    			var data = newCurrManagerData.data;
    			
    			var oldt = currSheet.FindText( "pkey", data.old_curr_cod );
   
    			this.setCellValue( oldt, "manager_name", "");
    			this.setCellValue( oldt, "manager_cod", "");
    			this.setCellValue(data.row, "manager_name", data.name);	
    			this.setCellValue(data.row, "manager_cod", data.pkey.split('-')[1]);
    		}
    	},
    	"popup_curr_student" : {
    		"success" : function (successdata){
    			if(successdata.data ==1){
    				this.load();
    			}
    		}
    	}
    },
    init: function () {
    
        this.create();
        this.resize();
        this.button();
    },
    createSheet: function () {
        /**
         * IBSheet를 생성
         * @param 	{object}				IBSheet가 생성될 대상 컨테이너
         * @param	{object}				IBSheet의 ID
         * @param	{String}	"100%"		너비
         * @param	{String}	"100%"		높이
         */
        createIBSheet2( document.getElementById( "currArea" ), "currSheet", "100%", "100%" );
    },
    resize: function () {
    	 var currView, contentsArea_h, button_view_h;

    	 currView = this;
    	
         contentsArea_h = $( "#contentsArea" ).height(); 

         button_view_h = $( "#button_view" ).height(); 

         $( "#currArea" ).height( contentsArea_h - button_view_h - 10 ); //둘을 뺀다.

         $( window ).resize( function () {
         	contentsArea_h = $( "#contentsArea" ).height();

             button_view_h = $( "#button_view" ).height();
             //리사이징 작업
             $( "#currArea" ).height( contentsArea_h - button_view_h -10 );

             
         } );
    },
    create: function () {
        // 개발자 가이드 - 5.1 항목을 보고 작성
        var option = {};

        this.createSheet();

        option = {
            Cfg: {
                SearchMode: smLazyLoad, // smLazyLoad : 스크롤 조회 , smGeneral : 전체 조회
                Page: 30, //한번에 가져올 데이터 갯수
                AutoFitColWidth: "search|resize|init|colhidden|rowtransaction", //기본값 "" ,조회 및 로드 시점, 시트 Resize시점, 초키화 및 Removeall 호출 시점
                MergeSheet: msHeaderOnly+msPrevColumnMerge,
                DataRowHeight : 35
            },
            HeaderMode: {
                Sort: 0, //헤더 클릭을 통한 컬럼 Sort 허용 여부.0 사용안함, 1 sort기능 사용(디폴트), 2 아이콘만 표시, 헤더셀만 사용
                ColMove: 0, //헤더칼럼 이동여부
                ColResize: 1 //컬럼 너비 ReSize 여부
            },
            Cols: [ 
            	{
                    Header: "분류|분류",
                    Type: "Text",
                    Width: 50,
                    SaveName: "class_name",
                    Align: "Center",
                    Edit: 1,
                    Hidden: 0
                },
                { 
                    Header: "고유번호|고유번호", 
                    SaveName: "pkey", 
                    Type: "Popup", 
                    Width: 50,
                    ShowMobile: 0
                },
                {
                    Header: "교육과정 명|교육과정 명",
                    Type: "Text",
                    Width: 100,
                    SaveName: "cur_name",
                    Align: "Left",
                    Edit: 1,
                    Hidden: 0
                },
                {
                    Header: "강사 명|강사 명",
                    Type: "Text",
                    Width: 50,
                    SaveName: "manager_name",
                    Align: "Center",
                    Edit: 0,
                    Hidden: 0
                },
                {
                    Header: "강사 코드|강사 코드",
                    Type: "Text",
                    Width: 50,
                    SaveName: "manager_cod",
                    Align: "Center",
                    Edit: 0,
                    Hidden: 1
                },
                {
                    Header: "단가|단가",
                    Type: "Float",
                    Width: 50,
                    SaveName: "price",
                    Align: "Center",
                    Format: "#,###",
                    Edit: 1,
                    Hidden: 0
                },
                {
                    Header: "강좌 설명|강좌 설명",
                    Type: "Popup",
                    Width: 50,
                    SaveName: "contents",
                    ShowMobile: 0
                },
                {
                    Header: "수강기간|시작일",
                    Type: "Date",
                    Width: 50,
                    SaveName: "start_time",
                    Align: "Center",
                    Edit: 1,
                    Hidden: 0
                },
                {
                    Header: "수강기간|종료일",
                    Type: "Date",
                    Width: 50,
                    SaveName: "end_time",
                    Align: "Center",
                    Edit: 1,
                    Hidden: 0
                },
                {
                    Header: "수강 인원|현재 인원",
                    Type: "Text",
                    Width: 50,
                    SaveName: "persons",
                    Align: "Center",
                    Edit: 0,
                    Hidden: 0
                },
                {
                    Header: "수강 인원|정원",
                    Type: "Text",
                    Width: 50,
                    SaveName: "person_num",
                    Align: "Center",
                    Edit: 1,
                    Hidden: 0
                },
                {
                    Header: "삭제|삭제",
                    Type: "DelCheck",
                    Width: 30,
                    SaveName: "del",
                    Align: "Center",
                    Edit: 1,
                    Hidden: 0
                },
                {
                    Header: "상태|상태",
                    Type: "Status",
                    Width: 50,
                    SaveName: "sStatus",
                    Align: "Center",
                    Edit: 0,                   
                    Hidden: 1
                },
                {
                    Header: "사진|사진",
                    Type: "Text",
                    Width: 50,
                    SaveName: "img",
                    Align: "Center",
                    Edit: 0,                   
                    Hidden: 1
                },
                {
                    Header: "남자|남자",
                    Type: "Text",
                    Width: 50,
                    SaveName: "man",
                    Align: "Center",
                    Edit: 0,                   
                    Hidden: 1
                },
                {
                    Header: "여자|여자",
                    Type: "Text",
                    Width: 50,
                    SaveName: "woman",
                    Align: "Center",
                    Edit: 0,                   
                    Hidden: 1
                }
            ]
        }

        IBS_InitSheet( currSheet, option ); // treeSheet는 IBSheet의 createIBSheet2에서 생성된 전역객체
        //currSheet.SetTheme("TM", "TreeMain");
        this.bind();
        this.load();

    },
    bind: function () {
        var currView;

        currView = this;
        //tree_view를 직접 불러올수 없다.

        function addEvents( currSheet, callbacks ) {
            //IBSheet 기능을 강제로 연결하여 사용할수 있도록 함.
            for ( var prop in callbacks ) {
                window[ currSheet + "_" + prop ] = callbacks[ prop ];
            }
        }

        addEvents( currSheet.id, {
            OnSelectCell: function ( OldRow, OldCol, NewRow, NewCol, isDelete ) {
            	var selectValue, rowJson, newData;
   
            	$("#changeManager").off().click(function () {
            		selectValue = {
                			row : NewRow,
                			data : currSheet.GetCellValue(NewRow, 1)
                	}
                    	
                   	inapp.raise( currView.name, {
                            action: "dblclick_manager",
                            data: selectValue
                        } );
            	});
            	
      
 
           		$("#changeStudent").off().click(function () {
           			selectValue = {
               				row : NewRow,
               				data : currSheet.GetCellValue(NewRow, 1),
               				num : currSheet.GetCellValue(NewRow, 10)
               		}
                    	
                   	inapp.raise( currView.name, {
                            action: "dblclick_students",
                            data: selectValue
                        } );
            	});
         
            },
          	OnPopupClick: function ( row, col ){
          		var selectValue, rowJson, newData;
                
            	if( col == 1 ){
                    rowJson = currView.getRowJson( row );
                    
                    if(rowJson.manager_name==""){
                    	rowJson.manager_name = "없음";
                    }
            	    newData = {
            	    	cur_name : {value : rowJson.cur_name },
            	    	contents : {value : rowJson.contents },
            	    	price : {value : rowJson.price },
            	    	start_time : { value : rowJson.start_time.substring(0,4)+"-"+rowJson.start_time.substring(4,6)+"-"+rowJson.start_time.substring(6,8) },
            	    	end_time : { value : rowJson.end_time.substring(0,4)+"-"+rowJson.end_time.substring(4,6)+"-"+rowJson.end_time.substring(6,8) },
            	    	person_num : { value : rowJson.persons },
            	    	img : { value : rowJson.img },
            	    	manager_name : { value : rowJson.manager_name },
            	    	person_max_num : { value : rowJson.person_num },
            	    	manConut : { value : rowJson.man },
            	    	womanCount : { value : rowJson.woman }
            	    }
            	    
                    inapp.raise( currView.name, {
                        action: "dblclick_num",
                        data: {
                            fields: newData
                        }
                    } );

            	}
            	else if( col == 6 ){
        
            		selectValue = {
            				row : row,
            				col : col,
            				data : currSheet.GetCellValue(row, col)
            		}
	
                	inapp.raise( currView.name, {
                         action: "dblclick_contents",
                         data: selectValue
                     } );
            	}
          	},
            OnDblClick: function ( row , col ) {
                //더블클릭시 팝업호출
            	var selectValue, rowJson, newData;
            
            	
            	if( col == 3 ){
            		selectValue = {
            				row : row,
            				data : currSheet.GetCellValue(row, 1)
            		}
                	
                	inapp.raise( currView.name, {
                         action: "dblclick_manager",
                         data: selectValue
                     } );

            	}
            	else if( col == 9 ){
            		
            		selectValue = {
            				row : row,
            				data : currSheet.GetCellValue(row, 1),
            				num : currSheet.GetCellValue(row, 10)
            		}
                	
                	inapp.raise( currView.name, {
                         action: "dblclick_students",
                         data: selectValue
                     } );
            	}
            },
            OnRowSearchEnd: function (row ){
            	if (currSheet.GetCellValue(row, "persons") == currSheet.GetCellValue(row, "person_num")) {
            		currSheet.SetCellFontColor(row,"persons","#FF0000"); //색
                }
            }
        } );
    },
    load: function ( ) {
    	currSheet.ShowFilterRow();
    	currSheet.SetRowHidden(2, 1);
    	
    	$.ajax({
         	type: "POST",
         	url : "/selectCurData.do",
         	dataType: "json",
         	contentType : "application/json; charset=UTF-8",
         	success: function(resData) {
         		currSheet.LoadSearchData( resData );
         	}
         });
    },
    button: function () {
    	var that = this;
    	
    	//검색
    	$("#searchOn").click(function(){
    		var searchType ,searchValue;
    		searchType = $( "#searchType" ).val();
    		searchValue = $( "#searchValue" ).val();
    		console.log(searchType+":"+searchValue);
    		if( searchValue != "" ){
    			if(searchType==0){
    				currSheet.SetFilterOption(1 , 0);
    				currSheet.SetFilterOption(2 , 0);
        			currSheet.SetFilterOption(3 , 0);
            		currSheet.SetFilterValue(0, searchValue , 11);
            		$( "#searchValue" ).val("");
    			}else if(searchType==1){
    				currSheet.SetFilterOption(0 , 0);
        			currSheet.SetFilterOption(2 , 0);
        			currSheet.SetFilterOption(3 , 0);
            		currSheet.SetFilterValue(1, searchValue , 11);
            		$( "#searchValue" ).val("");
            	}else if(searchType==2){
            		currSheet.SetFilterOption(0 , 0);
            		currSheet.SetFilterOption(1 , 0);
        			currSheet.SetFilterOption(3 , 0);
            		currSheet.SetFilterValue(2, searchValue , 11);
            		$( "#searchValue" ).val("");
            	}else if(searchType==3){
            		currSheet.SetFilterOption(0 , 0);
            		currSheet.SetFilterOption(1 , 0);
        			currSheet.SetFilterOption(2 , 0);
            		currSheet.SetFilterValue(3, searchValue , 11);
            		$( "#searchValue" ).val("");
            	}
    		}else{
    			currSheet.SetFilterOption(0 , 0);
    			currSheet.SetFilterOption(1 , 0);
    			currSheet.SetFilterOption(2 , 0);
    			currSheet.SetFilterOption(3 , 0);
    		}
    	});
    	
    	//데이터 추가
    	$("#addData").click(function() {
    		if( currSheet.GetSelectCol() == 0){
    			that.addClass();
    		}else{
    			that.addEdu();
    		}
    	});
    	
    	//저장
    	$("#saveData").click(function() {
    		
    		that.save();
    		
    	});
    	
    	//조직도에서 관리 버튼 누를시
    	if( $("#searchMainId").val() != ""){
    		$( "#searchType" ).val(1);
    		$( "#searchValue" ).val($("#searchMainId").val());
    		$("#searchOn").trigger("click");
    	}
    },
    addEdu : function () {
    	
    	//선택된행, 선택된행의 class_name이 시작된행, 시작줄의 pkey(-을 자른,배열), 추가된 행, 키
		var selectrow, startRow, startPkey, addIndex, key;
		// 중복된 줄의 줄 번호들(1,2,3,..), duprows 배열, 중복된 줄들의 pkey , 중복된  줄의 pkey와 줄번호를 담는 배열, 중복된 줄 정렬용 배열,
		var duprows, dupArray , duprowsKey , saveArray, sortarray;
		//부모 줄 데이터 , 부모줄 pkey, 자식데이터(새줄에 넣을 데이터)
		var rowJson, newPkeyArray, newJson;
		
		//0001 0102 식으로 숫자 변환하는 함수
		function transform(n, width) {
			  	n = n + '';
			  	return n.length >= width ? n : new Array(width - n.length + 1).join('0') + n;
		}
		
		selectrow= currSheet.GetSelectRow(); //현재 선택된 행.
		
		if( currSheet.GetRowJson( selectrow ).class_name != ""){
			startRow = currSheet.FindText( "class_name", currSheet.GetRowJson( selectrow ).class_name ); // 선택된 행의 class_name 값의 시작번호.
			
			//중복을 그 줄부터 계산한 후 그 줄의 모든 데이터값을 가져온다.
			duprows = currSheet.ColValueDupRows( "class_name", {
				 "StartRow" : startRow
			} );
			
			rowJson={};
			
			if(duprows == "" || duprows == null){
				//없으면 선택된 행의 json값을 저장
				rowJson = currSheet.GetRowJson( selectrow );
			}else if(duprows.indexOf(",")=== -1){
				//1개있다면 그 행의 json값을 저장
				rowJson = currSheet.GetRowJson( duprows );	
			}else{
				//있다면 자른 후 해당 데이터의 pkey값중 젤 높은것을 검색		
				console.log(duprows);
				dupArray = duprows.split(",");
				
				if(dupArray[0] > 4 ){
					rowJson = currSheet.GetRowJson( selectrow );
				}else{
					saveArray = [];
					sortarray = [];

					//시작줄의 pkey 번호
					startPkey= currSheet.GetRowJson( startRow ).pkey.split('-');	
					
					//자른이후 해당 키값을 배열에 넣는다.
					for(var i=0; i<dupArray.length; i++){	
						//해당줄 pkey 번호
						duprowsKey=currSheet.GetRowJson( dupArray[i] ).pkey.split('-'); 
						
						//해당 분류가 같다면,
						if(startPkey[0]==duprowsKey[0]){
							//해당 pkey, 해당 줄
							//해당 값을 저장한다.
							saveArray[duprowsKey[1]] = Number(dupArray[i]); 
						}
					}
					
					//배열의 pkey들을 배열에넣고
					for(key in saveArray){
						sortarray.push(key);
					}
					
					// 큰 수 순서로 정렬
					sortarray.sort(function (a,b){
						return b-a;
					});
					
					//정렬된 값중 첫번째 값의 해당하는 줄의 rowJson을 가져온다.
					for(key in saveArray ) {
						
						if(key == sortarray[0]){
							
							rowJson = currSheet.GetRowJson( saveArray[key] );	
						}
					}
				}
			}

			console.log(rowJson.pkey);
			newPkeyArray=(rowJson.pkey).split('-'); //부모줄의 pkey
			
			addIndex = currSheet.DataInsert();	//행 추가
			
			//추가된 행의 데이터를 입력해줌
	    	
	    	currSheet.SetCellValue( addIndex, "class_name" , rowJson.class_name );
	    	currSheet.SetCellValue( addIndex, "pkey" , newPkeyArray[0]+"-"+transform( Number(newPkeyArray[1])+1 , 4) );
	    	currSheet.SetCellValue( addIndex, "persons" , 0 );
	    	
		}else{
			alert("분류에 빈칸이 있습니다.");
		}
		
    },
    addClass : function () {
    	var lastIndex, addIndex, dataArray, codArray, newArray , newpkey;
    	
    	//0001 0102 식으로 숫자 변환하는 함수
		function transform(n, width) {
			  	n = n + '';
			  	return n.length >= width ? n : new Array(width - n.length + 1).join('0') + n;
		}
		
		newpkey = "";
    	codArray= [];	
    	
    	lastIndex = currSheet.LastRow();
  
    	dataPkeyArray = (currSheet.GetRangeValue(2,1,lastIndex,1)).split('^');
    	
    	for(var i = 0, length=dataPkeyArray.length; i<length; i++){
    		codArray.push(dataPkeyArray[i].split('-')[0]);
    	}
    	
    	newArray = codArray.filter(function(value, index, self) { 
    		  return self.indexOf(value) === index;
    	});
    	
    	newArray.sort(function (a,b){
			return Number(b)-Number(a);
		});
    	
    	newpkey = transform( Number(newArray[0])+1 , 4)+"-0001";
		
		addIndex = currSheet.DataInsert(1);
		
		console.log(newpkey);
    	//추가된 행의 데이터를 입력해줌
    	this.setCellValue(addIndex, "pkey", newpkey );
    	this.setCellValue(addIndex, "persons", 0 );
    	
    },
    save: function (){
    	
    	var saveJson, i, length;
    	
    	saveJson = currSheet.GetSaveJson(); //수정할 데이터를 가져옴
    	
		if (saveJson.data.length <= 0) {
			if (saveJson.Message == "NoTargetRows") {
				alert("저장할 내역이 없습니다.");
				return;
			}
		}
		for(i= 0, length=saveJson.data.length ; i<length; i++){
			if(saveJson.data[i].cur_name == "" || saveJson.data[i].class_name == ""){
				alert("필수 입력 사항 미달");
				return;
			}
		}
		console.log(saveJson.data);
		$.ajax({
         	type: "POST",
         	url : "/saveCurData.do",
         	data : JSON.stringify(saveJson),
         	dataType: "json",
         	contentType : "application/json; charset=UTF-8",
         	success: function(resData) {
         		if(resData == "fail"){
         			alert("오류! 저장 실패.");
         		}
         		currSheet.LoadSearchData( resData );
         		alert("저장되었습니다.");
         	},
         	error : function( resData ) {
         		alert("오류! 저장 실패.");
         	}
         });	
		
    },
    getRowJson: function ( params ) {
        //컬럼 데이터 행의 json 값 리턴 함수
        //파라미터는 찾을 row 번호.
        var value;

        value = currSheet.GetRowJson( params );

        return value;
    },
    setCellValue: function ( row , saveName , data) {

    	currSheet.SetCellValue( row, saveName , data );
        
    }
} );