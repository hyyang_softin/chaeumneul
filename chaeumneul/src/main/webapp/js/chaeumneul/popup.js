inapp.add( "popup", {
    init: function () {
    
    },
    //데이터 받기
    actionMap: {
        "org_view": {
        	"click_node": function ( orgData ) {
                var data;
                data = orgData.data;
                
                if( data.binding == "emp_nm" ){
                	this.showMember(data);
                }
                else if( data.binding == "cur_name" && data.level == 3){
                	this.showCurr( data );
                }
            },
            "select_save_type": function () {
                this.selectSaveTypeShow();
            }
        },
        "manager_view" : {
        	"show_member": function ( memberData ){
        		this.showMember(memberData.data);
        	}
        },
        "students_view" : {
        	"show_member": function ( memberData ){
        		this.showMember(memberData.data);
        	},
        	"send_email_data":function ( emailData ){
        		console.log(emailData);
        		this.inputEmailAddressShow( emailData );
        	}
        }
    },
    // 교육과정 팝업
    showCurr: function ( nodeJsonData ) {
    	
    	var fieldData, that, personnelRatio;
    	that = this;
    	fieldData = nodeJsonData.fields;
    	
    	
    	$( '#currModalName' ).html( "&nbsp&nbsp"+fieldData.cur_name.value ); 
    	
    	$( '#currModalContents' ).html( fieldData.contents.value ); 
    	$( '#currModalPrice' ).html( fieldData.price.value +" 원" ); 
    	$( '#currModalTime' ).html( fieldData.start_time.value +" ~ "+ fieldData.end_time.value ); 
    	
    	$( '#currModalPersonNum' ).html( fieldData.person_num.value +" / " + fieldData.person_max_num.value ); 
    	$( '#currModalImg').html( fieldData.img.value );
    	$( '#currModalManagerName' ).html( fieldData.manager_name.value ); 
    	
    	personnelRatio =  parseInt(fieldData.person_num.value) / parseInt(fieldData.person_max_num.value) * 100; 
        
    	echarts.init( $( '#peopleNumChart' )[ 0 ] ).setOption(
    			{
                    series: [ {
                        data: [ personnelRatio ]
                    } ]
                }		
    	);
 
        echarts.init( $( '#genderChart' )[ 0 ] ).setOption(
        		{
                    series: [ {
                        data: [{
                        		name: '남',
                        		value : parseInt(fieldData.manConut.value)
                        	},
                        	{
                        		name: '여',
                        		value : parseInt(fieldData.womanCount.value)
                        	}]
                    }]
        		}
    	);
    	
        $( '#currModal' ).modal( 'show' );
        
        $( '#currModalManage' ).on( "click", function (){
        	
        	location.href = "management.do?id="+fieldData.pkey.value;
        } );
        
        
    },
    showMember: function ( nodeJsonData ) {
    	var fieldData;
    	fieldData = nodeJsonData.fields;
    	
    	$( '#memberModalImg' ).html( fieldData.img.value );
    	$( '#memberModalCod' ).html( "등록번호 : "+ fieldData.pkey.value ); 
    	$( '#memberModalName' ).html( "이름 : "+fieldData.emp_nm.value ); 
    	$( '#memberModalGender' ).html( "성별 : " + fieldData.gender.value ); 
    	$( '#memberModalAge' ).html( "나이 : " +fieldData.age.value +" 세" ); 
    	$( '#memberModalPhone' ).html( "전화번호 : "+ fieldData.phone_num.value );
    	if(nodeJsonData.fields.member_cod.value == 1){
    		$( '#memberModalSub' ).html( "경력 : "+ fieldData.sub.value );
    	}else{
    		$( '#memberModalSub' ).html( "이력서 : <button type='button' id='resumeshowpop' >보기</button>");
    	}
        $( '#memberModal' ).modal( 'show' );
        
        $( '#memberModalManage' ).on( "click", function (){
        	if(nodeJsonData.fields.member_cod.value == 1){
        		location.href = "managerview.do?Id="+fieldData.pkey.value;
        	}else{
        		location.href = "studentview.do?Id="+fieldData.pkey.value;
        	}
        } );
        
        $( '#resumeshowpop' ).on("click",function (){
        	window.open('/resumemodi.do?Id='+fieldData.pkey.value, 'newWindow', "width=900,height=100%");
        })
    },
    inputEmailAddressShow: function ( emailData ){
    	
    	var data = emailData;
    	
    	$( '#sendEmail' ).modal( 'show' );
    	
    	$( "#sendEmailButton" ).off().on( "click", function () {
            var newData, sendEmailAddress;
            
            sendEmailAddress = $( '#sendEmailAddress' ).val();
            newData = {
             		address : [
             			sendEmailAddress
             			],
             		data : emailData.data
             };
            
            $.ajax({
             	type: "POST",
             	url : "/sendResume.do",
             	data : JSON.stringify(newData),
             	dataType: "json",
             	contentType : "application/json; charset=UTF-8",
             	success: function(resData) {
             		if(resData == 1){
             			alert("전송되었습니다!");
             		}else{
             			alert("오류! 실패. 이메일을 확인해주세요");
             		}
             	},
             	error : function( resData ) {
             		alert("오류! 실패.");
             	}
             });

        } );
    },
    // 저장 버튼을 누를때 선택되는 팝업을 보여준다
    selectSaveTypeShow: function () {
        var selfThis = this;

        $( '#saveModal' ).modal( 'show' );

        $( "#saveAsImage" ).off().on( "click", function () {
            var fileName = $( '#saveName' ).val();

            selfThis.saveFile( 1, fileName );
            fileName = "";

        } );

        $( "#saveAsJson" ).off().on( "click", function () {
            var fileName = $( '#saveName' ).val();

            selfThis.saveFile( 2, fileName );
            fileName = "";

        } );

    },
    //저장 파일 생성 함수
    saveFile: function ( fileType, fileName ) {
        if ( fileType == 1 ) {
            if ( fileName != "" ) {
                viewOrg.saveAsImage( {
                    filename: fileName
                } );
            } else {
                viewOrg.saveAsImage();
            }
        } else if ( fileType == 2 ) {
            if ( fileName != "" )
                viewOrg.saveAsJson( {
                    filename: fileName
                } );
            else
                viewOrg.saveAsJson();
        }
        $( '#saveName' ).val( "" );
    }
} );