//var tree_view = {
inapp.add( "statistics", {
    cfg: {
    	circleCharts: [],
    	pictorialCharts: [],
        barCharts: []
    },
    actionMap: {
       
    },
    init: function () {
        this.create();
    },
    create: function () {
    	charts = [];
        //차트 추가시 밑에 추가하고, 옵션에 맞는 함수의 밑에다 추가 설정을한다.
  
        charts.push( this.echart_init( "pictorial", $( '#peopleManagerStudentChart' )[ 0 ] ) );
        charts.push( this.echart_init( "circle", $( '#peopleCurChart' )[ 0 ] ) );
        charts.push( this.echart_init( "pictorial", $( '#averageAgeChart' )[ 0 ] ) );
        charts.push( this.echart_init( "bar", $( '#priceCurChart' )[ 0 ] ) );

        //차트 추가시 여기에

        this.resize( charts );
        this.load();
    },
    echart_init: function ( type, value ) {
        var chartOptions, setting;

        chartOptions = this.basicChartOption();
        setting = echarts.init( value );

        if ( type == 'circle' ) {
            setting.setOption( chartOptions.circleChartOption );
            this.cfg.circleCharts.push( setting );

        } else if ( type == 'pictorial' ) {
            setting.setOption( chartOptions.pictorialChartOption );
            this.cfg.pictorialCharts.push( setting );

        } else if ( type == 'bar' ) {
            setting.setOption( chartOptions.barChartOption );
            this.cfg.barCharts.push( setting );
        }

        return setting;
    },
    load: function ( ) {
    	// 데이터 로드에 성공하면 자동으로 순서대로 해당 타입에 맞게 데이터를 가져와 그래프에 set하는 함수를 호출해줌 손댈필요x
        var newdata,that, circleCharts, pictorialCharts, barCharts, length, i;
        newdata = {};
        that = this;
        
        $.ajax({
         	type: "POST",
         	url : "/statisticsData.do",
         	dataType: "json",
         	contentType : "application/json; charset=UTF-8",
         	success: function(resData) {
         	
         		newdata = resData;
         		
         		circleCharts = that.cfg.circleCharts;
                pictorialCharts = that.cfg.pictorialCharts;
                barCharts = that.cfg.barCharts;

                for ( i = 0, length = circleCharts.length; i < length; i++ ) {
                    circleCharts[ i ].setOption( that.circleChartOption( i, newdata ) );
                }
                for ( i = 0, length = pictorialCharts.length; i < length; i++ ) {
                     pictorialCharts[ i ].setOption( that.pictorialChartOption( i, newdata ) );
                }
                for ( i = 0, length = barCharts.length; i < length; i++ ) {
                    barCharts[ i ].setOption( that.barChartOption( i, newdata ) );
                }
         	}
        });
      
    },
    resize: function ( charts ) {
        var chart;

        chart = charts;

        $( window ).resize( function () {
            //제이커리 리사이즈 이벤트
            var i, length;

            for ( i = 0, length = chart.length; i < length; i++ ) {
                charts[ i ].resize();
            }

        } );
    },
    //원형 차트
    circleChartOption: function ( params, trueData ) {
        var option, data, i;
        
        option = [];
        dataCurrName = [];
        dataCurrCountPeople= [];
       
        for(i=0; i<trueData.curData.length; i++){
        	var data = {};
        	dataCurrName.push(trueData.curData[i].curname);
        	data = {
        			name : trueData.curData[i].curname,
        			value : trueData.curData[i].countPeople
        		}
        	dataCurrCountPeople.push(data);
        }
        
        option = [ 
            {
                legend: {
                    data: dataCurrName
                },
                series: [ {
                    name: 'peopleNum',
                    data: dataCurrCountPeople
                } ]
            }
        ];

        return option[ params ];
    },
  //개별 픽토리얼 차트 옵션 개별 설정하기(차트 순서대로 할것)
    pictorialChartOption: function ( params, trueData ) {

        var option = [];
        
        option = [ {
        		series: [{
                    type: 'pictorialBar',
                    name: 'all',
                    label: {
                        show: true,
                        position: 'bottom',
                        formatter: '{b} 명',
                        fontSize: 16,
                        color: '#000'
                    },
                    data: [ 
                        {
                        name: trueData.dataCount.countManager,
                        value: 2,
                        symbol: 'image://'+ location.origin +'/img/onMan.png',
                        z: 10
                    },{
                        name: (trueData.dataCount.countStudent/trueData.dataCount.countManager).toFixed(2),
                        value: 2,
                        symbol: 'none',
                        label: {
                            show: true,
                            position: 'center',
                            offset : [20, 40 ],
                            formatter: '1 : {b}',
                            fontSize: 16,
                            color: '#000'
                        }
                    }
                    ,{
                        name: trueData.dataCount.countStudent,
                        value: 2,
                        symbol: 'image://'+ location.origin +'/img/manyMan.png'
                    }]
                }]
    		},
    		{
        		series: [{
                    type: 'pictorialBar',
                    name: 'all',
                    label: {
                        show: true,
                        position: 'bottom',
                        formatter: '{b} 명',
                        fontSize: 16,
                        color: '#000'
                    },
                    data: [ 
                        {
                        name: trueData.dataCount.allCountMan,
                        value: 2,
                        symbol: 'image://'+ location.origin +'/img/man.png',
                        z: 10
                    },{
                        value: 1,
                        symbol: 'none',
                        label: {
                            show: false,
                            position: 'center',
                            offset : [30, 50 ],
                            formatter: '1 : {b}',
                            fontSize: 16,
                            color: '#000'
                        }
                    }
                    ,{
                        name: trueData.dataCount.allCountWoman,
                        value: 2,
                        symbol: 'image://'+ location.origin +'/img/woman.png'
                    }]
                }]
    		}
        ];

        return option[ params ];
    },
    //개별 바(기본형) 차트 옵션 개별 설정하기(차트 순서대로 할것)
    barChartOption: function ( params, trueData ) {
        var option,dataCurrName,dataCurrCountPeople;
        
        option = [];

        dataCurrName = [];
        dataCurrCountPeople= [];
       
        for(i=0; i<trueData.curData.length; i++){
        	var data = {};
        	dataCurrName.push(trueData.curData[i].curname);
        	data = {
        			name : trueData.curData[i].curname,
        			value : trueData.curData[i].diviCurrPrice
        		}
        	dataCurrCountPeople.push(data);
        }
        
        option = [
            {
                legend: {
                    data: [ '분류별 단가 평균' ]
                },
                xAxis: [ {
                    data: dataCurrName
                } ],
                yAxis: [ {
                    name: '(원)'
                } ],
                series: [ {
                    name: '분류별 단가 평균',
                    type: 'bar',
                    stack: 'one',
                    label: {
                        show: true,
                        position: 'top',
                        formatter: '{c}',
                    },
                    data: dataCurrCountPeople
                } ]
            }
        ];

        return option[ params ];
    },
  //차트 기본옵션
    basicChartOption: function () {
        var option;

        option = {
            circleChartOption: {
            	color : ['#749f83', '#ca8622', '#bda29a','#6e7074', '#546570', '#c4ccd3'],
                legend: {
                    bottom: 10,
                    left: 'center',
                    data: []
                },
                series: [ {
                    name: 'evaluation',
                    type: 'pie',
                    radius: '65%',
                    center: [ '50%', '50%' ],
                    data: [],
                    label: {
                        show: true,
                        position: 'top',
                        distance: 40,
                        formatter: '{d}% \n ({c})'
                    }
                } ]
            },
            pictorialChartOption: {
                tooltip: {
                	show: false
                },
                xAxis: [{
                    data: [ 'man','-', 'woman'],
                    axisTick: {show: false},
                    axisLine: {show: false},
                    axisLabel: { show: false }
                }],
                yAxis: {
                    splitLine: {show: false},
                    axisTick: {show: false},
                    axisLine: {show: false},
                    axisLabel: {show: false}
                },
                markLine: {
                    z: -1
                },
                animationEasing: 'elasticOut',
                series: []
            },
            barChartOption: {
                legend: {
                    data: [],
                    bottom: 'bottom',
                    z: 10
                },
                color: [ '#3398DB', '#ccc' ],
                grid: {
                    left: '2%',
                    right: '1%',
                    containLabel: true
                },
                xAxis: [ {
                    type: 'category',
                    data: [],
                    axisTick: {
                        alignWithLabel: true
                    }
                } ],
                yAxis: [ {
                    name: '',
                    type: 'value'
                } ],
                series: []
            }
        };

        return option;
    }
} );