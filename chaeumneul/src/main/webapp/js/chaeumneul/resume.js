inapp.add( "resume", {
    init: function () {
    	this.create();

    },
    //데이터 받기
    actionMap: {
       
    },
    create: function ( ) {
    	
    	$('#exit').on('click', function(){
    		
    		close();   //자기자신창을 닫습니다.
    	});
        
        this.load();

    },
    load: function ( ) {
		var id, skill,self, skillArray, key;
		
		key = $('#key').val();
		skill = $('#skilldata').val();
		
		console.log(key);
		skillArray = [];
		
		skillArray = skill.split(",");
		
		if(key == "2"){
			$('#careerdate1').text(skillArray[0]); 
		    $('#careerdate2').text(skillArray[2]);
		    $('#careerdate3').text(skillArray[4]);
		    $('#careerdate4').text(skillArray[6]); 
		    $('#careerval1').text(skillArray[1]); 
		    $('#careerval2').text(skillArray[3]);
			$('#careerval3').text(skillArray[5]); 
		    $('#careerval4').text(skillArray[7]);
		}else{
			$('#careerdate1').val(skillArray[0]); 
		    $('#careerdate2').val(skillArray[2]);
		    $('#careerdate3').val(skillArray[4]);
		    $('#careerdate4').val(skillArray[6]); 
		    $('#careerval1').val(skillArray[1]); 
		    $('#careerval2').val(skillArray[3]);
			$('#careerval3').val(skillArray[5]); 
		    $('#careerval4').val(skillArray[7]);
		}
        
    	$('#saveresume').on('click' , function() {
    	       var id, careerdate1, careerdate2, careerdate3, careerdate4, careerval1, careerval2, careerval3,careerval4, selfintro, saveData;
    	       
    	       id = $('#idnum').val();
    	       careerdate1 = $('#careerdate1').val(); 
    	       careerdate2 = $('#careerdate2').val();
    	       careerdate3 = $('#careerdate3').val();
    	       careerdate4 = $('#careerdate4').val(); 
    	       careerval1 = $('#careerval1').val(); 
    	       careerval2 = $('#careerval2').val();
    		   careerval3 = $('#careerval3').val(); 
    	       careerval4 = $('#careerval4').val();
    	  
    	       selfintro = $('#selfintro').val();
    	       
    		   saveData = {
    				"ID" : id,
    				"skill" : careerdate1+","+careerval1+","
    						+careerdate2+","+careerval2+","
    						+careerdate3+","+careerval3+","
    						+careerdate4+","+careerval4,
    				"self" : selfintro
    		   };
    		  
    		   $.ajax({
    			    type: "POST",
    			    data : JSON.stringify(saveData),
    			    dataType: "json",
    			    url : "/saveResume.do",
    	         	contentType : "application/json; charset=UTF-8",
    			    success : function(data){
    			        alert("저장되었습니다");
    			    },error : function(){
    			        //Ajax 실패시
    			    }
    			});

       
    	    });
    	
    		$('#pdfresume').click(function() {
    		  //pdf_wrap을 canvas객체로 변환
    		  html2canvas($('#page')[0]).then(function(canvas) {
    		    var doc = new jsPDF('p', 'mm', 'a4'); //jspdf객체 생성
    		    var imgData = canvas.toDataURL('image/png'); //캔버스를 이미지로 변환
    		    doc.addImage(imgData, 'PNG', 0, 0); //이미지를 기반으로 pdf생성
    		    doc.save('sample-file.pdf'); //pdf저장
    		  });
    	});
    }
} );