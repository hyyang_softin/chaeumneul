//var tree_view = {
inapp.add( "manager_view", {
    cfg: {
    	files : {},
    	clickvalue : "",
    	clickrow : ""
    },
    actionMap: {
    },
    init: function () {
        this.create();
        this.resize();
        this.button();
        this.saveFile();
    },
    createSheet: function () {
        /**
         * IBSheet를 생성
         * @param 	{object}				IBSheet가 생성될 대상 컨테이너
         * @param	{object}				IBSheet의 ID
         * @param	{String}	"100%"		너비
         * @param	{String}	"100%"		높이
         */
        createIBSheet2( document.getElementById( "managerArea" ), "managerSheet", "100%", "100%" );
    },
    resize: function () {
    	 var managerView, contentsArea_h, button_view_h;

    	 managerView = this;
    	
         contentsArea_h = $( "#contentsArea" ).height(); 

         button_view_h = $( "#button_view" ).height(); 

         $( "#managerArea" ).height( contentsArea_h - button_view_h - 10 ); //둘을 뺀다.

         $( window ).resize( function () {
         	contentsArea_h = $( "#contentsArea" ).height();

             button_view_h = $( "#button_view" ).height();
             //리사이징 작업
             $( "#managerArea" ).height( contentsArea_h - button_view_h -10 );

             
         } );
    },
    create: function () {
        // 개발자 가이드 - 5.1 항목을 보고 작성
        var option = {};

        this.createSheet();

        option = {
            Cfg: {
                SearchMode: smLazyLoad, // smLazyLoad : 스크롤 조회 , smGeneral : 전체 조회
                Page: 30, //한번에 가져올 데이터 갯수
                AutoFitColWidth: "search|resize|init|colhidden|rowtransaction" //기본값 "" ,조회 및 로드 시점, 시트 Resize시점, 초키화 및 Removeall 호출 시점
            
            },
            HeaderMode: {
                Sort: 0, //헤더 클릭을 통한 컬럼 Sort 허용 여부.0 사용안함, 1 sort기능 사용(디폴트), 2 아이콘만 표시, 헤더셀만 사용
                ColMove: 0, //헤더칼럼 이동여부
                ColResize: 1 //컬럼 너비 ReSize 여부  
            },
            Cols: [
            	{
                    Header: "멤버번호",
                    Width: 20,
                    SaveName: "pkey",
                    Align: "Center",
                    Type: "Popup",   
                    ShowMobile: 0
                },
                {
                    Header: "사진",
                    Type: "html",
                    SaveName: "picture",
                    Align: "Center",
                    Width: 25
                },
                {
                	Header: "멤버 숫자 번호",
                    Type: "Int",
                    Width: 30,
                    SaveName: "id",
                    Align: "Center",
                    Edit: 0,
                    Hidden: 1
                },
            	{
                    Header: "강사 명",
                    Type: "Text",
                    Width: 30,
                    SaveName: "name",
                    Align: "Center",
                    Edit: 1,
                    Hidden: 0
                },
            	{
                    Header: "성별",
                    Type: "Combo",
                    Width: 20,
                    SaveName: "gender",
                    Align: "Center",
                    ComboText: "남|여",
                    ComboCode: "0|1",
                    ShowMobile: 0
                },
                {
                    Header: "나이",
                    Type: "Text",
                    Width: 30,
                    SaveName: "age",
                    Align: "Center",
                    Format: "PhoneNo"
                },
            	{
                    Header: "연락처",
                    Type: "Text",
                    Width: 50,
                    SaveName: "phone_num",
                    Align: "Center",
                    Format: "PhoneNo"
                },
                {
                    Header: "담당 강좌",
                    Type: "Text",
                    Width: 50,
                    SaveName: "regi_cur",
                    Align: "Center",
                    Edit: 0,
                    Hidden: 1
                },
                {
                    Header: "사진변경 여부",
                    Type: "Text",
                    SaveName: "picture_yn",
                    Align: "Center",
                    Edit: 0,
                    Hidden: 1
                },
                { 
                    Header: "경력", 
                    Type: "Text", 
                    Width: 50, 
                    SaveName: "sub", 
                    Align: "Center", 
                    Edit: 1, 
                    Hidden: 0 
                }, 
                {
                    Header: "인건비",
                    Type: "Float",
                    Width: 50,
                    SaveName: "lobor_cost",
                    Format: "#,###",
                    Align: "Center",
                    Edit: 1,
                    Hidden: 0
                },
                {
                    Header: "삭제",
                    Type: "DelCheck",
                    Width: 20,
                    SaveName: "del",
                    Align: "Center",
                    Edit: 1,
                    Hidden: 0
                },
                {
                    Header: "상태",
                    Type: "Status",
                    Width: 50,
                    SaveName: "sStatus",
                    Align: "Center",
                    Edit: 0,                   
                    Hidden: 1
                }
            ]
        }

        IBS_InitSheet( managerSheet, option ); 
//        managerSheet.SetTheme("TM", "TreeMain");
        
        this.bind();
        this.load();

    },
    bind: function () {
        var managerView;

        managerView = this;
        //tree_view를 직접 불러올수 없다.

        function addEvents( managerSheet, callbacks ) {
            //IBSheet 기능을 강제로 연결하여 사용할수 있도록 함.
            for ( var prop in callbacks ) {
                window[ managerSheet + "_" + prop ] = callbacks[ prop ];
            }
        }

        addEvents( managerSheet.id, {
            OnSelectCell: function ( OldRow, OldCol, NewRow, NewCol, isDelete ) {
           
            	$("#imgUpload").off().click(function () {
            		managerView.cfg.clickvalue = managerSheet.GetCellValue( NewRow, 0 );
                	managerView.cfg.clickrow = NewRow;
                		
                	$("#uploadInputBox").trigger("click");
            	});
            	
            },
          	OnPopupClick: function ( row, col ){
          		var selectValue, rowJson, newData, idindex, photoindex, pictureURI; 	
                
            	if(col == 0){
            		rowJson = managerView.getRowJson( row );    
            		
            		idindex = rowJson.picture.indexOf("http")
            		
            		photoindex =rowJson.picture.indexOf("style");
            		pictureURI =rowJson.picture.substring(idindex, photoindex);
            		
            		newData={ 
            				fields: {
            					img : {value : pictureURI +" style='width: 100px;'>"
            							},
            					pkey : {value : rowJson.pkey},
            					emp_nm : {value : rowJson.name},
            					age : {value : rowJson.age},
            					phone_num : {value : rowJson.phone_num},
            					sub : {value : rowJson.sub},
            					member_cod : {value : 1}
            				}
            		};
            	
            		if(rowJson.gender == 0){
            			newData.fields.gender = {value : "남"};
            		}else{
            			newData.fields.gender = {value : "여"};
            		}
                    //데이터를 전부 전달.
                    inapp.raise( managerView.name, {
                        action: "show_member",
                        data: newData
                    } );
                    
            	}
            	
          	},
            OnDblClick: function ( row , col ) {
            	//더블클릭시 팝업호출
            	var selectValue, rowJson, newData, idindex, photoindex, pictureURI; 	
            
            	if(col == 1){
            		managerView.cfg.clickvalue = managerSheet.GetCellValue( row, 0 );
            		managerView.cfg.clickrow = row;
            		
            		$("#uploadInputBox").trigger("click");
            	}	
            }
        } );
    },
    load: function ( ) {
    	managerSheet.ShowFilterRow();
    	managerSheet.SetRowHidden(1, 1);
    	
      	$.ajax({
         	type: "POST",
         	url : "/selectMangerData.do",
         	dataType: "json",
         	contentType : "application/json; charset=UTF-8",
         	success: function(resData) {
        
         		managerSheet.LoadSearchData( resData );
         	}
         });
    },
    button: function () {
    	var that = this;
    	
    	$("#searchOn").click(function(){
    		var searchType ,searchValue;
    		searchType = $( "#searchType" ).val();
    		searchValue = $( "#searchValue" ).val();
    		
    		if( searchValue != "" ){
    			if(searchType==0){
    				managerSheet.SetFilterOption(2 , 0);
    				managerSheet.SetFilterValue(0, searchValue , 9);
    				$( "#searchValue" ).val("");
    			}else if(searchType==1){
    				managerSheet.SetFilterOption(0 , 0);
    				managerSheet.SetFilterValue(2, searchValue , 11);
    				$( "#searchValue" ).val("");
            	}
    		}else{
    			managerSheet.SetFilterOption(0 , 0);
    			managerSheet.SetFilterOption(2 , 0);
    		}
    	});
    	
    	$("#addData").click(function() {
    		var IndexValue, lastIndex, firstIndexValue, lastIndexValue, highIndexValue, addIndex, newIndexValue, domain,idindex, photoindex,searchURI, i;
    		
    		domain= "";
    		
    		IndexValue = managerSheet.GetCellValue( managerSheet.GetSelectRow(), 2 );
    		
    		//자동 pkey 입력
    		newIndexValue = managerSheet.GetColMaxValue(2)+1;	
    		addIndex = managerSheet.DataInsert();
    		that.setCellValue(addIndex, "pkey", 1+"-"+newIndexValue);
    		that.setCellValue(addIndex, "id", newIndexValue);
    		
    		//자동 사진 입력
    		searchURI = managerSheet.GetCellValue(IndexValue , 1);
    		idindex = searchURI.indexOf('http')
    		photoindex =searchURI.indexOf('photo');
    		domain =searchURI.substring(idindex, photoindex);
    		that.setCellValue(addIndex, "picture", "<img id= '1-"+newIndexValue+"' src='/img/photo/default.png' style='height: 60px; width: 60px;'>");
    	});
    	
    	$("#saveData").click(function() {
    		var files = that.cfg.files;
    		
    		//가상의 폼만들기.
    		var form = $('#uploadForm')[0];
    		var formData = new FormData(form);
    		
    		var keys = Object.keys(files);
    		
    		for (var index = 0; index < Object.keys(files).length; index++) {
    			//formData 공간에 files라는 이름으로 파일을 추가한다.
    			//동일명으로 계속 추가할 수 있다.
    			formData.append('files',files[keys[index]]);
    			formData.append('name',keys[index]);
    		}
    	
    		if( Object.keys(files).length > 0){
    			
    			//ajax 통신으로 multipart form을 전송한다.
        		$.ajax({
        			type : 'POST',
                    enctype : 'multipart/form-data',
                    processData : false,
                    contentType : false,
                    cache : false,
                    timeout : 600000,
                    url : '/multiImageUpload.do',
                    dataType : 'JSON',
                    data : formData,
                    success : function(result) {
                    	if (result === -1) {
                    		alert('jpg, gif, png, bmp 확장자만 업로드 가능합니다.');
     
                    	} else if (result === -2) {
                    		alert('파일이 10MB를 초과하였습니다.');
       
                    	} else {
        
                    		
                    		that.save();
                    	}
                    }  
        		});
    		}else{
    			that.save();
    		}
    	});
    	
    	if( $("#searchMainManagerId").val() != ""){
    		console.log($("#searchMainManagerId").val());
    		$( "#searchType" ).val(0);
    		$( "#searchValue" ).val($("#searchMainManagerId").val());
    		$("#searchOn").trigger("click");
    	}
    },
    save: function (){
    	var files, saveJson;
    	files = this.cfg.files;
    	saveJson = managerSheet.GetSaveJson(); //수정할 데이터를 가져옴
    	
		if (saveJson.data.length <= 0) {
			if (saveJson.Message == "NoTargetRows" ) {
				alert("저장할 내역이 없습니다.");
				return;
			}
		}
		
		for(i= 0, length=saveJson.data.length ; i<length; i++){
			if(saveJson.data[i].pkey == "" || saveJson.data[i].name == ""){
				alert("필수 입력 사항 미달");
				return;
			}
		}
		
		$.ajax({
         	type: "POST",
         	url : "/saveManagerData.do",
         	data : JSON.stringify(saveJson),
         	dataType: "json",
         	contentType : "application/json; charset=UTF-8",
         	success: function(resData) {
         		if(resData == "fail"){
         			alert("오류! 저장 실패1.");
         		}
         		
         		alert("저장되었습니다.");
         		
         		setTimeout(function() {
         			managerSheet.LoadSearchData( resData );
         		}, 100);
         	},
         	error : function( resData ) {
         		alert("오류! 저장 실패.");
         	}
         });	
		
    },
    getRowJson: function ( params ) {
        //컬럼 데이터 행의 json 값 리턴 함수
        //파라미터는 찾을 row 번호.
        var value;

        value = managerSheet.GetRowJson( params );

        return value;
    },
    setCellValue: function ( row , saveName , data) {

    	managerSheet.SetCellValue( row, saveName , data );
        
    },
    saveFile : function (){
    	
    	var that, previewIndex;
    	
    	that =this;
    	previewIndex = 0;
    	
    	//유효성 검사
    	function addPreview(input,id) {
    		if (input[0].files) {
    	    //파일 선택이 여러개였을 시의 대응
    	    for (var fileIndex = 0; fileIndex < input[0].files.length; fileIndex++) {
    	    	var file = input[0].files[fileIndex];
    	    	
    	    	if(validation(file.name)){ 
    	    		continue;
    	    	}
    	    	
    	    	setPreviewForm(file,id);
    	    	}
    		} else
    			alert('invalid file input'); 
    	}

    	//미리보기
    	function setPreviewForm(file,id, img){
    		var reader = new FileReader();
    	   	var value = "#"+id;
    		
    	    reader.onload = function(img) {
    	    	
    	    
    	    	$( value ).attr("src" , img.target.result);
    	    	
    	    		that.cfg.files[value] = file;  
    	    		
    	    	};
    	 
    	    	reader.readAsDataURL(file);
    	    	
    	    	
    	}
    	 

    	function validation(fileName) {
    		fileName = fileName + "";
    		var fileNameExtensionIndex = fileName.lastIndexOf('.') + 1;
    		var fileNameExtension = fileName.toLowerCase().substring(
    					fileNameExtensionIndex, fileName.length);
    		if (!((fileNameExtension === 'jpg')
    				|| (fileNameExtension === 'gif') || (fileNameExtension === 'png'))) {
    			alert('jpg, gif, png 확장자만 업로드 가능합니다.');
    			return true;
    		} else {
    			return false;
    		}
    	}

    	$('#attach input[type=file]').change(function() {
    		addPreview($(this), that.cfg.clickvalue); //preview form 추가하기
    		
    		that.setCellValue( that.cfg.clickrow ,"picture_yn", "Y");
    	});
    	
    }
} );