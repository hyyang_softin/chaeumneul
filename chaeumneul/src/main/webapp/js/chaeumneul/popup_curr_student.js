inapp.add( "popup_curr_student", {
	cfg: {
	    regi_cur : "",
	    regi_cur_pn : 0
	},
    init: function () {
    	this.create();
    	this.button();
    },
    //데이터 받기
    actionMap: {
        "curr_view":{
        	"dblclick_students": function ( studentdata ) {
        		this.cfg.regi_cur = studentdata.data.data;
        		this.cfg.regi_cur_pn = parseInt(studentdata.data.num);
        		this.load(studentdata.data);
        		this.showStudentStatus(studentdata.data);
        	}
        }
    },
    createSheet: function () {
        /**
         * IBSheet를 생성
         * @param 	{object}				IBSheet가 생성될 대상 컨테이너
         * @param	{object}				IBSheet의 ID
         * @param	{String}	"100%"		너비
         * @param	{String}	"100%"		높이
         */
        createIBSheet2( document.getElementById( "currPopupStudent1Area" ), "currPopupStudent1Sheet", "100%", "100%" );
        
        createIBSheet2( document.getElementById( "currPopupStudent2Area" ), "currPopupStudent2Sheet", "100%", "100%" );
    },
    create: function () {
        // 개발자 가이드 - 5.1 항목을 보고 작성
        var option = {};

        this.createSheet();

        option = {
            Cfg: {
            	DragMode: 1, // smLazyLoad : 스크롤 조회 , smGeneral : 전체 조회
                AutoFitColWidth: "search|resize|init|colhidden|rowtransaction", //기본값 "" ,조회 및 로드 시점, 시트 Resize시점, 초키화 및 Removeall 호출 시점
                DeferredVScroll: 1
            },
            HeaderMode: {
                Sort: 0, //헤더 클릭을 통한 컬럼 Sort 허용 여부.0 사용안함, 1 sort기능 사용(디폴트), 2 아이콘만 표시, 헤더셀만 사용
                ColMove: 0, //헤더칼럼 이동여부
                ColResize: 1 //컬럼 너비 ReSize 여부
               
            },
            Cols: [
            	{
                    Header: "번호",
                    Type: "Text",
                    Width: 20,
                    SaveName: "pkey",
                    Align: "Center",
                    Edit: 0,
                    Hidden: 0
                },
            	{
                    Header: "이름",
                    Type: "Text",
                    Width: 50,
                    SaveName: "name",
                    Align: "Center",
                    Edit: 0,
                    Hidden: 0
                },
            	{
                    Header: "연락처",
                    Type: "Text",
                    Width: 40,
                    SaveName: "phone_num",
                    Align: "Center",
                    Format: "PhoneNo",
                    Edit: 0
                },
                {
                    Header: "등록 강좌",
                    Type: "Text",
                    Width: 50,
                    SaveName: "regi_cur",
                    Align: "Center",
                    Edit: 0,
                    Hidden: 1
                }
            ]
        }

        IBS_InitSheet( currPopupStudent1Sheet, option ); 
        
        
        option = {
                Cfg: {
                	DragMode: 1, // smLazyLoad : 스크롤 조회 , smGeneral : 전체 조회
                    AutoFitColWidth: "search|resize|init|colhidden|rowtransaction", //기본값 "" ,조회 및 로드 시점, 시트 Resize시점, 초키화 및 Removeall 호출 시점
                    DeferredVScroll: 1
                },
                HeaderMode: {
                    Sort: 0, //헤더 클릭을 통한 컬럼 Sort 허용 여부.0 사용안함, 1 sort기능 사용(디폴트), 2 아이콘만 표시, 헤더셀만 사용
                    ColMove: 0, //헤더칼럼 이동여부
                    ColResize: 1 //컬럼 너비 ReSize 여부
                   
                },
                Cols: [
                	{
                        Header: "번호",
                        Type: "Text",
                        Width: 20,
                        SaveName: "pkey",
                        Align: "Center",
                        Edit: 0,
                        Hidden: 0
                    },
                	{
                        Header: "이름",
                        Type: "Text",
                        Width: 50,
                        SaveName: "name",
                        Align: "Center",
                        Edit: 0,
                        Hidden: 0
                    },
                	{
                        Header: "연락처",
                        Type: "Text",
                        Width: 40,
                        SaveName: "phone_num",
                        Align: "Center",
                        Format: "PhoneNo",
                        Edit: 0
                    },
                    {
                        Header: "등록 강좌",
                        Type: "Text",
                        Width: 50,
                        SaveName: "regi_cur",
                        Align: "Center",
                        Edit: 0,
                        Hidden: 0
                    }
                ]
            }
        
        IBS_InitSheet( currPopupStudent2Sheet, option ); 

        currPopupStudent1Sheet.SetEditable(0);
    	currPopupStudent2Sheet.SetEditable(0);
    	
        this.bind();
        //this.load();

    },
    bind: function () {
        var currPopupStudentView;

        currPopupStudentView = this;
        //tree_view를 직접 불러올수 없다.

        function addEvents1( currPopupStudent1Sheet, callbacks ) {
            //IBSheet 기능을 강제로 연결하여 사용할수 있도록 함.
            for ( var prop in callbacks ) {
                window[ currPopupStudent1Sheet + "_" + prop ] = callbacks[ prop ];
            }
        }
        
        function addEvents2( currPopupStudent2Sheet, callbacks ) {
            //IBSheet 기능을 강제로 연결하여 사용할수 있도록 함.
            for ( var prop in callbacks ) {
                window[ currPopupStudent2Sheet + "_" + prop ] = callbacks[ prop ];
            }
        }

        addEvents1( currPopupStudent1Sheet.id, {
        	OnDropEnd : function (FromSheet, FromRow, ToSheet, ToRow, X, Y, Type){
        		console.log("1");
        		if (FromSheet == ToSheet) return;
                var rowjson = FromSheet.GetRowData(FromRow);
                //행 데이터 복사
                ToSheet.SetRowData(ToRow + 1, rowjson, {
                    "Add": 1
                });
                //원본 데이터 삭제
                FromSheet.RowDelete(FromRow);
        	}
        } );
        
        addEvents2( currPopupStudent2Sheet.id, {
        	OnDropEnd : function (FromSheet, FromRow, ToSheet, ToRow, X, Y, Type){
        		console.log("2");  
        		var rowjson = FromSheet.GetRowData(FromRow),
                posRow = FromRow;
        		if (FromSheet == ToSheet && ToRow < FromRow) {
                //같은 시트내에서 이동은 신경을 써야 함.
        			posRow++;
        		}
        		
        		//행 데이터 복사(트리임으로 레벨을 고려할 것)
        		ToSheet.SetRowData(ToRow + 1, rowjson, {
        			"Add": 1,
        			"Level": 3
        		});
        		//원본 데이터 삭제
        		FromSheet.RowDelete(posRow);
        	}
        } );
    },
    load: function ( studentData ) {
    	
    	currPopupStudent2Sheet.ShowFilterRow();
    	currPopupStudent2Sheet.SetRowHidden(1, 1);
    	currPopupStudent2Sheet.SetFilterValue(3, '' , 1);
    	currPopupStudent2Sheet.SetFilterOption(1 , 0);
		currPopupStudent2Sheet.SetFilterOption(0 , 0);
    	$("#addStudentsearchType").val("3").prop("selected", true);
    	$( "#addStudentsearchValue" ).hide();
    	$( "#addStudentsearchOn" ).hide();
    	
    	$.ajax({
         	type: "POST",
         	url : "/addStudent1Data.do",
         	data : studentData.data,
         	dataType: "json",
         	contentType : "application/json; charset=UTF-8",
         	success: function(resData) {
  
         		currPopupStudent1Sheet.LoadSearchData( resData );
         	}
         });
    	
    	$.ajax({
         	type: "POST",
         	url : "/addStudent2Data.do",
         	data : studentData.data,
         	dataType: "json",
         	contentType : "application/json; charset=UTF-8",
         	success: function(resData) {

         		currPopupStudent2Sheet.LoadSearchData( resData );
         	}
         });
    	
    },
    showStudentStatus: function ( studentData ){
  	
    	$( '#addStudentsModal' ).modal({backdrop: 'static'});
	
    },
    button : function (){
    	var that;
    	
    	that = this;
    	
    	$("#addStudentsModalSave").click(function() {
    		var i, newData, me;
    		
    		newData = currPopupStudent1Sheet.GetSaveJson({AllSave:1});
    		me=that;
    		console.log(newData);
    		
    		if( newData.data.length > that.cfg.regi_cur_pn ){
    			alert("정원초과입니다...");
    			return;
    		}
    		if(newData.data.length > 0){
    			for(i=0; i<newData.data.length; i++){
        			newData.data[i].newcur = that.cfg.regi_cur;
        		}
    		}else{
    			newData = { data:[]};
    			newData.data[0] = {
    					pkey : "0000-0000",
    					newcur : that.cfg.regi_cur
    				};
    		}
    		
    		$.ajax({
             	type: "POST",
             	url : "/changeStudents.do",
             	data : JSON.stringify(newData),
             	dataType: "json",
             	contentType : "application/json; charset=UTF-8",
             	success: function(resData) {
             		inapp.raise( me.name, {
                        action: "success",
                        data: 1
                    } );
             	}
             });
    	});
    	
    	$( "#addStudentsearchType" ).on( "change", function () {
            if($( "#addStudentsearchType" ).val() == 3){
            	$( "#addStudentsearchValue" ).hide();
            	$( "#addStudentsearchOn" ).hide();
            	
            	currPopupStudent2Sheet.SetFilterValue(3, '' , 1);
            	currPopupStudent2Sheet.SetFilterOption(1 , 0);
    			currPopupStudent2Sheet.SetFilterOption(0 , 0);
            }else{
            	$( "#addStudentsearchValue" ).show();
            	$( "#addStudentsearchOn" ).show();
            	currPopupStudent2Sheet.SetFilterOption(3 , 0);
            	currPopupStudent2Sheet.SetFilterOption(1 , 0);
    			currPopupStudent2Sheet.SetFilterOption(0 , 0);
            }
    		
        } );
    	
    	$("#addStudentsearchOn").click(function(){
    		var searchType ,searchValue;
    		searchType = $( "#addStudentsearchType" ).val();
    		searchValue = $( "#addStudentsearchValue" ).val();
    		
    		if( searchValue != "" ){
    			if(searchType==0){
    				currPopupStudent2Sheet.SetFilterOption(1 , 0);
    				currPopupStudent2Sheet.SetFilterValue(0, searchValue , 9);
    				$( "#addStudentsearchValue" ).val("");
    			}else if(searchType==1){
    				currPopupStudent2Sheet.SetFilterOption(0 , 0);
    				currPopupStudent2Sheet.SetFilterValue(1, searchValue , 11);
    				$( "#addStudentsearchValue" ).val("");
            	}
    		}else{
    			currPopupStudent2Sheet.SetFilterOption(1 , 0);
    			currPopupStudent2Sheet.SetFilterOption(0 , 0);
    		}
    	});
    	
    	$("#leftStudentMove").click(function(){
    		var row, idArray,jsonData, addIndex;
    		row = currPopupStudent2Sheet.GetSelectRow();
    		
    		jsonData = currPopupStudent2Sheet.GetRowJson( row );
    		
    		addIndex = currPopupStudent1Sheet.DataInsert(-1);	//행 추가
    		
    		currPopupStudent2Sheet.RowDelete(row);
    		currPopupStudent1Sheet.SetCellValue( addIndex, "pkey" , jsonData.pkey );
    		currPopupStudent1Sheet.SetCellValue( addIndex, "name" , jsonData.name );
    		currPopupStudent1Sheet.SetCellValue( addIndex, "phone_num" , jsonData.phone_num );
    	});
    	
    	$("#rightStudentMove").click(function(){
    		var row, idArray,jsonData, addIndex;
    		
    		row = currPopupStudent1Sheet.GetSelectRow();
    		
    		jsonData = currPopupStudent1Sheet.GetRowJson( row );
    		
    		addIndex = currPopupStudent2Sheet.DataInsert(-1);	//행 추가
    		
    		currPopupStudent1Sheet.RowDelete(row);
    		currPopupStudent2Sheet.SetCellValue( addIndex, "pkey" , jsonData.pkey );
    		currPopupStudent2Sheet.SetCellValue( addIndex, "name" , jsonData.name );
    		currPopupStudent2Sheet.SetCellValue( addIndex, "phone_num" , jsonData.phone_num );
    	});
  
    }
} );