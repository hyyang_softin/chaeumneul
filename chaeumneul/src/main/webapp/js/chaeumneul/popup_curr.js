inapp.add( "popup_curr", {
    init: function () {
    	this.create();
    	this.chartCreate();
    },
    //데이터 받기
    actionMap: {
        "curr_view":{
        	"dblclick_num": function ( currdata ) {
        		this.showCurr( currdata.data );
        	},
        	"dblclick_manager": function ( selectValue ) {
        		this.load();
        		this.showSelectManager( selectValue );
        	},
        	"dblclick_contents": function ( selectValue ) {
        		this.showSelectContnets( selectValue );
        	}
        }
    },
    createSheet: function () {
        /**
         * IBSheet를 생성
         * @param 	{object}				IBSheet가 생성될 대상 컨테이너
         * @param	{object}				IBSheet의 ID
         * @param	{String}	"100%"		너비
         * @param	{String}	"100%"		높이
         */
        createIBSheet2( document.getElementById( "currPopupArea" ), "currPopupSheet", "100%", "100%" );
    },
    create: function () {
        // 개발자 가이드 - 5.1 항목을 보고 작성
        var option = {};

        this.createSheet();

        option = {
            Cfg: {
                SearchMode: smLazyLoad, // smLazyLoad : 스크롤 조회 , smGeneral : 전체 조회
                Page: 30, //한번에 가져올 데이터 갯수
                AutoFitColWidth: "search|resize|init|colhidden|rowtransaction", //기본값 "" ,조회 및 로드 시점, 시트 Resize시점, 초키화 및 Removeall 호출 시점
                MergeSheet: msHeaderOnly+msPrevColumnMerge
            },
            HeaderMode: {
                Sort: 0, //헤더 클릭을 통한 컬럼 Sort 허용 여부.0 사용안함, 1 sort기능 사용(디폴트), 2 아이콘만 표시, 헤더셀만 사용
                ColMove: 0, //헤더칼럼 이동여부
                ColResize: 1 //컬럼 너비 ReSize 여부
               
            },
            Cols: [
            	{
                    Header: "번호",
                    Type: "Text",
                    Width: 20,
                    SaveName: "pkey",
                    Align: "Center",
                    Edit: 0,
                    Hidden: 0
                },
            	{
                    Header: "강사 명",
                    Type: "Text",
                    Width: 50,
                    SaveName: "name",
                    Align: "name",
                    Edit: 0,
                    Hidden: 0
                },
            	{
                    Header: "성별",
                    Type: "Text",
                    Width: 50,
                    SaveName: "gender",
                    Align: "name",
                    Edit: 0,
                    Hidden: 1
                },
            	{
                    Header: "연락처",
                    Type: "Text",
                    Width: 40,
                    SaveName: "phone_num",
                    Align: "Center",
                    Format: "PhoneNo",
                    Edit: 0
                },
                {
                    Header: "담당 강좌",
                    Type: "Text",
                    Width: 50,
                    SaveName: "regi_cur",
                    Align: "Center",
                    Edit: 0,
                    Hidden: 0
                },
                {
                    Header: "사진",
                    Type: "Text",
                    Width: 50,
                    SaveName: "picture_name",
                    Align: "Center",
                    Edit: 0,
                    Hidden: 1
                },
                { //칼럼에 대한 정보. 시트에서 표현할 정보 나열
                    Header: "경력", //제목
                    Type: "Text", //타입
                    Width: 50, // 넓이
                    SaveName: "sub", //INOrg 필드에 있는거 중에 가져올 데이터 이름.
                    Align: "Center", //정렬
                    Edit: 0, // 편집 가능여부. 0-불가 1- 가능
                    Hidden: 0 // 숨김여부 0-보임 1-숨김
                }, 
                {
                    Header: "인건비",
                    Type: "Text",
                    Width: 50,
                    SaveName: "lobor_cost",
                    Align: "Center",
                    Edit: 0,
                    Hidden: 1
                }
            ]
        }

        IBS_InitSheet( currPopupSheet, option ); // treeSheet는 IBSheet의 createIBSheet2에서 생성된 전역객체

        this.bind();
        //this.load();

    },
    bind: function () {
        var currPopupView;

        currPopupView = this;
        //tree_view를 직접 불러올수 없다.

        function addEvents( currPopupSheet, callbacks ) {
            //IBSheet 기능을 강제로 연결하여 사용할수 있도록 함.
            for ( var prop in callbacks ) {
                window[ currSheet + "_" + prop ] = callbacks[ prop ];
            }
        }

        addEvents( currPopupSheet.id, {
            OnSelectCell: function ( OldRow, OldCol, NewRow, NewCol, isDelete ) {
            
                
            },
            OnDblClick: function ( row , col ) {
              
            }
        } );
    },
    load: function ( ) {
    	currPopupSheet.ShowFilterRow();
    	currPopupSheet.SetRowHidden(1, 1);
    	currPopupSheet.SetFilterValue(4, '' , 1);
    	currPopupSheet.SetFilterOption(1 , 0);
    	currPopupSheet.SetFilterOption(0 , 0);
    	$("#modalsearchType").val("3").prop("selected", true);
    	$( "#modalsearchValue" ).hide();
    	$( "#modalsearchOn" ).hide();
    	
    	$.ajax({
         	type: "POST",
         	url : "/selectMangerData.do",
         	dataType: "json",
         	contentType : "application/json; charset=UTF-8",
         	success: function(resData) {
        
         		currPopupSheet.LoadSearchData( resData );
         	}
         });
    	
    },
    // 교육과정 팝업
    showCurr: function ( nodeJsonData ) {
    	
    	var fieldData = nodeJsonData.fields , personnelRatio;
    	
    	$( '#currModalName' ).html( "&nbsp&nbsp"+fieldData.cur_name.value ); 
    	$( '#currModalContents' ).html( fieldData.contents.value ); 
    	$( '#currModalPrice' ).html( fieldData.price.value +" 원" ); 
    	$( '#currModalTime' ).html( fieldData.start_time.value +" ~ "+ fieldData.end_time.value ); 
    	
    	$( '#currModalPersonNum' ).html(fieldData.person_num.value +" / " + fieldData.person_max_num.value); 
    	$( '#currModalImg').html( fieldData.img.value );
    	
    	$( '#currModalManagerName' ).html( fieldData.manager_name.value ); 
    	
        $( '#currModal' ).modal( 'show' );
        
        personnelRatio =  parseInt(fieldData.person_num.value) / parseInt(fieldData.person_max_num.value) * 100; 
        echarts.init( $( '#peopleNumChart' )[ 0 ] ).setOption(
    			{
                    series: [ {
                        data: [ personnelRatio ]
                    } ]
                }		
    	);
 
        echarts.init( $( '#genderChart' )[ 0 ] ).setOption(
        		{
                    series: [ {
                        data: [{
                        		name: '남',
                        		value : parseInt(fieldData.manConut.value)
                        	},
                        	{
                        		name: '여',
                        		value : parseInt(fieldData.womanCount.value)
                        	}]
                    }]
        		}
    	);
        
    },
    // 강사 선택 화면
    showSelectManager: function ( selectValue ){
    
    	var that, data;
    	
    	that =this;
    	data = selectValue.data;
    	
    	$( '#managerSelectRegi_cur' ).val( data.data );
    	$( '#managerSelectRow' ).val( data.row );
    	$( '#managerSelectModal' ).modal( 'show' );
    	
    	$( "#ManagerSelectSave" ).off().on( "click", function () {
    		
    		var selectRow, newCurrManagerData;
    		
    		selectRow = currPopupSheet.GetSelectRow();
    		
    		newCurrManagerData = {
    				row : $( '#managerSelectRow' ).val( ),
    				new_curr_cod : $( '#managerSelectRegi_cur' ).val( ),
    				old_curr_cod : currPopupSheet.GetCellValue(selectRow, 4),
    				name : currPopupSheet.GetCellValue(selectRow, 1),
    				pkey : currPopupSheet.GetCellValue(selectRow, 0)	
    		}
    		
    		inapp.raise( that.name, {
                action: "new_curr_manager",
                data: newCurrManagerData
            } );
    		
    	} );
    	
    	$( "#modalsearchType" ).on( "change", function () {
            if($( "#modalsearchType" ).val() == 3){
            	$( "#modalsearchValue" ).hide();
            	$( "#modalsearchOn" ).hide();
            	
            	currPopupSheet.SetFilterValue(4, '' , 1);
            	currPopupSheet.SetFilterOption(1 , 0);
            	currPopupSheet.SetFilterOption(0 , 0);
            }else{
            	$( "#modalsearchValue" ).show();
            	$( "#modalsearchOn" ).show();
            	currPopupSheet.SetFilterOption(4 , 0);
            	currPopupSheet.SetFilterOption(1 , 0);
            	currPopupSheet.SetFilterOption(0 , 0);
            }
    		
        } );
    	
    	$("#modalsearchOn").click(function(){
    		var searchType ,searchValue;
    		searchType = $( "#modalsearchType" ).val();
    		searchValue = $( "#modalsearchValue" ).val();
    		
    		if( searchValue != "" ){
    			if(searchType==0){
    				currPopupSheet.SetFilterOption(1 , 0);
    				currPopupSheet.SetFilterValue(0, searchValue , 9);
    				$( "#modalsearchValue" ).val("");
    			}else if(searchType==1){
    				currPopupSheet.SetFilterOption(0 , 0);
    				currPopupSheet.SetFilterValue(1, searchValue , 11);
    				$( "#modalsearchValue" ).val("");
            	}
    		}else{
    			currPopupSheet.SetFilterOption(1 , 0);
    			currPopupSheet.SetFilterOption(0 , 0);
    		}
    	});
    	
    },
    showSelectContnets: function ( selectValue ){
    	var that, data 
    	
    	that =this;
    	data = selectValue.data;
    	
    	$( '#currContetsRow' ).val( data.row ); 
    	$( '#currContetsCol' ).val( data.col ); 
    	$( 'textarea#currModalContents' ).val( data.data ); 
    	
    	$( '#contentsModal' ).modal( 'show' );
    	
    	$( "#contentsSave" ).on( "click", function () {
    		
            var newContentsData = {
            	row : $( '#currContetsRow' ).val(),
            	col : $( '#currContetsCol' ).val(),
            	data : $( 'textarea#currModalContents' ).val( )
            }
   
            inapp.raise( that.name, {
                action: "new_curr_contents",
                data: newContentsData
            } );
     
        } );
    },
    chartCreate: function () {
    	echarts.init( $( '#peopleNumChart' )[ 0 ] ).setOption(
    			{
                    grid: {
                        top: 'center',
                        left: '3%',
                        right: '5%',
                        bottom: '100%',
                        containLabel: true
                    },
                    xAxis: [ {
                            type: 'value',
                            splitLine: {
                                show: false
                            },
                            boundaryGap: [ 0, 0.1 ],
                            max: 100,
                            min: 0
                        },
                        {
                            type: 'value'
                        }
                    ],
                    yAxis: {
                        type: 'category',
                        splitArea: {
                            show: true
                        },
                        data: [ '' ]
                    },
                    series: [ {
                        name: 'incentive',
                        type: 'bar',
                        color: '#003366',
                        markLine: {
                            label: {
                                show: false
                            },
                            symbol: [ 'none', 'none' ],
                            lineStyle: {
                                color: '#d48265',
                                type: 'solid'
                            },
                            data: [ {
                                xAxis: 80
                            } ]
                        },
                        data: []
                    } ]
                }		
    	);
    	
    	echarts.init( $( '#genderChart' )[ 0 ] ).setOption(
    			{
    				color: ['#5793f3', '#d14a61'],
                    legend: {
                    	orient: 'vertical',
                        right: -5,
                        top: 20,
                        data: [ '남', '여' ]
                    },
                    series: [ {
                        name: 'evaluation',
                        type: 'pie',
                        radius: '65%',
                        center: [ '35%', '50%' ],
                        data: [],
                        label: {
                            show: true,
                            position: 'inside',
                            formatter: '{d}% \n ({c}명)'
                        }
                    } ]
                }		  		
    	);
    }
} );